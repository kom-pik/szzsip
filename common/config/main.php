<?php

return [
     'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
     'language' => 'pl-PL',
     'sourceLanguage' => 'en-US',
     'aliases' => [
          'sql' => dirname(dirname(__DIR__)) . '/flyway/sql'
     ],
     'components' => [
//          'cache' => [
//               'class' => 'yii\caching\FileCache',
//          ],
          'authManager' => [
               'class' => 'yii\rbac\DbManager',
               'defaultRoles' => ['admin', 'serviceman'],
          ],
          'i18n' => [
               'translations' => [
                    'app*' => [
                         'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@common/messages',
//                    'sourceLanguage' => 'en-US',
                         'fileMap' => [
                              'app' => 'common/messages/app/pl.php',
                              'app/error' => 'error.php',
                         ],
                    ],
                    'yii' => [
                         'class' => 'yii\i18n\PhpMessageSource',
                         'basePath' => '@common/messages/yii',
                         'sourceLanguage' => 'pl-PL',
                    ],
               ],
          ],
     ],
     'modules' => [
          'admin' => [
               'class' => 'mdm\admin\Module',
//            'userClassName' => 'common\models\UsersModel'
               'controllerMap' => [
                    'assignment' => [
                         'class' => 'mdm\admin\controllers\AssignmentController',
                         'userClassName' => 'common\models\UsersModel',
                         'idField' => 'usr_id',
                         'usernameField' => 'usr_username',
                         'searchClass' => 'common\models\UsersSearchModel'
                    ],
                    'user' => [
                         'class' => 'frontend\controllers\UsersController',
//                    'userClassName' => 'common\models\UsersModel',
//                    'idField' => 'usr_id',
//                    'usernameField' => 'usr_username',
                    ],
//                'role' => 'frontend\components\admin\controllers\MyRoleController',
//                'rule' => 'frontend\components\admin\controllers\MyRuleController',
//                'default' => 'frontend\components\admin\controllers\MyDefaultController'
               ]
          ],
     ],
//    'as access' => [
//        'class' => 'mdm\admin\components\AccessControl',
//        'allowActions' => [
//            'site/*',
//            'rbac/*'
//        ]
//    ]
];
