<?php
return [
    'adminEmail' => 'admin@kom-pik.pl',
    'supportEmail' => 'admin@kom@kom-pik.pl',
    'users.usr_passwordResetTokenExpire' => 3600,
];
