<?php

namespace common\controllers;

use Yii;
use yii\web\Controller;
use common\models\UsersModel;
use common\models\GroupsModel;
use common\models\EntitiesCostsModel;
use frontend\services\ServiceUtilities;

/**
 * Description of MyWebController
 *
 * @author piqs
 * @property UsersModel $user 
 * @property GroupsModel $group For adding/edit
 * @property Request $request The request component. This property is read-only.
 */
abstract class MyWebController extends Controller {

     //put your code here
     protected $user = null;
     protected $client_ids = array();

     /**
      *
      * @var integer For search (if admin then null) 
      * 
      */
     protected $group_id = null;
     protected $group = null;
     protected $request;
     protected $messageNotAllowed = 'Nie posiadasz uprawnień do wykonania tej akcji.';
     protected $messageNoParams = "Brak odpowiednich parametrów";
     protected $messageNotFound = "Nie znaleziono ";
     protected $viewsPath = '@app/views/partials/';

     public function init() {
          parent::init();
          $this->user = empty(Yii::$app->user->identity) ? null : UsersModel::findIdentity(Yii::$app->user->identity->usr_id);
          if (!empty($this->user)) {
               $this->group = $this->user->isAdminOrSupervisor() && !empty($this->user->supervisedGroup) ? $this->user->supervisedGroup : $this->user->group;
               if (!$this->user->isAdmin()) {
                    if ($this->user->isSupervisor()) {
                         if (!empty($this->user->supervisedGroup)) {
                              $this->group_id = $this->user->supervisedGroup->gro_id;
                         } else {
                              $this->group_id = $this->user->group->gro_id;
                         }
                    } elseif (!empty($this->user->group)) {
                         $this->group_id = $this->user->group->gro_id;
                    }
               }
          }

          $this->request = Yii::$app->request;
          if (!empty($this->user) && $this->user->isClient()) {
               $this->client_ids[] = $this->user->usr_client_fkey;
          }
          $this->view->params = [
               'user' => $this->user,
               'controllerName' => $this->id,
               'group_id' => $this->group_id,
               'groups' => GroupsModel::getGroups()
          ];
     }

     public function goBack($type = '', $message = '') {
          if (!empty($type) && !empty($message)) {
               Yii::$app->session->addFlash($type, Yii::t('app', $message));
          }
          parent::goBack($this->request->referrer);
     }

     public function actionAddCost($entityId, $entityType) {
          if ($this->user->isClient()) {
               return $this->goBack('error', $this->messageNotAllowed);
          }
          if (empty($entityId) || empty($entityType)) {
               Yii::$app->response->format = 'json';
               return ['success' => false, 'message' => 'Brak wymaganych danych'];
          }
          $entityCostModel = new EntitiesCostsModel();
          if ($this->request->isPost) {
               if ($entityCostModel->load($this->request->post()) && $entityCostModel->save()) {
                    Yii::$app->response->format = 'json';
                    return ['success' => true, 'message' => 'Dodano koszt'];
               }
          }

          if (!empty($entityId) && !empty($entityType)) {
               $entityCostModel->enc_entity_fkey = $entityId;
               $entityCostModel->enc_entity_type = $entityType;
               return $this->renderAjax('@app/views/partials/add-cost', ['entityCostModel' => $entityCostModel]);
          }
     }

     public function actionCostsList($entityId, $entityType) {
          if (empty($entityId) || empty($entityType)) {
               Yii::$app->response->format = 'json';
               return ['success' => false, 'message' => 'Brak wymaganych danych'];
          }

          $entityCosts = EntitiesCostsModel::find()->onCondition(['enc_entity_fkey' => $entityId, 'enc_entity_type' => $entityType])
                          ->where(['!=', 'enc_status', EntitiesCostsModel::STATUS_DELETED])->all();
          return $this->renderAjax($this->viewsPath . 'costs-list', ['entityCosts' => $entityCosts]);
     }

     public function actionEditCost($id) {
          if (!empty($id)) {
               $entityCostModel = EntitiesCostsModel::find()->where(['enc_id' => $id])->andWhere(['!=', 'enc_status', EntitiesCostsModel::STATUS_DELETED])->one();
               if (!empty($entityCostModel)) {
                    if ($this->request->isPost) {
                         if ($entityCostModel->load($this->request->post()) && $entityCostModel->save()) {
                              Yii::$app->response->format = 'json';
                              return ['success' => true, 'message' => 'Zapisano zmiany'];
                         }
                    }
                    return $this->renderPartial($this->viewsPath . 'add-cost', ['entityCostModel' => $entityCostModel]);
               }
          }
          Yii::$app->response->format = 'json';
          return ['success' => false, 'message' => 'Brak wymaganych danych'];
     }

     public function actionDeleteCost(array $id) {
          if (!empty($id)) {
               $entitiesCostModelIds = EntitiesCostsModel::getIds(['in', 'enc_id', $id], new EntitiesCostsModel());
               if (!empty($entitiesCostModelIds)) {
                    EntitiesCostsModel::updateAll([
                         'enc_status' => EntitiesCostsModel::STATUS_DELETED,
                         'enc_updated_by' => $this->user->usr_id,
                         'enc_updated_at' => (new \yii\db\Expression('UNIX_TIMESTAMP(NOW())'))
                            ], ['in', 'enc_id', $id]);
               }
               return $this->goBack('success', 'Usunięto wpisy');
          }
     }
     
     public function actionDelete($id = null) {
          if ($this->request->isPost && !empty($this->request->post('ids')) || !empty($id)) {
               $ids = !empty($this->request->post('ids')) ? $this->request->post('ids') : $id;
               $className = \frontend\services\ServiceUtilities::getClassForDeleteActionByController()[$this->id];
               $message = ServiceUtilities::massDelete($ids, new $className(), $this->user);
               Yii::$app->session->addFlash($message['type'], $message['message']);
               return $this->goBack();
          } 
          return $this->goBack('error', $this->messageNoParams);
     }

}
