<?php

if (!function_exists('dd')){
     function dd(){
          $args = func_get_args();
          array_map(function($arg){
               echo '<pre>';
               var_dump($arg);
               echo '</pre>';               
          }, $args);
          die();
     }
}

if (!function_exists('vd')){
     
     function vd(){
          $args = func_get_args();
          array_map(function($arg){
               echo '<pre>';
               var_dump($arg);
               echo '</pre>';               
          }, $args);
     }
}
