<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\UsersModel */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['sites/reset-password', 'token' => $user->usr_password_reset_token]);
?>
<div class="password-reset">
    <p>Witaj <?= Html::encode($user->usr_username) ?>,</p>

    <p>Kliknij w poniższy link, aby zresetować hasło:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
