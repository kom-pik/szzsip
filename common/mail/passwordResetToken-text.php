<?php

/* @var $this yii\web\View */
/* @var $user common\models\UsersModel */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['sites/reset-password', 'token' => $user->usr_password_reset_token]);
?>
Witaj <?= $user->usr_username ?>,

Kliknij w poniższy link, aby zresetować hasło:

<?= $resetLink ?>
