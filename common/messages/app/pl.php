<?php

return [
    'owner' => 'Właściciel',
    'Nazwa cannot be blank.' => 'Nazwa nie może pozostać bez wartości.',
    'Forbidden' => 'Brak dostępu',
    'Time' => 'Czas',
    'reset_grid_view' => 'Resetuj widok',
    'created' => 'Utworzony'
];

