<?php

return [
    'Add Book' => 'Dodaj książkę',
    'Reset Grid' => 'Resetuj widok',
    'owner' => 'Właściciel',
    'Nazwa cannot be blank.' => 'Nazwa nie może pozostać bez wartości.',
    'Forbidden' => 'Brak dostępu',
    'Time' => 'Czas',
    'You are not allowed to perform this action.' => 'Nie możesz edytować tego obiektu',
    'Home' => 'Główna',
    'reset_grid_view' => 'Resetuj widok'
];

