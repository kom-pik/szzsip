<?php

namespace common\models;

use Yii;
use common\models\ProjectsModel;
use common\models\PriceListsModel;
use common\models\UsersModel;
use common\models\MyModel;
use common\models\GroupsModel;
use common\models\EntitiesCostsModel;
use yii\db\Query;

/**
 * User model
 *
 * @property integer $cli_id
 * @property string $cli_firstname
 * @property string $cli_lastname
 * @property string $cli_acronym
 * @property string $cli_phone
 * @property string $cli_nip
 * @property string $cli_street
 * @property string $cli_street_no
 * @property string $cli_postcode
 * @property string $cli_city
 * @property string $cli_email
 * @property string $cli_status
 * @property integer $cli_created_at
 * @property integer $cli_updated_at
 * @property string $cli_type
 * @property string $cli_info
 * @property integer $cli_attendant_fkey Description
 * @property integer $cli_price_list_fkey Description
 * @property integer $cli_group_fkey Description
 */
class ClientsModel extends MyModel {

     const TABLE_FIELD_PREFIX = 'cli_';
     const STATUS_ACTIVE = 'active';
     const STATUS_DELETED = 'deleted';
     const STATUS_LOCKED = 'locked';
     const TYPE_CUSTOMER = 'customer';
     const TYPE_COMPANY = 'company';
     const PROPERTY_PHONE = 'phone';
     const PROPERTY_STATUS = 'status';
     const PROPERTY_EMAIL = 'email';
     const PROPERTY_ATTENDANT = 'attendant';

     public static function getPropertiesAllowedToChangeByClientRole() {
          return [
               self::PROPERTY_EMAIL,
               self::PROPERTY_PHONE
          ];
     }

     public $cli_name;
     public $costsValue;
     public $clientTime;
     public $costs;

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return '{{%clients}}';
     }

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               ['cli_status', 'default', 'value' => self::STATUS_ACTIVE],
               [['cli_acronym', 'cli_firstname', 'cli_type'], 'required', 'message' => $this->messageRequired],
               ['cli_acronym', 'unique', 'message' => 'Klient o takim akronimie już istnieje'],
               ['cli_nip', 'unique', 'message' => 'Klient o takim numerze NIP już istnieje'],
               [['cli_firstname', 'cli_lastname', 'cli_acronym', 'cli_street', 'cli_street_no', 'cli_postcode', 'cli_city', 'cli_type', 'cli_nip', 'cli_info', 'cli_phone'], 'string'],
               [['cli_attendant_fkey', 'cli_group_fkey', 'cli_price_list_fkey'], 'integer'],
               ['cli_attendant_fkey', 'default', 'value' => $this->user->identity->usr_id],
               ['cli_email', 'email']
          ];
     }

     public function attributeLabels() {
          return [
               'cli_firstname' => $this->cli_type ? ($this->cli_type == self::TYPE_CUSTOMER ? Yii::t('app', 'Imię') : Yii::t('app', 'Nazwa')) : 'Imię lub nazwa firmy',
               'cli_lastname' => Yii::t('app', 'Nazwisko'),
               'cli_acronym' => Yii::t('app', 'Akronim'),
               'acronym' => Yii::t('app', 'Akronim'),
               'cli_phone' => Yii::t('app', 'Telefon'),
               'cli_nip' => Yii::t('app', 'NIP'),
               'cli_street' => Yii::t('app', 'Ulica'),
               'cli_street_no' => Yii::t('app', 'Numer budynku'),
               'cli_postcode' => Yii::t('app', 'Kod pocztowy'),
               'cli_city' => Yii::t('app', 'Miasto'),
               'cli_status' => Yii::t('app', 'Status'),
               'cli_info' => Yii::t('app', 'Opis'),
               'cli_created_at' => Yii::t('app', 'Utworzony'),
               'cli_created' => Yii::t('app', 'Utworzony'),
               'created' => Yii::t('app', 'Utworzony'),
               'cli_updated_at' => Yii::t('app', 'Edytowany'),
               'cli_type' => Yii::t('app', 'Typ'),
               'type' => Yii::t('app', 'Typ'),
               'cli_name' => Yii::t('app', 'Nazwa'),
               'name' => Yii::t('app', 'Nazwa'),
               'cli_attendant_fkey' => Yii::t('app', 'Opiekun'),
               'cli_email' => Yii::t('app', 'E-mail'),
               'cli_group_fkey' => Yii::t('app', 'Grupa klienta'),
               'cli_price_list_fkey' => Yii::t('app', 'Cennik')
          ];
     }

     public static function listStatuses() {
          return [
               self::STATUS_ACTIVE => Yii::t('app', 'Aktywny'),
               self::STATUS_DELETED => Yii::t('app', 'Usunięty'),
               self::STATUS_LOCKED => Yii::t('app', 'Zablokowany')
          ];
     }

     public static function listTypes() {
          return [
               self::TYPE_COMPANY => Yii::t('app', 'Firma'),
               self::TYPE_CUSTOMER => Yii::t('app', 'Osoba fiz.'),
          ];
     }

     /**
      * Finds user by username
      *
      * @param string $username
      * @return static|null
      */
     public static function findByFirstname($firstname) {
          return static::findOne(['cli_firstname' => $firstname, 'cli_status' => self::STATUS_ACTIVE]);
     }

     public static function findByAcronym($acronym) {
          return static::findOne(['cli_acronym' => $acronym, 'cli_status' => self::STATUS_ACTIVE]);
     }

     /**
      * @inheritdoc
      */
     public function getId() {
          return $this->getPrimaryKey();
     }
     
     public static function getClient($id, $group_id){
          $query = static::find()
                  ->onCondition(['!=', 'cli_status', self::STATUS_DELETED])
                  ->where(['cli_id' => $id]);
          if (!empty($group_id)){
               $query->andWhere(['cli_group_fkey' => $group_id]);
          }
          return $query->one();
     }

     public static function findAllClients($client_ids = array(), $group_id = null) {
          $query = (new Query())->select(["CONCAT(cli_firstname, ' ', COALESCE(cli_lastname, ''), ' (', cli_acronym, ')') AS fullname"])
                  ->from(self::tableName())
                  ->where(['!=', 'cli_status', UsersModel::STATUS_DELETED]);
          if (!empty($client_ids)) {
               $query->andWhere(['in', 'cli_id', $client_ids]);
          }
          if (!empty($group_id)) {
               $query->andWhere(['cli_group_fkey' => $group_id]);
          }
          $query->indexBy('cli_id');
          return $query->column();
     }

     public function getName() {
          if (empty($this->cli_firstname) && empty($this->cli_lastname)) {
               return 'brak danych';
          }
          $this->setName();
          return $this->cli_name;
     }

     public function setName() {
          if ($this->cli_type == self::TYPE_CUSTOMER) {
               $this->cli_name = $this->cli_firstname . ' ' . $this->cli_lastname;
          } else {
               $this->cli_name = $this->cli_firstname;
          }
     }

     public function getProjects($startDate, $endDate) {
          return $this->hasMany(ProjectsModel::className(), ['pro_client_fkey' => 'cli_id'])
                          ->onCondition(['!=', 'pro_status', ProjectsModel::STATUS_DELETED])
                          ->where(['>=', 'pro_created_at', $startDate])
                          ->andWhere(['<=', 'pro_created_at', $endDate])
                          ->all();
     }

     public function getOrders($startDate, $endDate) {
          return $this->hasMany(OrdersModel::className(), ['ord_client_fkey' => 'cli_id'])
                          ->onCondition(['!=', 'ord_status', OrdersModel::STATUS_DELETED])
                          ->where(['>=', 'ord_created_at', $startDate])
                          ->andWhere(['<=', 'ord_created_at', $endDate])
                          ->all();
     }

     public function getCreator() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'cli_created_by'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     public function getAttendant() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'cli_attendant_fkey'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     public function getUsers() {
          return $this->hasMany(UsersModel::className(), ['usr_client_fkey' => 'cli_id'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     public function getPriceList() {
          return $this->hasOne(PriceListsModel::className(), ['prl_id' => 'cli_price_list_fkey'])
                          ->onCondition(['!=', 'prl_status', PriceListsModel::STATUS_DELETED]);
     }

     public function getNewOrderOwner() {
          if (!empty($this->attendant)) {
               $newOrderOwner[$this->attendant->usr_id] = $this->attendant->usr_username;
          } elseif (!empty($this->group) && !empty($this->group->supervisor->usr_id)) {
               $newOrderOwner[$this->group->supervisor->usr_id] = $this->group->supervisor->usr_username;
          } else {
               $admin = UsersModel::findOne(['usr_type' => UsersModel::TYPE_ADMIN, 'usr_status' => UsersModel::STATUS_ACTIVE]);
               $newOrderOwner[$admin->usr_id] = $admin->usr_username;
          }
          return $newOrderOwner;
     }

     public function getClientTime($startDate, $endDate) {
          $this->clientTime = 0;
          if (!empty($this->getOrders($startDate, $endDate))) {
               $time = array_map(function($order) use ($startDate, $endDate) {
                    return $order->getOrderTime($startDate, $endDate);
               }, $this->getOrders($startDate, $endDate));
               $this->clientTime = array_sum($time);
          }
          return $this->clientTime;
     }
     
     public function getCostsValue($startDate, $endDate, $costSource = 'all'){
          $this->costsValue = 0;
          $clientProjects = $this->getProjects($startDate, $endDate);
          $clientOrders = $this->getOrders($startDate, $endDate);
          if (!empty($clientOrders) || !empty($clientProjects)){
               $projectsCosts = array_map(function($project) use ($startDate, $endDate){
                    return $project->getCostsValue($startDate, $endDate);
               }, $clientProjects);
               $ordersCosts = array_map(function($order) use ($startDate, $endDate, $costSource){
                    return $costSource == 'all' ? (empty($order->project) ? $order->getCostsValue($startDate, $endDate) : null) : $order->getCostsValue($startDate, $endDate);
               }, $clientOrders);
               if ($costSource == 'all'){
                    $this->costsValue = array_sum(array_merge($projectsCosts, $ordersCosts));
               } elseif ($costSource == EntitiesCostsModel::ENTITY_TYPE_PROJECTS) {
                    $this->costsValue = array_sum($projectsCosts);
               } else {
                    $this->costsValue = array_sum($ordersCosts);
               }
          }
          return $this->costsValue;
     }
     
     public function getCosts($startDate, $endDate){
          $query = new Query();
          $costs = $query
                  ->select('sum(enc_net_value) as cost_group_value, enc_type')
                  ->from(EntitiesCostsModel::tableName())
                  ->leftJoin(ProjectsModel::tableName(), 'enc_entity_fkey = pro_id and enc_entity_type = '.EntitiesCostsModel::ENTITY_TYPE_PROJECTS)
                  ->leftJoin(OrdersModel::tableName(), 'enc_entity_fkey = ord_id and enc_entity_type = '.EntitiesCostsModel::ENTITY_TYPE_ORDERS)
                  ->where(['!=', 'enc_status', EntitiesCostsModel::STATUS_DELETED])
                  ->andWhere(['between', 'enc_created_at', $startDate, $endDate])
                  ->andWhere('pro_client_fkey = ' .$this->cli_id . ' and pro_status != '.ProjectsModel::STATUS_DELETED.' or ord_client_fkey = ' .$this->cli_id .' and ord_status != '.OrdersModel::STATUS_DELETED)
                  ->groupBy('enc_type')
                  ->all();
          $this->costs = $costs;
          return $this->costs;
     }
}
