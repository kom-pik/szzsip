<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use common\models\ClientsModel;

class ClientsSearchModel extends ClientsModel {

     // add the public attributes that will be used to store the data to be search
     public $acronym;
     public $name;
     public $status;
     public $creator;
     public $created;
     public $type;

     // now set the rules to make those attributes safe
     public function rules() {
          return [
               // ... more stuff here
               [['acronym', 'name', 'status', 'creator', 'created', 'type'], 'safe'],
                  // ... more stuff here
          ];
     }

// ... model continues here
     public function search($params, $group_id = null) {
          // create ActiveQuery
          $query = ClientsModel::find()->having(['<>', 'cli_status', ClientsModel::STATUS_DELETED])->orderBy('cli_acronym');
          // Important: lets join the query with our previously mentioned relations
          // I do not make any other configuration like aliases or whatever, feel free
          // to investigate that your self
          $query->joinWith(['creator']);
          
          if (!empty($group_id)){
               $query->where(['cli_group_fkey' => $group_id]);
          }
          $dataProvider = new ActiveDataProvider([
               'query' => $query,
          ]);

          // Important: here is how we set up the sorting
          // The key is the attribute name on our "TourSearch" instance
          $dataProvider->sort->attributes['acronym'] = [
               // The tables are the ones our relation are configured to
               // in my case they are prefixed with "tbl_"
               'asc' => ['clients.cli_acronym' => SORT_ASC],
               'desc' => ['clients.cli_acronym' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['creator'] = [
               'asc' => ['users.usr_id' => SORT_ASC],
               'desc' => ['users.usr_id' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['status'] = [
               'asc' => ['clients.cli_status' => SORT_ASC],
               'desc' => ['clients.cli_status' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['name'] = [
               'asc' => ['clients.cli_firstname' => SORT_ASC],
               'desc' => ['clients.cli_firstname' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['created'] = [
               'asc' => ['clients.cli_created_at' => SORT_ASC],
               'desc' => ['clients.cli_created_at' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['type'] = [
               'asc' => ['clients.cli_type' => SORT_ASC],
               'desc' => ['clients.cli_type' => SORT_DESC],
          ];
          // No search? Then return data Provider
          if (!($this->load($params) && $this->validate())) {
               return $dataProvider;
          }

          $query->andFilterWhere(['like', 'clients.cli_acronym', $this->acronym])
                  ->andFilterWhere(['like', 'users.usr_id', $this->creator])
                  ->andFilterWhere(['like', 'clients.cli_status', $this->status])
                  ->andFilterWhere(['like', 'clients.cli_type', $this->type])
                  ->andFilterWhere(['like', 'clients.cli_id', $this->name]);


          if (!empty($this->created)) {
               $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
               if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2) {
                    $date1 = strtotime(trim($matches[0][0]));
                    $date2 = strtotime(trim($matches[0][1]));
                    $query->andFilterWhere(['between', 'cli_created_at', $date1, $date2]);
               }
          }
          // We have to do some search... Lets do some magic


          return $dataProvider;
     }

}
