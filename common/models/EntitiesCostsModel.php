<?php

namespace common\models;

use common\models\MyModel;
use common\models\UsersModel;
use common\models\ProjectsModel;
use common\models\OrdersModel;
use common\models\TasksModel;

/**
 * Description of CostsModel
 *
 * @author Marcin Pikul
 */

/**
 * @property integer $enc_id Description
 * @property integer $enc_entity_fkey Description
 * @property integer $enc_entity_type Description
 * @property integer $enc_type
 * @property float   $enc_net_value
 * @property integer $enc_created_at
 * @property integer $enc_updated_at
 * @property integer $enc_created_by
 * @property integer $enc_updated_by
 * @property integer $enc_status Description
 * @property string $enc_description Description
 * @property string $enc_own_number Description
 */
class EntitiesCostsModel extends MyModel {

     const TABLE_FIELD_PREFIX = 'enc_';
     const ENTITY_TYPE_PROJECTS = 1;
     const ENTITY_TYPE_ORDERS = 2;
     const ENTITY_TYPE_TASKS = 3;
     const COST_TYPE_WORK = 1;
     const COST_TYPE_STUFF = 2;
     const COST_TYPE_FOREIGN_SERVICE = 3;
     const COST_TYPE_JOURNEY = 4;
     const COST_TYPE_OTHER = 99;

     public static function tableName() {
          return '{{%entities_costs}}';
     }

     public function behaviors() {
          return parent::behaviors();
     }

     public function rules() {
          return [
               [['enc_entity_fkey', 'enc_entity_type', 'enc_type', 'enc_status'], 'integer'],
               ['enc_status', 'default', 'value' => self::STATUS_ACTIVE],
               [['enc_description', 'enc_own_number'], 'string'],
               ['enc_net_value', 'number']
          ];
     }

     public function attributeLabels() {
          return [
               'enc_entity_fkey' => 'Id encji',
               'enc_entity_type' => 'Typ encji',
               'enc_type' => 'Typ kosztu',
               'enc_net_value' => 'Wartość netto',
               'enc_status' => 'Status',
               'enc_description' => 'Opis',
               'enc_own_number' => 'Własny numer',
               'creator' => 'Dodał',
               'costType' => 'Typ kosztu',
               'value' => 'Wartość netto',
               'number' => 'Własny numer',
               'description' => 'Opis',
               'enc_created_at' => 'Dodany',
               'entityName' => 'Nazwa źródła'
          ];
     }

     public static function listEntitiesTypes() {
          return [
               self::ENTITY_TYPE_PROJECTS => 'projekty',
               self::ENTITY_TYPE_ORDERS => 'zlecenia',
               self::ENTITY_TYPE_TASKS => 'zadania',
          ];
     }

     public static function getEntityClassName() {
          return [
               self::ENTITY_TYPE_PROJECTS => ProjectsModel::className(),
               self::ENTITY_TYPE_ORDERS => OrdersModel::className(),
               self::ENTITY_TYPE_TASKS => TasksModel::className()
          ];
     }

     public static function listCostsTypes() {
          return [
               self::COST_TYPE_WORK => 'praca własna',
               self::COST_TYPE_STUFF => 'materiały',
               self::COST_TYPE_FOREIGN_SERVICE => 'usługi obce',
               self::COST_TYPE_JOURNEY => 'dojazd',
               self::COST_TYPE_OTHER => 'inne'
          ];
     }

     public function getCreator() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'enc_created_by'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     public function getEntity() {
//          dd($this->enc_entity_type);
          $class = static::getEntityClassName()[$this->enc_entity_type];
          $model = new $class();
          return $this->hasOne($class, [$model->idAttribute => 'enc_entity_fkey'])
                          ->onCondition(['!=', $model->statusAttribute, $class::STATUS_DELETED]);
     }

     public function getProject() {
          return $this->hasOne(ProjectsModel::className(), ['pro_id' => 'enc_entity_fkey'])
                          ->onCondition(['!=', 'pro_status', ProjectsModel::STATUS_DELETED]);
     }
     
     public function getOrder(){
          return $this->hasOne(OrdersModel::className(), ['ord_id' => 'enc_entity_fkey'])
                          ->onCondition(['!=', 'ord_status', OrdersModel::STATUS_DELETED]);
     }

     public static function getSum($costs) {
          $total = [];
          if (is_array($costs)) {
               $total = array_map(function($cost) {
                    return $cost->enc_net_value;
               }, $costs);
          } else {
               $total[] = $costs->enc_net_value;
          }
          return array_sum($total);
     }

}
