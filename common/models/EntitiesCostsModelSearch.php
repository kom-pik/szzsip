<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use common\models\EntitiesCostsModel;

class EntitiesCostsModelSearch extends EntitiesCostsModel {

     // add the public attributes that will be used to store the data to be search
     public $creator;
     public $id;
     public $created;
     public $entity;
     public $number;
     public $value;
     public $costType;
     public $description;
     public $entityName;

     public function rules() {
          return [
               [['creator', 'id', 'created', 'entity', 'number', 'value', 'costType', 'description', 'entityName'], 'safe'],
          ];
     }

     public function search($params, $id, $entityType) {
          $query = EntitiesCostsModel::find()
                  ->where(['in', 'enc_entity_fkey', (array) $id])
                  ->andWhere(['!=', $this->statusAttribute, self::STATUS_DELETED])
                  ->andWhere(['in', 'enc_entity_type',(array) $entityType]);
          // Important: lets join the query with our previously mentioned relations
          // I do not make any other configuration like aliases or whatever, feel free
          // to investigate that your self
          $query->joinWith(['project']);
          if (in_array(EntitiesCostsModel::ENTITY_TYPE_PROJECTS, (array) $entityType)){
               $query->joinWith('order');
          }
          $dataProvider = new ActiveDataProvider([
               'query' => $query,
          ]);

          // Important: here is how we set up the sorting
          // The key is the attribute name on our "TourSearch" instance

          $dataProvider->sort->attributes['creator'] = [
               'asc' => ['users.usr_id' => SORT_ASC],
               'desc' => ['users.usr_id' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['created'] = [
               'asc' => ['entities_costs.enc_created_at' => SORT_ASC],
               'desc' => ['entities_costs.enc_created_at' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['number'] = [
               'asc' => ['entities_costs.enc_own_number' => SORT_ASC],
               'desc' => ['entities_costs.enc_own_number' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['value'] = [
               'asc' => ['entities_costs.enc_net_value' => SORT_ASC],
               'desc' => ['entities_costs.enc_net_value' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['costType'] = [
               'asc' => ['entities_costs.enc_type' => SORT_ASC],
               'desc' => ['entities_costs.enc_type' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['description'] = [
               'asc' => ['entities_costs.enc_description' => SORT_ASC],
               'desc' => ['entities_costs.enc_description' => SORT_DESC],
          ];
//          $dataProvider->sort->attributes['description'] = [
//               'asc' => ['entities_costs.enc_description' => SORT_ASC],
//               'desc' => ['entities_costs.enc_description' => SORT_DESC],
//          ];
          // No search? Then return data Provider
          if (!($this->load($params) && $this->validate())) {
               return $dataProvider;
          }

          $query->andFilterWhere(['like', 'entities_costs.enc_own_number', $this->number])
                  ->andFilterWhere(['entities_costs.enc_type' => $this->costType])
                  ->andFilterWhere(['like', 'entities_costs.enc_description', $this->description])
                  ->andFilterWhere(['like', 'entities_costs.enc_net_value', $this->value]);

//        
          if (!empty($this->created)) {
               $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
               if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2) {
                    $date1 = strtotime(trim($matches[0][0]));
                    $date2 = strtotime(trim($matches[0][1]));
                    $query->andFilterWhere(['between', 'enc_created_at', $date1, $date2]);
               }
          }

          $query->joinWith(['creator' => function($q) {
                    $q->where('users.usr_firstname LIKE "%' . $this->creator . '%" OR users.usr_lastname LIKE "%' . $this->creator . '%"');
               }]);
          return $dataProvider;
     }

}
