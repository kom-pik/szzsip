<?php

namespace common\models;

use Yii;
use yii\db\Query;
use common\models\MyModel;
use common\models\UsersModel;
use common\models\ClientsModel;
use common\models\ProjectsModel;
use common\models\OrdersModel;

/**
 * This is the model class for table "group".
 *
 * @property integer $gro_id
 * @property string $gro_name
 * @property string $gro_description
 * @property integer $gro_status
 * @property integer $gro_created_at
 * @property integer $gro_updated_at
 * @property integer $gro_created_by
 * @property integer $gro_updated_by
 * @property integer $gro_supervisor_fkey
 * @property UsersModel[] $members Description
 * @property ProjectsModel[] $projects Description
 * @property OrdersModel[] $orders Description
 */
class GroupsModel extends MyModel {

     const TABLE_FIELD_PREFIX = 'gro_';
     
     public $members;
     public $projects;
     public $orders;
     /**
      * @inheritdoc
      */
     public static function tableName() {
          return 'groups';
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               [['gro_status', 'gro_supervisor_fkey', 'gro_id'], 'integer'],
               ['gro_status', 'default', 'value' => static::STATUS_ACTIVE],
               [['gro_name'], 'string', 'max' => 255],
               [['gro_description'], 'string', 'max' => 500],
               [['gro_supervisor_fkey', 'gro_name'], 'required', 'message' => '{attribute} nie może pozostać bez wartości']
          ];
     }

     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function attributeLabels() {
          return [
               'gro_id' => Yii::t('app', 'ID'),
               'gro_name' => Yii::t('app', 'Nazwa'),
               'name' => Yii::t('app', 'Nazwa'),
               'gro_description' => Yii::t('app', 'Opis'),
               'gro_status' => Yii::t('app', 'Status'),
               'gro_created_at' => Yii::t('app', 'Utworzona'),
               'created' => Yii::t('app', 'Utworzona'),
               'gro_updated_at' => Yii::t('app', 'Edytowana'),
               'gro_created_by' => Yii::t('app', 'Utworzył'),
               'gro_updated_by' => Yii::t('app', 'Edytował'),
               'gro_supervisor_fkey' => Yii::t('app', 'Przełożony'),
               'supervisor' => Yii::t('app', 'Przełożony'),
          ];
     }

     public function getSupervisor() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'gro_supervisor_fkey'])
                         ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED])
                         ->andOnCondition(['in', 'usr_type', array(UsersModel::TYPE_ADMIN, UsersModel::TYPE_SUPERVISOR)]);
     }

     public function getClients() {
          return $this->hasMany(ClientsModel::className(), ['cli_group_fkey' => 'gro_id'])
                          ->onCondition(['!=', 'cli_status', ClientsModel::STATUS_DELETED]);
     }
     
     public function getMembers(){
          return $this->hasMany(UsersModel::className(), ['usr_group_fkey' => 'gro_id'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }
     
     public function getProjects($startDate, $endDate){
          return $this->hasMany(ProjectsModel::className(), ['pro_group_fkey' => 'gro_id'])
                          ->onCondition(['!=', 'pro_status', ProjectsModel::STATUS_DELETED])
                          ->where(['>=', 'pro_created_at', $startDate])
                          ->andWhere(['<=', 'pro_created_at', $endDate])
                          ->all();
     }
     
     public function getOrders($startDate, $endDate){
          return $this->hasMany(OrdersModel::className(), ['ord_group_fkey' => 'gro_id'])
                          ->onCondition(['!=', 'ord_status', OrdersModel::STATUS_DELETED])
                          ->andWhere(['>=', 'ord_created_at', $startDate])
                          ->andWhere(['<=', 'ord_created_at', $endDate])
                          ->all();
     }

     public static function getGroups(){
          return (new Query())->select('gro_name')->from(self::tableName())
                  ->where(['gro_status' => self::STATUS_ACTIVE])
                  ->indexBy('gro_id')
                  ->column();
     }
     
     public function findGroupById($group_id){
          return static::find()->onCondition(['gro_id' => $group_id])
                  ->where(['gro_status' => self::STATUS_ACTIVE])
                  ->one();
     }

}
