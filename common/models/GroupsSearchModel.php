<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models;

use common\models\GroupsModel;
use yii\data\ActiveDataProvider;

/**
 * Description of GroupsModelSearch
 *
 * @author Marcin Pikul
 */
class GroupsSearchModel extends GroupsModel{
     
     public $supervisor;
     public $name;
     public $created;
     
     public function rules() {
          return [
               [['supervisor', 'name', 'created'], 'safe']
          ];
     }
     
     public function search($params) {
          $query = GroupsModel::find()->having(['!=', 'gro_status', GroupsModel::STATUS_DELETED]);
          
          $query->joinWith(['supervisor']);
          
          $dataProvider = new ActiveDataProvider([
               'query' => $query,
          ]);
          
          $dataProvider->sort->attributes['supervisor'] = [
               'asc' => ['users.usr_username' => SORT_ASC],
               'desc' => ['users.usr_username' => SORT_DESC],
          ];
          
          $dataProvider->sort->attributes['name'] = [
               'asc' => ['groups.gro_name' => SORT_ASC],
               'desc' => ['groups.gro_name' => SORT_DESC],
          ];
          
          if (!($this->load($params) && $this->validate())) {
               return $dataProvider;
          }
          
          $query->andFilterWhere(['like', 'users.usr_username', $this->supervisor])
               ->andFilterWhere(['like', 'groups.gro_name', $this->name]);
          
          if (!empty($this->created)) {
               $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
               if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2) {
                    $date1 = strtotime(trim($matches[0][0]));
                    $date2 = strtotime(trim($matches[0][1]));
                    $query->andFilterWhere(['between', 'gro_created_at', $date1, $date2]);
               }
          }
          
          return $dataProvider;
     }
}
