<?php

namespace common\models;

use Yii;

/**
 * Description of MigrationsModel
 *
 * @author Marcin Pikul
 */
/**
 * @property string $name Description
 * @property string $body Description
 */
class MigrationsModel extends \yii\base\Model {
     public $name;
     public $body;
     
     public function rules() {
          return [
               [['name', 'body'], 'string'],
               [['name', 'body'], 'required', 'message' => 'Pole wymagane'],
          ];
     }
     
     public function saveMigration(){
          
          $this->prepareFileName();
          return file_put_contents(Yii::getAlias("@sql/{$this->name}"), $this->body);
     }
     
     public function prepareFileName() {
          $this->name = 'V'. date('Y_m_d_H_i_s') . '__' .trim(str_replace('`', '', str_replace(' ', '_', $this->name))) . '.sql';
     }
}
