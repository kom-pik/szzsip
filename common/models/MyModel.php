<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Query;
use common\models\UsersModel;
use common\models\GroupsModel;

/**
 * Description of MyModel
 *
 * @author piqs
 * @property UsersModel $user Description
 * @property Query $query Description
 */
abstract class MyModel extends \yii\db\ActiveRecord {

     public $tableFieldPrefix;
     public $user;
     public $createdAtAttribute;
     public $updatedAtAttribute;
     public $createdByAttribute;
     public $updatedByAttribute;
     public $statusAttribute;
     public $idAttribute;
     public $groupAttribute;
     public $nameAttribute;
     public $query;
     public $messageRequired = 'Pole nie może być puste';

     const STATUS_DELETED = 0;
     const STATUS_ACTIVE = 1;
     const TABLE_FIELD_PREFIX = '';

     public function behaviors() {
          return [
              [
                  'class' => TimestampBehavior::className(),
                  'createdAtAttribute' => $this->createdAtAttribute,
                  'updatedAtAttribute' => $this->updatedAtAttribute,
                  'attributes' => [
                      \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => $this->createdAtAttribute,
                      \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => $this->updatedAtAttribute,
                  ]
              ],
              [
                  'class' => BlameableBehavior::className(),
                  'createdByAttribute' => $this->createdByAttribute,
                  'updatedByAttribute' => $this->updatedByAttribute,
                  'attributes' => [
                      \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => $this->createdByAttribute,
                      \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => $this->updatedByAttribute,
                  ]
              ],
          ];
     }
     
//     public function beforeSave($insert) {
//          return parent::beforeSave($insert);
//     }

     public function init() {
          $this->createdAtAttribute = static::TABLE_FIELD_PREFIX . 'created_at';
          $this->updatedAtAttribute = static::TABLE_FIELD_PREFIX . 'updated_at';
          $this->createdByAttribute = static::TABLE_FIELD_PREFIX . 'created_by';
          $this->updatedByAttribute = static::TABLE_FIELD_PREFIX . 'updated_by';
          $this->statusAttribute = static::TABLE_FIELD_PREFIX . 'status';
          $this->idAttribute = static::TABLE_FIELD_PREFIX . 'id';
          $this->groupAttribute = $this->className() == GroupsModel::className() ? $this->idAttribute : static::TABLE_FIELD_PREFIX . 'group_fkey';
          $noName = [UsersModel::className(), EntitiesCostsModel::className(), EntitiesCostsModelSearch::className(), ClientsModel::className(), OrdersTasksModel::className(), StartsStopsModel::className()];
          $this->nameAttribute = !in_array($this->className(), $noName) ? static::TABLE_FIELD_PREFIX . 'name' : '';
          $this->user = \Yii::$app->user;
          $this->query = new Query();
          parent::init();
     }

     public function delete() {
          $this->setAttributes([
              $this->statusAttribute => static::STATUS_DELETED,
              $this->updatedAtAttribute => time(),
              $this->updatedByAttribute => $this->user->identity->usr_id
          ], false);
          return $this->save(false, [
                      static::TABLE_FIELD_PREFIX . 'status',
                      static::TABLE_FIELD_PREFIX . 'updated_at',
                      static::TABLE_FIELD_PREFIX . 'updated_by'
          ]);
     }
     
     public static function getIds($condition, $model){
          return (new Query)->select($model->idAttribute)
                  ->from($model::tableName())
                  ->where(['!=', $model->statusAttribute, $model::STATUS_DELETED])
                  ->andWhere($condition)
                  ->column();
     }
     
     public function getName(){
          return $this->attributes[$this->nameAttribute];
     }
     
     public function getGroup() {
          return $this->hasOne(GroupsModel::className(), ['gro_id' => static::TABLE_FIELD_PREFIX.'group_fkey'])
                          ->onCondition(['!=', 'gro_status', GroupsModel::STATUS_DELETED]);
     }

}
