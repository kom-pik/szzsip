<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use common\models\OrdersTasksModel;

class OrderTaskSearch extends OrdersTasksModel
{

    // add the public attributes that will be used to store the data to be search
    public $creator;
//    public $status;
    public $id;
    public $created;
    public $task;
    public $order;
    public $time;



    // now set the rules to make those attributes safe
    public function rules()
    {
        return [
            // ... more stuff here
            [['creator', 'id', 'created', 'task', 'order', 'time'], 'safe'],
            // ... more stuff here
        ];
    }
    
    public function attributeLabels()
    {
        return [
            
            'creator' => Yii::t('app', 'Właściciel zlecenia'),
            'client' => Yii::t('app', 'Klient'),
            'executive' => Yii::t('app', 'Wykonawca'),
//            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Nazwa zadania'),
            'task' => Yii::t('app', 'Nazwa zadania'),
            'created' => Yii::t('app', 'Utworzony'),
            'project' => Yii::t('app', 'Projekt'),
        ];
    }
// ... model continues here
    public function search($params, $id)
    {
        // create ActiveQuery
        $query = OrdersTasksModel::find()->where(['ort_order_fkey' => $id])->andWhere(['!=', $this->statusAttribute, self::STATUS_DELETED]);
        // Important: lets join the query with our previously mentioned relations
        // I do not make any other configuration like aliases or whatever, feel free
        // to investigate that your self
        $query->joinWith(['creator', 'task', 'order']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance

        $dataProvider->sort->attributes['creator'] = [
            'asc' => ['users.usr_id' => SORT_ASC],
            'desc' => ['users.usr_id' => SORT_DESC],
        ];
//        $dataProvider->sort->attributes['status'] = [
//            'asc' => ['task.status' => SORT_ASC],
//            'desc' => ['task.status' => SORT_DESC],
//        ];
        $dataProvider->sort->attributes['task'] = [
            'asc' => ['tasks.tas_name' => SORT_ASC],
            'desc' => ['tasks.tas_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['created'] = [
            'asc' => ['task.tas_created_at' => SORT_ASC],
            'desc' => ['task.tas_created_at' => SORT_DESC],
        ];
        // No search? Then return data Provider
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
        ])
        ->andFilterWhere(['like', 'users.usr_id', $this->creator])
//        ->andFilterWhere(['like', 'task.status', $this->status])
        ->andFilterWhere(['like', 'tasks.tas_name', $this->task]);
//        
//        
        if(!empty($this->created)){
            $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
            if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2){
               $date1 = strtotime(trim($matches[0][0]));
               $date2 = strtotime(trim($matches[0][1]));
               $query->andFilterWhere(['between', 'ort_created_at', $date1, $date2]);
            }
        }
        // We have to do some search... Lets do some magic


        return $dataProvider;
    }
}