<?php

namespace common\models;

use Yii;
use common\models\UsersModel;
use common\models\ClientsModel;
use common\models\ProjectsModel;
use common\models\OrdersTasksModel;
use yii\helpers\ArrayHelper;

/**
 * Order model
 *
 * @property integer $ord_id
 * @property string $ord_name
 * @property integer $ord_type
 * @property integer $ord_owner_fkey
 * @property integer $ord_group_fkey
 * @property integer $ord_executive_fkey
 * @property integer $ord_client_fkey Description
 * @property integer $ord_created_at
 * @property integer $ord_updated_at
 * @property integer $ord_status
 * @property integer $ord_project_fkey
 * @property string $ord_description
 * @property integer $ord_budget_type
 * @property float $ord_budget_value
 */
class OrdersModel extends ProjectsModel {

     const TABLE_FIELD_PREFIX = 'ord_';
     const SCENARIO_UPDATE_ORDER = 'scenario_update_order';
     const SCENARIO_ADD_ORDER = 'scenario_add_order';
     const ENTITY_TYPE = EntitiesCostsModel::ENTITY_TYPE_ORDERS;

     public $orderTime;
     /**
      * @inheritdoc
      */
     public static function tableName() {
          return '{{%orders}}';
     }

     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               [['ord_executive_fkey', 'ord_project_fkey', 'ord_client_fkey', 'ord_status', 'ord_owner_fkey', 'ord_budget_type', 'ord_type', 'ord_group_fkey'], 'integer'],
               ['ord_name', 'string'],
               ['ord_name', 'required', 'message' => 'Nazwa nie może zostać pusta'],
               ['ord_description', 'string'],
               ['ord_status', 'default', 'value' => static::STATUS_NEW],
               [['ord_owner_fkey', 'ord_executive_fkey'], 'default', 'value' => \Yii::$app->user->identity->id],
               ['ord_budget_value', 'number'],
               ['ord_group_fkey', 'required', 'when' => function(){
                    return $this->user->identity->isAdmin();
               }, 'message' => 'Wybierz grupę'],
          ];
     }

     public function attributeLabels() {
          return [
               'ord_owner_fkey' => Yii::t('app', 'Właściciel zlecenia'),
               'ord_group_fkey' => Yii::t('app', 'Grupa'),
               'ord_type' => Yii::t('app', 'Typ zlecenia'),
               'ord_executive_fkey' => Yii::t('app', 'Wykonawca'),
               'ord_name' => Yii::t('app', 'Nazwa'),
               'ord_client_fkey' => Yii::t('app', 'Klient'),
               'ord_status' => Yii::t('app', 'Status'),
               'ord_description' => Yii::t('app', 'Opis'),
               'ord_created_at' => Yii::t('app', 'Utworzony'),
               'ord_created' => Yii::t('app', 'Utworzony'),
               'ord_project_fkey' => Yii::t('app', 'Projekt'),
               'owner' => Yii::t('app', 'Właściciel zlecenia'),
               'client' => Yii::t('app', 'Klient'),
               'project' => Yii::t('app', 'Projekt'),
               'time' => Yii::t('app', 'Czas pracy'),
               'created' => Yii::t('app', 'Utworzony'),
               'name' => Yii::t('app', 'Nazwa'),
               'ord_budget_type' => Yii::t('app', 'Typ budżetu'),
               'ord_budget_value' => !empty($this->ord_budget_type) ? ($this->ord_budget_type == self::BUDGET_TYPE_HOURS ? Yii::t('app', 'Liczba godzin') : Yii::t('app', 'Wartość w PLN')) : Yii::t('app', 'Wartość w PLN lub godz.'),
          ];
     }

     public static function getAllOrdersNames($index = 'ord_name', $project_ids = array(), $client_ids = array(), $user_ids = array(), $group_id = null) {
          $query = (new \yii\db\Query)
                  ->select(['ord_name'])
                  ->from(self::tableName())
                  ->where(['!=', 'ord_status', self::STATUS_DELETED]);
          if (!empty($project_ids)) {
               $query->andWhere(['in', 'ord_project_fkey', $project_ids]);
          }

          if (!empty($client_ids)) {
               $query->andWhere(['in', 'ord_client_fkey', $client_ids]);
          }

          if (!empty($user_ids)) {
               $query->andWhere(['in', 'ord_owner_fkey', $user_ids]);
          }
          
          if (!empty($group_id)) {
               $query->andWhere(['ord_group_fkey' => $group_id]);
          }

          $query->indexBy($index)->orderBy('ord_name');
          return $query->column();
     }

     public function getShortName($charsCount = 25) {
          if (strlen($this->ord_name) > $charsCount) {
               return substr($this->ord_name, 0, 22) . '...';
          }
          return $this->ord_name;
     }

     public function getClient() {
          return $this->hasOne(ClientsModel::className(), ['cli_id' => 'ord_client_fkey'])
                          ->onCondition(['!=', 'cli_status', ClientsModel::STATUS_DELETED]);
     }

     public function getOwner() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'ord_owner_fkey'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     public function getProject() {
          return $this->hasOne(ProjectsModel::className(), ['pro_id' => 'ord_project_fkey'])
                          ->onCondition(['!=', 'pro_status', ProjectsModel::STATUS_DELETED]);
     }

     public function getOrdersTasks($startDate, $endDate) {
          return $this->hasMany(OrdersTasksModel::className(), ['ort_order_fkey' => 'ord_id'])
                          ->onCondition(['!=', 'ort_status', OrdersTasksModel::STATUS_DELETED])
                          ->where(['>=', 'ort_created_at', $startDate])
                          ->andWhere(['<=', 'ort_created_at', $endDate])->all();
     }

     public function getTasks() {
          return $this->hasMany(TasksModel::className(), ['tas_id' => 'ort_task_fkey'])
                          ->via('ordersTasks');
     }

     public function getOrdersTasksTime($startDate, $endDate) {
          $time = [];
          $ordersTasks = $this->getOrdersTasks($startDate, $endDate);
          if (!empty($ordersTasks)) {
               $time = array_map(function($ot) use ($startDate, $endDate) {
                    return [
                         'ort_id' => $ot->ort_id, 
                         'ort_order_fkey' => $ot->ort_order_fkey, 
                         'ort_task_fkey' => $ot->ort_task_fkey, 
                         'time' => $ot->getTime($startDate, $endDate)];
               }, $ordersTasks);
          }
          return $time;
     }

     public function getOrderTime($startDate = null, $endDate = null) {
          $this->orderTime = 0;
          if (empty($startDate)) {
               $startDate = 0;
          }
          if (empty($endDate)) {
               $endDate = time();
          }
          $ordersTasksTime = $this->getOrdersTasksTime($startDate, $endDate);
          if (!empty($ordersTasksTime)) {
               $time = array_map(function($ott) {
                    return ArrayHelper::getValue($ott, 'time');
               }, $ordersTasksTime);
               $this->orderTime = array_sum($time);
          }
          return $this->orderTime;
     }

     public function getCostsValue($startDate = null, $endDate = null) {
          $this->costsValue = 0;
          if (empty($startDate)) {
               $startDate = 0;
          }
          if (empty($endDate)) {
               $endDate = time();
          }
          $costs = $this->getCosts($startDate, $endDate);
          if (!empty($costs)) {
               $costsValue = array_map(function($cost) {
                    return $cost->enc_net_value;
               }, $costs);
               $this->costsValue = array_sum($costsValue);
          }
          return $this->costsValue;
     }

}
