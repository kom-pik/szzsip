<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use common\models\ClientsModel;

class OrdersSearchModel extends OrdersModel {

     // add the public attributes that will be used to store the data to be search
     public $owner;
     public $client;
     public $executive;
     public $status;
     public $name;
     public $created;
     public $project;
     public $time;

     // now set the rules to make those attributes safe
     public function rules() {
          return [
               // ... more stuff here
               [['owner', 'client', 'status', 'name', 'created', 'executive', 'project', 'time'], 'safe'],
                  // ... more stuff here
          ];
     }

// ... model continues here
     public function search($params, $project_ids = null, $client_ids = null, $owner_ids = null, $group_id = null) {
          // create ActiveQuery
          $query = OrdersModel::find()->onCondition(['!=', 'ord_status', OrdersModel::STATUS_DELETED]);

          if (!empty($project_ids)) {
               $query->where(['in', 'ord_project_fkey', $project_ids]);
          }

          if (!empty($client_ids)) {
               $query->andWhere(['in', 'ord_client_fkey', $client_ids]);
          }

          if (!empty($owner_ids)) {
               $query->andWhere(['in', 'ord_owner_fkey', $owner_ids]);
          }

          if (!empty($group_id)) {
               $query->andWhere(['ord_group_fkey' => $group_id]);
          }
          // Important: lets join the query with our previously mentioned relations
          // I do not make any other configuration like aliases or whatever, feel free
          // to investigate that your self
          $query->joinWith(['owner', 'client', 'project']);

          $dataProvider = new ActiveDataProvider([
               'query' => $query,
          ]);

          // Important: here is how we set up the sorting
          // The key is the attribute name on our "TourSearch" instance
          $dataProvider->sort->attributes['client'] = [
               // The tables are the ones our relation are configured to
               // in my case they are prefixed with "tbl_"
               'asc' => ['clients.cli_id' => SORT_ASC],
               'desc' => ['clients.cli_id' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['owner'] = [
               'asc' => ['users.usr_id' => SORT_ASC],
               'desc' => ['users.usr_id' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['project'] = [
               'asc' => ['projects.pro_id' => SORT_ASC],
               'desc' => ['projects.pro_id' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['status'] = [
               'asc' => ['orders.ord_status' => SORT_ASC],
               'desc' => ['orders.ord_status' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['name'] = [
               'asc' => ['orders.ord_name' => SORT_ASC],
               'desc' => ['orders.ord_name' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['created'] = [
               'asc' => ['orders.ord_created_at' => SORT_ASC],
               'desc' => ['orders.ord_created_at' => SORT_DESC],
          ];
          // No search? Then return data Provider
          if (!($this->load($params) && $this->validate())) {
               return $dataProvider;
          }

          $query->andFilterWhere([
                  ])
                  ->andFilterWhere(['like', 'clients.cli_id', $this->client])
                  ->andFilterWhere(['like', 'users.usr_id', $this->owner])
                  ->andFilterWhere(['like', 'executive.usr_id', $this->executive])
                  ->andFilterWhere(['like', 'projects.pro_id', $this->project])
                  ->andFilterWhere(['like', 'orders.ord_status', $this->status])
                  ->andFilterWhere(['like', 'orders.ord_name', $this->name]);


          if (!empty($this->created)) {
               $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
               if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2) {
                    $date1 = strtotime(trim($matches[0][0]));
                    $date2 = strtotime(trim($matches[0][1]));
                    $query->andFilterWhere(['between', 'ord_created_at', $date1, $date2]);
               }
          }
          // We have to do some search... Lets do some magic

          return $dataProvider;
     }

}
