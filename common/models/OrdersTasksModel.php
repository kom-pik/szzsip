<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use common\models\UsersModel;
use common\models\TasksModel;
use common\models\OrdersModel;
use common\models\StartsStopsModel;
use common\models\MyModel;

/**
 * This is the model class for table "task".
 *
 * @property integer $ort_id
 * @property integer $ort_order_fkey
 * @property integer $ort_task_fkey
 * @property string  $ort_description
 * @property integer $ort_created_at
 * @property integer $ort_updated_at
 * @property integer $ort_created_by
 * @property integer $ort_updated_by
 * @property integer $ort_locked
 * @property integer $ort_status Description
 * @property integer $ort_group_fkey Description
 */
class OrdersTasksModel extends MyModel {

     const TABLE_FIELD_PREFIX = 'ort_';
     
     const LOCKED = 1;
     const UNLOCKED = 0;
     const PAUSED = 2;
     
     const STATUS_DELETED = 0;
     const STATUS_ACTIVE = 1;
     
     const SCENARIO_LOCK_UNLOCK_TASK = 'lock_unlock_task';

     protected $time;

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return 'orders_tasks';
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
              [['ort_status'], 'default', 'value' => self::STATUS_ACTIVE],
              [['ort_locked', 'ort_order_fkey', 'ort_task_fkey', 'ort_locked', 'ort_status', 'ort_group_fkey'], 'integer'],
              ['ort_description', 'string']
          ];
     }

     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function attributeLabels() {
          return [
              'ort_id' => 'ID',
              'ort_description' => 'Opis',
              'ort_created_at' => 'Utworzony',
              'created' => 'Utworzone',
              'ort_created' => 'Utworzony',
              'ort_updated_at' => 'Edytowany',
              'ort_created_by' => 'Utworzył',
              'creator' => 'Utworzył',
              'ort_updated_by' => 'Edytował',
              'time' => 'Czas pracy'
          ];
     }
     
     public static function listStatuses(){
          return [
          self::STATUS_ACTIVE => 'aktywne'
          ];
     }

     public function getCreator() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'ort_created_by'])
                  ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     public function getWorker() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'ort_updated_by'])
                  ->onCondition(['usr_status' => UsersModel::STATUS_ACTIVE]);
     }

     public function getTask() {
          return $this->hasOne(TasksModel::className(), ['tas_id' => 'ort_task_fkey'])
                  ->onCondition(['!=', 'tas_status', TasksModel::STATUS_DELETED]);
     }

     public function getOrder() {
          return $this->hasOne(OrdersModel::className(), ['ord_id' => 'ort_order_fkey'])
                  ->onCondition(['!=', 'ord_status', OrdersModel::STATUS_DELETED]);
     }

     public function getStartStops($startDate, $endDate) {
          return $this->hasMany(StartsStopsModel::className(), ['sts_order_task_fkey' => 'ort_id'])
                  ->onCondition(['!=', 'sts_status', StartsStopsModel::STATUS_DELETED])
                  ->where(['between', 'sts_created_at', $startDate, $endDate])->all();
     }

     public function setTime($time) {
          $this->time = $time;
     }

     public function getTime($startDate = null, $endDate = null) {
          if (empty($startDate)){
               $startDate = 0;
          }
          if (empty($endDate)){
               $endDate = time();
          }
          if (empty($this->getStartStops($startDate, $endDate))) {
               return 0;
          }
          $this->setTime($this->countTimeForOrderTask($startDate, $endDate));
          return $this->time;
     }

     public function lock() {
          if (in_array($this->ort_locked, array(self::UNLOCKED, self::PAUSED, null))) {
               $this->ort_locked = self::LOCKED;
               return $this->save(false);
          }
          return FALSE;
     }

     public function unlock() {
          if (in_array($this->ort_locked, array(self::LOCKED, self::PAUSED))) {
               $this->ort_locked = self::UNLOCKED;
               return $this->save(false);
          }
          return FALSE;
     }

     public function pause() {
          if ($this->ort_locked == self::LOCKED) {
               $this->ort_locked = self::PAUSED;
               return $this->save(false);
          }
          return FALSE;
     }

     public static function template() {
          return '{work}{view}{update}{delete}{add-worktime}';
     }

     public static function workButton($url, $model) {
          $currentUser = Yii::$app->user->identity->id == $model->ort_updated_by;
          $status = in_array($model->ort_locked, [OrdersTasksModel::LOCKED, OrdersTasksModel::PAUSED]);
          if ($status && $currentUser) {
               if ($model->ort_locked == OrdersTasksModel::LOCKED) {
                    $button = '<span class="glyphicon glyphicon-pause"></span>';
                    $title = 'Pauza';
                    $id = 'pause';
               } else {
                    $button = '<span class="glyphicon glyphicon-play"></span>';
                    $title = 'Kontynuuj pracę z zadaniem';
                    $id = 'continue';
               }
          } elseif ($status) {
               $button = '<span class="glyphicon glyphicon-hourglass"></span>';
               $title = 'Obecnie nad tym zadaniem pracuje ' . $model->worker->usr_username;
          } else {
               $button = '<span class="glyphicon glyphicon-play"></span>';
               $title = 'Zacznij pracę z zadaniem';
               $id = 'start';
          }
          return Html::a($button, $url, ['title' => Yii::t('app', $title), 'id' => $id, 'data-pjax' => 0]) .
                  Html::a('<span class="glyphicon glyphicon-stop"></span> ', ['tasks/stop-work', 'id' => $model->ort_id], [
                      'title' => Yii::t('app', 'Stop'),
                      'style' => $model->ort_locked && $currentUser ?: 'display: none',
                      'id' => 'stop',
                      'data-pjax' => 0
          ]);
     }

     public function countTimeForOrderTask($startDate, $endDate) {
          $time = 0;
          $sS = $this->getStartStops($startDate, $endDate);
          if (!empty($sS)){
               $count = count($sS);
               if ($count == 1) {
                    $time += time() - $sS[0]->sts_created_at;
               }
               if ($count >= 2) {
                    for ($i = 0; $i < $count - 1; $i++) {
                         if (in_array($sS[$i]->sts_type, [StartsStopsModel::TYPE_START, StartsStopsModel::TYPE_RESUME])) {
                              $time += $sS[$i + 1]->sts_created_at - $sS[$i]->sts_created_at;
                         }
                    }
                    if (in_array($sS[$count - 1]->sts_type, [StartsStopsModel::TYPE_START, StartsStopsModel::TYPE_RESUME])) {
                         $time += time() - $sS[$count - 1]->sts_created_at;
                    }
               }
          }
          return $time;
     }

}
