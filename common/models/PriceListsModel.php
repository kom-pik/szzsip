<?php

namespace common\models;

use Yii;
use common\models\UsersModel;
use common\models\MyModel;

/**
 * This is the model class for table "price_list".
 *
 * @property integer $prl_id
 * @property string $prl_name
 * @property integer $prl_type
 * @property integer $prl_status Description
 * @property string $prl_description Description
 * @property float $prl_h_in_packet
 * @property float $prl_et_price Description
 * @property float $prl_net_hourly_rate
 * @property float $prl_sla
 * @property integer $prl_created_by
 * @property integer $prl_created_at
 * @property integer $prl_updated_by
 * @property integer $prl_updated_at
 */
class PriceListsModel extends MyModel {

     const TABLE_FIELD_PREFIX = 'prl_';
     
     const STATUS_ACTIVE = 1;
     const STATUS_INACTIVE = 2;
     const STATUS_DELETED = 0;
     
     const TYPE_STANDARD = 1;
     const TYPE_INDIVIDUAL = 2;

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return 'price_lists';
     }

     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               [['prl_type', 'prl_status'], 'integer'],
               [['prl_net_hourly_rate'], 'number', 'min' => 15, 'tooSmall' => Yii::t('app', 'Stawka nie może być niższa niż 15 zł/h netto')],
               [['prl_h_in_packet', 'prl_net_price'], 'number', 'min' => 0, 'tooSmall' => Yii::t('app', '{attribute} nie może być mniejsza od 0')],
               [['prl_sla'], 'number', 'min' => 0, 'tooSmall' => Yii::t('app', 'Czas reakcji nie może być mniejszy od 0')],
               [['prl_name'], 'string', 'max' => 30],
               [['prl_name'], 'unique'],
               ['prl_name', 'required', 'message' => Yii::t('app', 'Nazwa jest wymagana')],
               ['prl_status', 'default', 'value' => self::STATUS_ACTIVE],
               ['prl_type', 'default', 'value' => self::TYPE_STANDARD],
               ['prl_description', 'string', 'max' => 255]
          ];
     }

     public static function listStatuses() {
          return [
               self::STATUS_ACTIVE => Yii::t('app', 'aktywny'),
               self::STATUS_INACTIVE => Yii::t('app', 'nieaktywny'),
               self::STATUS_DELETED => Yii::t('app', 'usunięty'),
          ];
     }

     public static function listTypes() {
          return [
               self::TYPE_STANDARD => Yii::t('app', 'standardowy'),
               self::TYPE_INDIVIDUAL => Yii::t('app', 'indywidualny'),
          ];
     }

     /**
      * @inheritdoc
      */
     public function attributeLabels() {
          return [
               'prl_id' => Yii::t('app', 'ID'),
               'prl_name' => Yii::t('app', 'Nazwa cennika'),
               'name' => Yii::t('app', 'Nazwa cennika'),
               'prl_type' => Yii::t('app', 'Typ cennika'),
               'type' => Yii::t('app', 'Typ cennika'),
               'prl_h_in_packet' => Yii::t('app', 'Liczba godzin w pakiecie'),
               'prl_net_price' => Yii::t('app', 'Cena netto za pakiet'),
               'prl_net_hourly_rate' => Yii::t('app', 'Stawka za roboczogodzinę (PLN netto)'),
               'prl_sla' => Yii::t('app', 'Gwarantowany czas reakcji (h)'),
               'prl_created_by' => Yii::t('app', 'Utworzył'),
               'prl_created' => Yii::t('app', 'Utworzony'),
               'prl_creator' => Yii::t('app', 'Utworzył'),
               'creator' => Yii::t('app', 'Utworzył'),
               'prl_created_at' => Yii::t('app', 'Utworzony'),
               'created' => Yii::t('app', 'Utworzony'),
               'prl_updated_by' => Yii::t('app', 'Edytował'),
               'prl_updated_at' => Yii::t('app', 'Edytowany'),
               'prl_status' => Yii::t('app', 'Status'),
               'prl_description' => Yii::t('app', 'Opis'),
          ];
     }

     public function getCreator() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'prl_created_by']);
     }

     public function getClients() {
          return $this->hasMany(ClientsModel::className(), ['cli_price_list_fkey' => 'prl_id']);
     }

     public static function getAllPriceListsNames($created_by = null, $index = 'prl_name') {
          $query = (new \yii\db\Query)->select(['prl_name'])
                  ->from(self::tableName())
                  ->where(['not in', 'prl_status', [self::STATUS_DELETED, self::STATUS_INACTIVE]])
                  ->indexBy($index)
                  ->orderBy('prl_name');
          if ($created_by) {
               $query->andWhere(['prl_created_by' => $created_by]);
          }
          return $query->orderBy('prl_name')->column();
     }

     public function getShortName() {
          if (strlen($this->prl_name) > 25) {
               return substr($this->prl_name, 0, 22) . '...';
          }
          return $this->prl_name;
     }

}
