<?php

namespace common\models;

use yii\data\ActiveDataProvider;

class PriceListsSearchModel extends PriceListsModel
{

    // add the public attributes that will be used to store the data to be search
    public $creator;
    public $status;
    public $name;
    public $created;
    

    // now set the rules to make those attributes safe
    public function rules()
    {
        return [
            // ... more stuff here
            [['creator', 'status', 'name', 'created'], 'safe'],
            // ... more stuff here
        ];
    }
    
    public function attributeLabels()
    {
        return [
            
            'creator' => Yii::t('app', 'Utworzył'),
            'client' => Yii::t('app', 'Klient'),
            'executive' => Yii::t('app', 'Wykonawca'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Nazwa'),
            'created' => Yii::t('app', 'Utworzony'),
            'project' => Yii::t('app', 'Projekt'),
        ];
    }
// ... model continues here
    public function search($params)
    {
        // create ActiveQuery
        $query = PriceListsModel::find()->having(['<>', 'prl_status', TasksModel::STATUS_DELETED]);
        // Important: lets join the query with our previously mentioned relations
        // I do not make any other configuration like aliases or whatever, feel free
        // to investigate that your self
        $query->joinWith('creator');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance

        $dataProvider->sort->attributes['creator'] = [
            'asc' => ['users.usr_id' => SORT_ASC],
            'desc' => ['users.usr_id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['status'] = [
            'asc' => ['price_lists.prl_status' => SORT_ASC],
            'desc' => ['price_lists.prl_status' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['name'] = [
            'asc' => ['price_lists.prl_name' => SORT_ASC],
            'desc' => ['price_lists.prl_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['created'] = [
            'asc' => ['price_lists.prl_created_at' => SORT_ASC],
            'desc' => ['price_lists.prl_created_at' => SORT_DESC],
        ];
        // No search? Then return data Provider
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
        ])
        ->andFilterWhere(['like', 'users.usr_id', $this->creator])
        ->andFilterWhere(['like', 'price_lists.prl_status', $this->status])
        ->andFilterWhere(['like', 'price_lists.prl_name', $this->name]);
        
        
        if(!empty($this->created)){
            $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
            if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2){
               $date1 = strtotime(trim($matches[0][0]));
               $date2 = strtotime(trim($matches[0][1]));
               $query->andFilterWhere(['between', 'prl_created_at', $date1, $date2]);
            }
        }
        // We have to do some search... Lets do some magic


        return $dataProvider;
    }
}