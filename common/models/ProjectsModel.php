<?php

namespace common\models;

use Yii;
use common\models\MyModel;
use common\models\UsersModel;
use common\models\ClientsModel;
use common\models\EntitiesCostsModel;

/**
 * Project model
 *
 * @property integer $pro_id
 * @property string $pro_name
 * @property integer $pro_owner_fkey
 * @property integer $pro_client_fkey
 * @property integer $pro_group_fkey
 * @property integer $pro_created_at
 * @property integer $pro_updated_at
 * @property integer $pro_created_by
 * @property integer $pro_updated_by
 * @property string $pro_status
 * @property string $pro_type
 * @property string $pro_description
 * @property integer $pro_budget_type
 * @property integer $pro_budget_value
 * 
 */
class ProjectsModel extends MyModel {
     
     const TABLE_FIELD_PREFIX = 'pro_';
     
     const STATUS_NEW = 1;
     const STATUS_IN_PROGRESS = 2;
     const STATUS_CLOSED = 3;
     const STATUS_DELETED = 0;
     
     const PROJECT_TYPE_SERVICE = 1;
     const PROJECT_TYPE_PROGRAM = 2;
     const PROJECT_TYPE_TRAINING = 3;
     const PROJECT_TYPE_SALES = 4;
     const PROJECT_TYPE_IMPLEMENT = 5;
     const PROJECT_TYPE_OTHER = 99;
     
     const BUDGET_TYPE_HOURS = 1;
     const BUDGET_TYPE_AMOUNT = 2;
     
     const SCENARIO_UPDATE_PROJECT = 'scenario_update_project';
     const SCENARIO_ADD_PROJECT = 'scenario_add_project';
     
     const ENTITY_TYPE = EntitiesCostsModel::ENTITY_TYPE_PROJECTS;

     public $budgetUse;
     public $costsValue;
     public $projectTime;
     public $ordersData = array();

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return '{{%projects}}';
     }

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               ['pro_status', 'default', 'value' => self::STATUS_NEW],
               ['pro_owner_fkey', 'default', 'value' => \Yii::$app->user->identity->id],
               ['pro_name', 'required', 'message' => 'Nazwa nie może zostać pusta'],
               ['pro_group_fkey', 'required', 'when' => function(){
                    return $this->user->identity->isAdmin();
               }, 'message' => 'Wybierz grupę'],
               [['pro_name', 'pro_description'], 'string'],
               [['pro_status', 'pro_type', 'pro_budget_type', 'pro_owner_fkey', 'pro_client_fkey', 'pro_group_fkey'], 'integer'],
//               ['pro_group_fkey', 'default', 'value' => $this->user->isAdminOrSupervisor() ? $this->user->supervisedGroup->gro_id : $this->user->group->gro_id],
               ['pro_budget_value', 'number'],
          ];
     }

     public function scenarios() {
          return array_merge(parent::scenarios(), [
               self::SCENARIO_ADD_PROJECT => [
                    'pro_name',
                    'pro_type',
                    'pro_owner_fkey',
                    'pro_client_fkey',
                    'pro_status',
                    'pro_description',
                    'pro_budget_type',
                    'pro_budget_value',
                    'pro_group_fkey'
                    ],
               self::SCENARIO_UPDATE_PROJECT => [
                    'pro_name',
                    'pro_type',
                    'pro_owner_fkey',
                    'pro_client_fkey',
                    'pro_status',
                    'pro_description',
                    'pro_budget_type',
                    'pro_budget_value',
               ]
          ]);
     }

     public function attributeLabels() {
          return [
               'pro_name' => Yii::t('app', 'Nazwa'),
               'name' => Yii::t('app', 'Nazwa'),
               'pro_group_fkey' => Yii::t('app', 'Grupa'),
               'pro_owner_fkey' => Yii::t('app', 'Właściciel projektu'),
               'pro_owner' => Yii::t('app', 'Właściciel projektu'),
               'owner' => Yii::t('app', 'Właściciel projektu'),
               'pro_client_fkey' => Yii::t('app', 'Klient'),
               'pro_client' => Yii::t('app', 'Klient'),
               'client' => Yii::t('app', 'Klient'),
               'pro_status' => Yii::t('app', 'Status'),
               'pro_description' => Yii::t('app', 'Opis'),
               'pro_created_at' => Yii::t('app', 'Utworzony'),
               'pro_created' => Yii::t('app', 'Utworzony'),
               'created' => Yii::t('app', 'Utworzony'),
               'actions' => Yii::t('app', 'Akcje'),
               'pro_type' => Yii::t('app', 'Typ projektu'),
               'pro_budget_type' => Yii::t('app', 'Typ budżetu'),
               'pro_budget_value' => !empty($this->pro_budget_type) ? ($this->pro_budget_type == self::BUDGET_TYPE_HOURS ? Yii::t('app', 'Liczba godzin') : Yii::t('app', 'Wartość w PLN')) : Yii::t('app', 'Wartość w PLN lub godz.'),
               'time' => 'Czas pracy',
               'costs' => 'Koszty',
               'budget_use' => 'Wykorzystanie budżetu'
          ];
     }

     public static function listStatuses() {
          return [
               self::STATUS_NEW => Yii::t('app', 'nowy'),
               self::STATUS_IN_PROGRESS => Yii::t('app', 'w realizacji'),
               self::STATUS_CLOSED => Yii::t('app', 'zamknięty'),
               self::STATUS_DELETED => Yii::t('app', 'usunięty'),
          ];
     }

     public static function listOpenStatuses() {
          return [
               self::STATUS_NEW => Yii::t('app', 'nowy'),
               self::STATUS_IN_PROGRESS => Yii::t('app', 'w realizacji'),
          ];
     }

     public static function listTypes() {
          return [
               self::PROJECT_TYPE_SERVICE => Yii::t('app', 'serwisowy'),
               self::PROJECT_TYPE_PROGRAM => Yii::t('app', 'programistyczny'),
               self::PROJECT_TYPE_TRAINING => Yii::t('app', 'szkoleniowy'),
               self::PROJECT_TYPE_SALES => Yii::t('app', 'sprzedażowy'),
               self::PROJECT_TYPE_IMPLEMENT => Yii::t('app', 'wdrożeniowy'),
               self::PROJECT_TYPE_OTHER => Yii::t('app', 'inny'),
          ];
     }

     public static function listBudgetTypes() {
          return [
               self::BUDGET_TYPE_HOURS => 'godzinowy',
               self::BUDGET_TYPE_AMOUNT => 'kwotowy'
          ];
     }

     public static function getAllProjectsNames($client_id = null, $owner_id = null, $index = 'pro_name', $group_id = null) {
          $query = (new \yii\db\Query)->select(['pro_name'])
                  ->from(self::tableName())
                  ->where(['!=', 'pro_status', self::STATUS_DELETED]);
          if (!empty($client_id)) {
               $query->andWhere(['pro_client_fkey' => $owner_id]);
          }
          if (!empty($owner_id)) {
               $query->andWhere(['pro_owner_fkey' => $client_id]);
          }
          if (!empty($group_id)) {
               $query->andWhere(['pro_group_fkey' => $group_id]);
          }
          
          $query->indexBy($index)->orderBy('pro_name');
          return $query->column();
     }

     public static function getAllProjects($cli_ids = null, $group_id = null) {
          $query = (new \yii\db\Query)->select(['pro_name'])
                  ->from(self::tableName())
                  ->where(['not in', 'pro_status', [self::STATUS_DELETED, self::STATUS_CLOSED]]);
          if (!empty($cli_ids)) {
               $query->andWhere(['in', 'pro_client_fkey', (array) $cli_ids]);
          }
          if (!empty($group_id)) {
               $query->andWhere(['pro_group_fkey' => $group_id]);
          }
          $query->indexBy('pro_id')->orderBy('pro_name');
          return $query->column();
     }

     /**
      * returns trimmed name of project name
      * @return string
      */
     public function getShortName($charsCount = 25) {
          if (strlen($this->pro_name) > $charsCount) {
               return substr($this->pro_name, 0, 22) . '...';
          }
          return $this->pro_name;
     }

     /**
      * 
      * @return ClientsModel
      */
     public function getClient() {
          return $this->hasOne(ClientsModel::className(), ['cli_id' => 'pro_client_fkey'])
                          ->onCondition(['!=', 'cli_status', ClientsModel::STATUS_DELETED]);
     }

     /**
      * 
      * @return UsersModel
      */
     public function getOwner() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'pro_owner_fkey'])
                          ->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED]);
     }

     /**
      * 
      * @param int $startDate
      * @param int $endDate
      * @return OrdersModel[]
      */
     public function getOrders() {
          return $this->hasMany(OrdersModel::className(), ['ord_project_fkey' => 'pro_id'])
                          ->onCondition(['!=', 'ord_status', OrdersModel::STATUS_DELETED]);
     }

     /**
      * returns array of total times for orders as ord_id => time
      * @param int $startDate
      * @param int $endDate
      * @return array
      */
     public function getProjectOrdersTime($startDate = null, $endDate = null) {
          $time = [];
          if (empty($startDate)) {
               $startDate = 0;
          }
          if (empty($endDate)) {
               $endDate = time();
          }
          $orders = $this->getOrders($startDate, $endDate);
          if (!empty($orders)) {
               $time = array_map(function($po) use ($startDate, $endDate) {
                    return ['ord_id' => $po->ord_id, 'time' => $po->getOrderTime($startDate, $endDate)];
               }, $orders);
          }
          return $time;
     }
     
     public function setProjectTime($time){
          $this->projectTime = $time;
     }

     /**
      * returns total time for project
      * @return integer
      */
     public function getProjectTime($startDate = null, $endDate = null) {
          $time = [];
          if (empty($startDate)) {
               $startDate = 0;
          }
          if (empty($endDate)) {
               $endDate = time();
          }
          if (!empty($this->orders)) {
               $time = array_map(function($order) use ($startDate, $endDate) {
                    return $order->getOrderTime($startDate, $endDate);
               }, $this->orders);
          }
          $this->setProjectTime(array_sum($time));
          return $this->projectTime;
     }

     /**
      * 
      * @param type $budgetUse
      */
     public function setBudgetUse($budgetUse) {
          $this->budgetUse = $budgetUse;
     }

     /**
      * 
      * @param int $startDate
      * @param type $endDate
      * @return float
      */
     public function getBudgetUse($startDate = null, $endDate = null) {
          $costs = 0;
          if (empty($startDate)) {
               $startDate = 0;
          }
          if (empty($endDate)) {
               $endDate = time();
          }

          $projectTime = $this->getProjectTime($startDate, $endDate);
          if (!empty($this->pro_budget_value) && $this->pro_budget_value > 0){
               if ($this->pro_budget_type == self::BUDGET_TYPE_AMOUNT ) {
                    $costs = $this->getCostsValue($startDate, $endDate);
                    if (!empty($this->client)){
                         $priceList = $this->client->priceList;                         
                    }
                    //adding cost of time spent on project
                    if (!empty($priceList) && !empty($projectTime)) {
                         $costs += ($projectTime / 3600) * $priceList->prl_net_hourly_rate;
                    }
                    $this->setBudgetUse($costs / $this->pro_budget_value);
               } else {
                    $this->setBudgetUse($projectTime / ($this->pro_budget_value * 3600));
               }
               return $this->budgetUse;
          }
          return 0;
     }

     /**
      * 
      * @param int $startDate
      * @param int $endDate
      * @return EntitiesCostsModel[]
      */
     public function getCosts($startDate, $endDate) {
          return $this->hasMany(EntitiesCostsModel::className(), ['enc_entity_fkey' => $this->idAttribute])
                          ->onCondition(['!=', 'enc_status', EntitiesCostsModel::STATUS_DELETED])
                          ->andOnCondition(['enc_entity_type' => static::ENTITY_TYPE])
                          ->where(['between', 'enc_created_at', $startDate, $endDate])
                          ->all();
     }
     
     public function setCostsValue($costValue){
          $this->costsValue = $costValue;
          return $this;
     }

     public function getCostsValue($startDate = null, $endDate = null) {
          if (empty($startDate)) {
               $startDate = 0;
          }
          if (empty($endDate)) {
               $endDate = time();
          }
          $costsValues = [0];
          $costs = $this->getCosts($startDate, $endDate);
          if (!empty($costs)) {
               $costsValues = array_map(function($cost) {
                    return $cost->enc_net_value;
               }, $costs);
          }
          if ($this->className() == ProjectsModel::className()){
               $orders = $this->orders;
               if (!empty($orders)){
                    $ordersCosts = array_map(function($order) use ($startDate, $endDate){
                         return $order->getCosts($startDate, $endDate);
                    }, $orders);
                    if (!empty($ordersCosts[0])){
                         $ordersCostsValues = array_map(function($cost) {
                              
                              return !empty($cost) ? $cost[0]->enc_net_value : null;
                         }, $ordersCosts);
                         $costsValues = array_merge($costsValues, $ordersCostsValues);
                    }                    
               }
          }
          $this->setCostsValue(array_sum($costsValues));
          return $this->costsValue;
     }
     
     /**
      * 
      * @param type $id
      * @param type $group_id
      * @param type $condition
      * @return ProjectsModel
      */
     public function findProject($id, $group_id, $condition = null){
          $query = static::find()->where(['pro_id' => $id]);
          if (!empty($group_id)){
               $query->onCondition(['pro_group_fkey' => $group_id]);
          }
          if (!empty($condition)){
               $query->andWhere($condition);
          }
          return $query->one();
     }

}
