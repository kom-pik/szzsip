<?php

namespace common\models;

use yii\data\ActiveDataProvider;

class ProjectsSearchModel extends ProjectsModel {

     // add the public attributes that will be used to store the data to be search
     public $owner;
     public $client;
     public $status;
     public $name;
     public $created;
     public $orders;
     public $time;

     public function attributeLabels() {
          return [
               'Owner' => Yii::t('app', 'Właściciel projektu'),
          ];
     }

     // now set the rules to make those attributes safe
     public function rules() {
          return [
               // ... more stuff here
               [['owner', 'client', 'status', 'name', 'created', 'orders'], 'safe'],
                  // ... more stuff here
          ];
     }

// ... model continues here
     public function search($params, $client_ids = null, $owner_id = null, $qroup_id = null) {
          // create ActiveQuery
          $query = ProjectsModel::find()->having(['!=', 'pro_status', ProjectsModel::STATUS_DELETED])
                  ->orderBy('pro_name', 'pro_status');
          // Important: lets join the query with our previously mentioned relations
          // I do not make any other configuration like aliases or whatever, feel free
          // to investigate that your self
          $query->joinWith(['owner', 'client', 'orders']);
          
          if (!empty($client_ids)) {
               $query->where(['in', 'pro_client_fkey', $client_ids]);
          }
          if (!empty($owner_id)) {
               $query->where(['pro_owner_fkey' => $owner_id]);
          }
          if (!empty($qroup_id)){
               $query->where(['pro_group_fkey' => $qroup_id]);
          }
          $dataProvider = new ActiveDataProvider([
               'query' => $query,
          ]);

          // Important: here is how we set up the sorting
          // The key is the attribute name on our "TourSearch" instance
          $dataProvider->sort->attributes['client'] = [
               // The tables are the ones our relation are configured to
               // in my case they are prefixed with "tbl_"
               'asc' => ['clients.cli_id' => SORT_ASC],
               'desc' => ['clients.cli_id' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['owner'] = [
               'asc' => ['users.usr_id' => SORT_ASC],
               'desc' => ['users.usr_id' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['status'] = [
               'asc' => ['projects.pro_status' => SORT_ASC],
               'desc' => ['projects.pro_status' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['name'] = [
               'asc' => ['projects.pro_name' => SORT_ASC],
               'desc' => ['projects.pro_name' => SORT_DESC],
          ];
          $dataProvider->sort->attributes['created'] = [
               'asc' => ['projects.pro_created_at' => SORT_ASC],
               'desc' => ['projects.pro_created_at' => SORT_DESC],
          ];

//        $dataProvider->sort->attributes['orders'] = [
//            'asc' => ['orders.id' => SORT_ASC],
//            'desc' => ['orders.id' => SORT_DESC],
//        ];
          // No search? Then return data Provider
          if (!($this->load($params) && $this->validate())) {
               return $dataProvider;
          }

          $query->andFilterWhere([
                  ])
                  ->andFilterWhere(['like', 'clients.cli_id', $this->client])
                  ->andFilterWhere(['like', 'users.usr_id', $this->owner])
                  ->andFilterWhere(['like', 'projects.pro_status', $this->status])
                  ->andFilterWhere(['like', 'projects.pro_name', $this->name])
//        ->andFilterWhere(['like', 'orders.id', $this->order])
          ;

          if (!empty($this->created)) {
               $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
               if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2) {
                    $date1 = strtotime(trim($matches[0][0]));
                    $date2 = strtotime(trim($matches[0][1]));
                    $query->andFilterWhere(['between', 'pro_created_at', $date1, $date2]);
               }
          }
          // We have to do some search... Lets do some magic


          return $dataProvider;
     }

}
