<?php

namespace common\models;

use common\models\ClientsModel;
use common\models\GroupsModel;
use common\models\MyModel;
use frontend\services\ServiceReporting;

/**
 * Description of ReportModel
 *
 * @author piqs
 */
class ReportModel extends MyModel {

     public $start_date;
     public $end_date;
     public $type;
     public $cli_id;
     public $gro_id;

     const REPORT_TYPE_ORDERS = 'type_orders';
     const REPORT_TYPE_PROJECTS = 'type_projects';
     const REPORT_TYPE_COSTS = 'type_costs';

     static $reportsTypes = [
          self::REPORT_TYPE_ORDERS => 'zleceń',
          self::REPORT_TYPE_PROJECTS => 'projektów',
          self::REPORT_TYPE_COSTS => 'kosztów'
     ];

     public function attributeLabels() {
          return [
               'start_date' => 'Od',
               'end_date' => 'Do',
               'type' => 'Wybierz rodzaj raportu'
          ];
     }

     public function rules() {
          return [
               [['start_date', 'end_date'], 'required'],
               [['type', 'start_date', 'end_date'], 'string'],
               [['cli_id', 'gro_id'], 'integer']
          ];
     }

     public function generateReport() {
          if (!empty($this->cli_id)){
               switch ($this->type) {
                    case self::REPORT_TYPE_PROJECTS:
                         return $this->generateProjectsReport();
                    case self::REPORT_TYPE_ORDERS:
                         return $this->generateOrdersReport();
                    case self::REPORT_TYPE_COSTS:
                         return $this->generateCostsReport();
                    default:
                         return $this->generateProjectsReport();
               }               
          }
          if (!empty($this->gro_id)){
               switch ($this->type) {
                    case self::REPORT_TYPE_PROJECTS:
                         return $this->generateProjectsReportForGroup();
                    case self::REPORT_TYPE_ORDERS:
                         return $this->generateOrdersReportForGroup();
                    case self::REPORT_TYPE_COSTS:
                         return $this->generateCostsReportForGroup();
                    default:
                         return $this->generateProjectsReportForGroup();
               }
          }
     }

     public function generateProjectsReport($costValueSource = EntitiesCostsModel::ENTITY_TYPE_PROJECTS) {
          $client = ClientsModel::findOne([$this->cli_id]);
          $this->prepareDates();
          $clientProjects = $client->getProjects($this->start_date, $this->end_date);

          if (!empty($clientProjects)) {
               $clientProjectsData = ServiceReporting::prepareProjectsData($clientProjects, $this);
          }
          $client->getCostsValue($this->start_date, $this->end_date, $costValueSource);
          $client->getClientTime($this->start_date, $this->end_date);
          return [
               'view' => 'report-projects',
               'params' => [
                    'reportModel' => $this,
                    'client' => $client,
                    'projectsData' => !empty($clientProjectsData) ? $clientProjectsData : array(),
                    'totalCost' => $client->costsValue,
                    'totalTime' => $client->clientTime
               ]
          ];
     }
     
     public function generateOrdersReport($client = null, $prepareDates = true) {
          if ($prepareDates){
               $this->prepareDates();
          }
          if (empty($client)){
               $client = ClientsModel::findOne([$this->cli_id]);
               $client->getCostsValue($this->start_date, $this->end_date, EntitiesCostsModel::ENTITY_TYPE_ORDERS);
               $client->getClientTime($this->start_date, $this->end_date);         
          }
          $clientOrders = $client->getOrders($this->start_date, $this->end_date);
          if (!empty($clientOrders)) {
               $clientOrdersData = ServiceReporting::prepareOrdersData($clientOrders, $this);
          }

          return [
               'view' => 'report-orders',
               'params' => [
                    'reportModel' => $this,
                    'client' => $client,
                    'ordersData' => !empty($clientOrdersData) ? $clientOrdersData : array(),
                    'totalCost' => $client->costsValue,
                    'totalTime' => $client->clientTime
               ]
          ];
     }
     
     public function generateCostsReport(){
          $client = ClientsModel::findOne([$this->cli_id]);
          $this->prepareDates();
          $client->getCosts($this->start_date, $this->end_date);
          if (!empty($client->costs)){
               $costsValue = array_map(function($cost){
                    return $cost['cost_group_value'];
               }, $client->costs);
               $client->costsValue = array_sum($costsValue);
          }
          return [
               'view' => 'report-costs',
               'params' => [
                    'reportModel' => $this,
                    'client' => $client,
               ]
          ];
     }

     private function prepareDates(){
          if (empty($this->start_date)) {
               $this->start_date = 0;
          } else {
               $this->start_date = strtotime($this->start_date);
          }
          if (empty($this->end_date)) {
               $this->end_date = time();
          } else {
               $this->end_date = strtotime($this->end_date . ' +1 day') - 1;
          }
     }
     
     public function generateProjectsReportForGroup(){
          $this->prepareDates();
          $group = GroupsModel::findOne([$this->gro_id]);
          
          if (!empty($group)){
               $groupProjects = $group->getProjects($this->start_date, $this->end_date);
               if (!empty($groupProjects)){
                    $groupProjectsData = ServiceReporting::prepareProjectsData($groupProjects, $this, true);
                    $totalTime = $groupProjectsData['projectsTime'];
                    $totalCost = $groupProjectsData['projectsCost'];
                    unset($groupProjectsData['projectsTime']);
                    unset($groupProjectsData['projectsCost']);
               }
          }
          return [
               'view' => 'report-projects',
               'params' => [
                    'reportModel' => $this,
                    'group' => $group,
                    'projectsData' => !empty($groupProjectsData) ? $groupProjectsData : array(),
                    'totalTime' => !empty($totalTime) ? $totalTime : 0,
                    'totalCost' => !empty($totalCost) ? $totalCost : 0,
               ]
          ];
     }
     
     public function generateOrdersReportForGroup($group = null, $prepareDates = true){
          if ($prepareDates){
               $this->prepareDates();               
          }
          if (empty($group)){
               $group = GroupsModel::findOne([$this->gro_id]);               
          }

          if (!empty($group)){
               $groupOrders = $group->getOrders($this->start_date, $this->end_date);
               if (!empty($groupOrders)){
                    $groupOrdersData = ServiceReporting::prepareOrdersData($groupOrders, $this, true);
                    $totalTime = $groupOrdersData['ordersTime'];
                    $totalCost = $groupOrdersData['ordersCost'];
                    unset($groupOrdersData['ordersTime']);
                    unset($groupOrdersData['ordersCost']);
               }
          }

          return [
               'view' => 'report-orders',
               'params' => [
                    'reportModel' => $this,
                    'group' => $group,
                    'totalTime' => !empty($totalTime) ? $totalTime : 0,
                    'totalCost' => !empty($totalCost) ? $totalCost : 0,
                    'ordersData' => !empty($groupOrdersData) ? $groupOrdersData : array()
               ]
          ];
     }
     
     /**
      * @todo costs report for group
      */
     public function generateCostsReportForGroup(){
          $this->prepareDates();
          $group = GroupsModel::findOne([$this->gro_id]);  
          
          return [
               'view' => 'report-orders',
               'params' => [
                    'reportModel' => $this,
                    'group' => $group,
                    'totalTime' => !empty($totalTime) ? $totalTime : 0,
                    'totalCost' => !empty($totalCost) ? $totalCost : 0,
                    'ordersData' => !empty($groupOrdersData) ? $groupOrdersData : array()
               ]
          ];
     }

}
