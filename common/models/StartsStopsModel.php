<?php

namespace common\models;

use common\models\OrdersTasksModel;
use common\models\MyModel;

/**
 * This is the model class for table "start_stop".
 *
 * @property integer $sts_id
 * @property integer $sts_order_task_fkey
 * @property integer $sts_created_at
 * @property integer $sts_created_by
 * @property integer $sts_updated_at
 * @property integer $sts_updated_by
 * @property integer $sts_type
 * @property integer $sts_status Description
 */
class StartsStopsModel extends MyModel{

     const TABLE_FIELD_PREFIX = 'sts_';
     
     const TYPE_STOP = 0;
     const TYPE_START = 1;
     const TYPE_PAUSE = 2;
     const TYPE_RESUME = 3;
     const TYPE_CLOSE = 4;
     
     const STATUS_ACTIVE = 1;
     const STATUS_DELETED = 0;

     public static $types = array (
          'start-work' => self::TYPE_START,
          'stop-work' => self::TYPE_STOP,
          'pause-work' => self::TYPE_PAUSE
     );

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return 'starts_stops';
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
              ['sts_status', 'default', 'value' => self::STATUS_ACTIVE],
              [['sts_id', 'sts_order_task_fkey', 'sts_type', 'sts_status'], 'integer'],
          ];
     }

     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function attributeLabels() {
          return [
              'id' => 'ID',
              'order_task_id' => 'Order Task ID',
              'created_at' => 'Created At',
              'created_by' => 'Created By',
              'updated_at' => 'Updated At',
              'updated_by' => 'Updated By',
              'type' => 'Type',
          ];
     }

     public function getOrderTasks() {
          return $this->hasMany(OrdersTasksModel::className(), ['id' => 'order_task_id'])
                  ->onCondition(['!=', 'ort_status', OrdersTasksModel::STATUS_DELETED]);
     }
     
     public function saveStartStop($ort_id, $saveType = 'start-work') {
          $orderTask = OrdersTasksModel::findOne([$ort_id]);
          if (!empty($orderTask)) {
               if (!empty(self::$types[$saveType])) {
                    return $this->saveTime($orderTask, self::$types[$saveType]);
               }
          }
          return false;
     }
     
     public function saveTime(OrdersTasksModel $orderTask, $type) {
          $this->sts_order_task_fkey = $orderTask->ort_id;
          $this->sts_type = $type;
          $this->sts_status = StartsStopsModel::STATUS_ACTIVE;
          switch ($type) {
               case self::TYPE_STOP:
                    return $orderTask->unlock() && $this->save();
               case self::TYPE_PAUSE:
                    return $orderTask->pause() && $this->save();
               case self::TYPE_START:
                    return $orderTask->lock() && $this->save();
          }
     }
}
