<?php

namespace common\models;

use Yii;
use common\models\UsersModel;
use common\models\OrdersTasksModel;
use common\models\MyModel;
use common\models\StartsStopsModel;

/**
 * This is the model class for table "task".
 *
 * @property integer $tas_id
 * @property string $tas_name
 * @property string $tas_description
 * @property string $tas_status
 * @property integer $tas_created_at
 * @property integer $tas_updated_at
 * @property integer $tas_created_by
 * @property integer $tas_updated_by
 * @property integer $tas_group_fkey
 */
class TasksModel extends MyModel {

     const TABLE_FIELD_PREFIX = 'tas_';
     const STATUS_ACTIVE = 1;
     const STATUS_DELETED = 0;

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return 'tasks';
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               [['tas_status'], 'default', 'value' => self::STATUS_ACTIVE],
               [['tas_name'], 'string'],
               ['tas_description', 'string'],
               ['tas_name', 'required', 'message' => 'Nazwa zadania nie może być pusta'],
               ['tas_group_fkey', 'integer'],
               ['tas_group_fkey', 'required', 'when' => function(){
                    return !$this->user->identity->isAdmin();
               }, 'message' => $this->messageRequired],
          ];
     }

     public static function listStatuses() {
          return [
               self::STATUS_ACTIVE => Yii::t('app', 'aktywne'),
               self::STATUS_DELETED => Yii::t('app', 'usunięte'),
          ];
     }

     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function attributeLabels() {
          return [
               'tas_id' => 'ID',
               'tas_name' => 'Nazwa zadania',
               'name' => 'Nazwa zadania',
               'tas_description' => 'Opis',
               'tas_task' => 'Nazwa zadania',
               'tas_status' => 'Status',
               'tas_created_at' => 'Utworzone',
               'tas_created' => 'Utworzone',
               'created' => 'Utworzone',
               'tas_updated_at' => 'Edytowany',
               'tas_created_by' => 'Utworzył',
               'creator' => 'Utworzył',
               'tas_updated_by' => 'Edytował',
               'tas_group_fkey' => 'Grupa'
          ];
     }

     public function getCreator() {
          return $this->hasOne(UsersModel::className(), ['usr_id' => 'tas_created_by']);
     }

     public static function getAllTasksNames($id = null, $group_id = null) {
          $query = new \yii\db\Query();
//          if (!empty($id)) {
//               $query->select('t.tas_name')
//                       ->from(OrdersTasksModel::tableName() . ' ort')
//                       ->leftJoin(self::tableName() . ' t', 'ort_task_fkey = tas_id')
//                       ->where(['ort_order_fkey' => $id])->indexBy('tas_name');
//               
//          } else {
               $query->select(['tas_name'])
                       ->from(self::tableName())
                       ->indexBy('tas_id');
//          }
          $query->where(['!=', 'tas_status', self::STATUS_DELETED]);
          if (!empty($group_id)){
               $query->where(['tas_group_fkey' => $group_id]);
          }
//          dd($query->createCommand());
          return $query->column();
     }

     public function getShortName() {
          if (strlen($this->tas_name) > 25) {
               return substr($this->tas_name, 0, 22) . '...';
          }
          return $this->tas_name;
     }

     public function getOrdersTasks() {
          return $this->hasMany(OrdersTasksModel::className(), ['ort_task_fkey' => 'tas_id']);
     }

     public function getOrders() {
          return $this->hasMany(OrdersModel::className(), ['tas_id' => 'tas_order_fkey'])
                          ->via('orderTasks');
     }

     public function getStartStops() {
          return $this->hasMany(StartsStopsModel::className(), ['sts_task_fkey' => 'tas_id'])
                          ->via('ordersTasks');
     }

}
