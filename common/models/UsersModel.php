<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use common\models\MyModel;
use yii\web\IdentityInterface;
use common\models\ProjectsModel;
use common\models\TasksModel;
use common\models\GroupsModel;

/**
 * User model
 *
 * @property integer $usr_id
 * @property string $usr_username
 * @property string $usr_firstname
 * @property string $usr_lastname
 * @property string $usr_client_fkey
 * @property string $usr_phone
 * @property string $usr_password_hash
 * @property string $usr_password_reset_token
 * @property string $usr_email
 * @property string $usr_auth_key
 * @property integer $usr_status
 * @property integer $usr_created_at
 * @property integer $usr_updated_at
 * @property integer $usr_group_fkey
 * @property string $usr_type Description
 * @property string $usr_password write-only password
 */
class UsersModel extends MyModel implements IdentityInterface {

     const TABLE_FIELD_PREFIX = 'usr_';
     
     const STATUS_ACTIVE = 'active';
     const STATUS_DELETED = 'deleted';
     const STATUS_LOCKED = 'locked';
     
     const TYPE_ADMIN = 'admin';
     const TYPE_SUPERVISOR = 'supervisor';
     const TYPE_SERVICEMAN = 'serviceman';
     const TYPE_CLIENT = 'client';
     
     const SCENARIO_UPDATE = 'scenario_update';
     const SCENARIO_ADD = 'scenario_add';

     public $name;
     private $currentPassword;
     private $newPassword = '';
     private $newPasswordConfirm = '';

     /**
      * @inheritdoc
      */
     public static function tableName() {
          return '{{%users}}';
     }

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return parent::behaviors();
     }

     /**
      * @inheritdoc
      */
     public function rules() {
          return [
               ['usr_status', 'default', 'value' => self::STATUS_ACTIVE],
               [['usr_username', 'usr_email'], 'unique'],
               ['usr_email', 'email'],
               [['usr_firstname', 'usr_lastname', 'usr_username', 'usr_type', 'usr_phone', 'newPassword', 'newPasswordConfirm', 'currentPassword'], 'string'],
               [['usr_client_fkey', 'usr_created_at', 'usr_updated_at', 'usr_created_by', 'usr_updated_by', 'usr_group_fkey'], 'integer'],
               ['newPasswordConfirm', 'compare', 'compareAttribute' => 'newPassword', 'message' => \Yii::t('app', 'Wprowadzone hasła nie są takie same')],
               [['usr_username', 'newPassword', 'newPasswordConfirm', 'usr_type'], 'required', 'on' => self::SCENARIO_ADD, 'message' => $this->messageRequired],
               [['usr_type'], 'required', 'message' => $this->messageRequired],
               ['usr_group_fkey', 'required', 'when' => function($model){
                    return !$model->isAdmin();
               }, 'message' => $this->messageRequired]
//            [['currentPassword'], 'required', 'when' => function($model) {
//                return $model->newPassword != '';
//            }]
          ];
     }

     public function scenarios() {
          return array_merge(parent::scenarios(), [
               self::SCENARIO_ADD => [
                    'usr_username', 
                    'newPassword', 
                    'newPasswordConfirm', 
                    'usr_type', 
                    'usr_group_fkey',
                    'usr_email',
                    'usr_firstname', 
                    'usr_lastname',
                    'usr_phone',
                    'usr_client_fkey',
                    ],
               self::SCENARIO_UPDATE => [
                    'currentPassword', 
                    'newPassword', 
                    'newPasswordConfirm',
                    'usr_status',
                    ]
          ]);
     }

     public static function listStatuses(UsersModel $user = null) {
          if (!empty($user) && $user->isClient()) {
               return [
                    self::STATUS_ACTIVE => Yii::t('app', 'aktywny'),
                    self::STATUS_LOCKED => Yii::t('app', 'zablokowany'),
               ];
          }
          return [
               self::STATUS_ACTIVE => Yii::t('app', 'aktywny'),
               self::STATUS_DELETED => Yii::t('app', 'usunięty'),
               self::STATUS_LOCKED => Yii::t('app', 'zablokowany'),
          ];
     }

     public static function listStatuesWhenClient() {
          
     }

     public static function listTypes() {
          return [
               self::TYPE_ADMIN => Yii::t('app', 'administrator'),
               self::TYPE_SUPERVISOR => Yii::t('app', 'kierownik'),
               self::TYPE_SERVICEMAN => Yii::t('app', 'pracownik'),
               self::TYPE_CLIENT => Yii::t('app', 'klient'),
          ];
     }
     
     public static function listTypesWhenSupervisor(){
          return [
               self::TYPE_SERVICEMAN => Yii::t('app', 'pracownik'),
               self::TYPE_CLIENT => Yii::t('app', 'klient'),
          ];
     }

     public function attributeLabels() {
          return [
               'usr_firstname' => Yii::t('app', 'Imię'),
               'usr_lastname' => Yii::t('app', 'Nazwisko'),
               'usr_username' => Yii::t('app', 'Login'),
               'username' => Yii::t('app', 'Login'),
               'usr_phone' => Yii::t('app', 'Telefon'),
               'usr_status' => Yii::t('app', 'Status'),
               'usr_email' => Yii::t('app', 'E-mail'),
               'usr_created_at' => Yii::t('app', 'Utworzony'),
               'usr_created' => Yii::t('app', 'Utworzony'),
               'created' => Yii::t('app', 'Utworzony'),
               'usr_updated_at' => Yii::t('app', 'Edytowany'),
               'usr_type' => Yii::t('app', 'Typ'),
               'type' => Yii::t('app', 'Typ'),
               'usr_name' => Yii::t('app', 'Imię i nazwisko'),
               'name' => Yii::t('app', 'Imię i nazwisko'),
               'currentPassword' => Yii::t('app', 'Obecne hasło'),
               'newPassword' => Yii::t('app', 'Nowe hasło'),
               'newPasswordConfirm' => Yii::t('app', 'Potwierdź nowe hasło'),
               'usr_group_fkey' => Yii::t('app', 'Grupa'),
               'usr_client_fkey' => Yii::t('app', 'Klient'),
          ];
     }

     /**
      * @inheritdoc
      */
     public static function findIdentity($id) {
          return static::findOne(['usr_id' => $id, ['in', 'status', [self::STATUS_ACTIVE, self::STATUS_LOCKED]]]);
     }

     /**
      * @inheritdoc
      */
     public static function findIdentityByAccessToken($token, $type = null) {
          throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
     }

     /**
      * Finds user by username
      *
      * @param string $username
      * @return static|null
      */
     public static function findByUsername($username) {
          return static::findOne(['usr_username' => $username, 'usr_status' => self::STATUS_ACTIVE]);
     }

     /**
      * Finds user by password reset token
      *
      * @param string $token password reset token
      * @return static|null
      */
     public static function findByPasswordResetToken($token) {
          if (!static::isPasswordResetTokenValid($token)) {
               return null;
          }

          return static::findOne([
                       'usr_password_reset_token' => $token,
                       'usr_status' => self::STATUS_ACTIVE,
          ]);
     }

     /**
      * Finds out if password reset token is valid
      *
      * @param string $token password reset token
      * @return bool
      */
     public static function isPasswordResetTokenValid($token) {
          if (empty($token)) {
               return false;
          }

          $timestamp = (int) substr($token, strrpos($token, '_') + 1);
          $expire = Yii::$app->params['users.usr_passwordResetTokenExpire'];
          return $timestamp + $expire >= time();
     }

     /**
      * @inheritdoc
      */
     public function getId() {
          return $this->getPrimaryKey();
     }

     /**
      * @inheritdoc
      */
     public function getAuthKey() {
          return $this->usr_auth_key;
     }

     /**
      * @inheritdoc
      */
     public function validateAuthKey($authKey) {
          return $this->getAuthKey() === $authKey;
     }

     /**
      * Validates password
      *
      * @param string $password password to validate
      * @return bool if password provided is valid for current user
      */
     public function validatePassword($password) {
          return Yii::$app->security->validatePassword($password, $this->usr_password_hash);
     }

     /**
      * Generates password hash from password and sets it to the model
      *
      * @param string $password
      */
     public function setPassword($password) {
          $this->usr_password_hash = Yii::$app->security->generatePasswordHash($password);
     }

     public function getCurrentPassword() {
          return $this->currentPassword;
     }

     public function setCurrentPassword($currentPassword) {
          $this->currentPassword = $currentPassword;
     }

     public function getNewPassword() {
          return $this->newPassword;
     }

     public function setNewPassword($newPassword) {
          $this->newPassword = $newPassword;
     }

     public function getNewPasswordConfirm() {
          return $this->newPasswordConfirm;
     }

     public function setNewPasswordConfirm($newPasswordConfirm) {
          $this->newPasswordConfirm = $newPasswordConfirm;
     }

     /**
      * Generates "remember me" authentication key
      */
     public function generateAuthKey() {
          $this->usr_auth_key = Yii::$app->security->generateRandomString();
     }

     /**
      * Generates new password reset token
      */
     public function generatePasswordResetToken() {
          $this->usr_password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
     }

     /**
      * Removes password reset token
      */
     public function removePasswordResetToken() {
          $this->usr_password_reset_token = null;
     }

     public function isAdmin() {
          return $this->usr_type == self::TYPE_ADMIN;
     }

     public function isSupervisor() {
          return $this->usr_type == self::TYPE_SUPERVISOR;
     }

     public function isAdminOrSupervisor() {
          return in_array($this->usr_type, [
               self::TYPE_ADMIN,
               self::TYPE_SUPERVISOR
          ]);
     }

     public function isServiceman() {
          return $this->usr_type == self::TYPE_SERVICEMAN;
     }

     public function isClient() {
          return $this->usr_type == self::TYPE_CLIENT;
     }
     
     public function findUserById($id, $group_id = null){
          $user = static::find()->onCondition(['usr_id' => $id])
                  ->where(['!=', 'usr_status', self::STATUS_DELETED]);
          if (!empty($group_id)){
               $user->andWhere(['usr_group_fkey' => $group_id]);
          }
          return $user->one();
     }

     public static function findAllUsers($group_id = null, $condition = null) {
          $query = (new \yii\db\Query)->select(['usr_username'])
                  ->from(self::tableName().' u')
                  ->where(['not in', 'usr_status', [self::STATUS_DELETED, self::STATUS_LOCKED]]);
          if (!empty($group_id)){
               $query->andWhere(['usr_group_fkey' => $group_id]);
          }
          if (!empty($condition)) {
               $query->andWhere($condition);
          }
          $query->indexBy('usr_id');
          return $query->column();
     }

     public static function findAllUsersClients() {
          $query = (new \yii\db\Query)->select(['usr_username'])
                  ->from(self::tableName())
                  ->where(['!=', 'usr_status', UsersModel::STATUS_DELETED])
                  ->andWhere(['usr_type' => UsersModel::TYPE_CLIENT])
                  ->indexBy('usr_id')
                  ->column();
          return $query;
     }

     public function getProjects() {
          return $this->hasMany(ProjectsModel::className(), ['usr_id' => 'pro_owner_fkey'])
                  ->onCondition(['!=', 'pro_status', ProjectsModel::STATUS_DELETED]);
     }

     public function getTasks() {
          return $this->hasMany(TasksModel::className(), ['usr_id' => 'pro_created_by'])
                  ->onCondition(['!=', 'tas_status', TasksModel::STATUS_DELETED]);
     }

     public function setName($name) {
          $this->name = $name;
          return $this;
     }

     public function getName() {
          if (empty($this->usr_firstname) && empty($this->usr_lastname)) {
               $this->setName($this->usr_username);
          } else {
               $name = $this->usr_firstname . ' ' . $this->usr_lastname;
               $this->setName($name);               
          }
          return $this->name;
     }

     public function getCompany() {
          return $this->hasOne(ClientsModel::className(), ['cli_id' => 'usr_client_fkey'])
                  ->onCondition(['!=', 'cli_status', ClientsModel::STATUS_DELETED]);
     }

     public static function findAllOperators($group_id = null) {
          $operators = static::find()
                          ->where(['!=', 'usr_type', UsersModel::TYPE_CLIENT])
                          ->onCondition(['usr_status' => UsersModel::STATUS_ACTIVE])
                          ->select('usr_username')
                          ->indexBy('usr_id');
          if (!empty($group_id)){
               $operators->andWhere(['usr_group_fkey' => $group_id]);
          }
          return $operators->column();
     }
     
     public function getGroup(){
          return $this->hasOne(GroupsModel::className(), ['gro_id' => 'usr_group_fkey'])
                  ->onCondition(['!=', 'gro_status', GroupsModel::STATUS_DELETED]);
     }
     
     public function getSupervisedGroup(){
          return $this->hasOne(GroupsModel::className(), ['gro_supervisor_fkey' => 'usr_id'])
                  ->onCondition(['!=', 'gro_status', GroupsModel::STATUS_DELETED]);
     }

}
