<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use common\models\UsersModel;

class UsersSearchModel extends UsersModel {

     // add the public attributes that will be used to store the data to be search
     public $username;
     public $name;
     public $status;
     public $created;
     public $type;

     // now set the rules to make those attributes safe
     public function rules() {
          return [
               // ... more stuff here
               [['acronym', 'name', 'status', 'creator', 'created', 'type'], 'safe'],
                  // ... more stuff here
          ];
     }

// ... model continues here
     public function search($params, $client_id = null, $group_id = null, $operatorsOnly = false) {
          // create ActiveQuery
          $query = UsersModel::find()->onCondition(['!=', 'usr_status', UsersModel::STATUS_DELETED])->orderBy('usr_lastname');
          // Important: lets join the query with our previously mentioned relations
          // I do not make any other configuration like aliases or whatever, feel free
          // to investigate that your self
          if (!empty($client_id)) {
               $query->andWhere(['usr_client_fkey' => $client_id]);
          }
          if (!empty($group_id)) {
               $query->andWhere(['usr_group_fkey' => $group_id]);
          }

          if ($operatorsOnly) {
               $query->andWhere(['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          }


          $dataProvider = new ActiveDataProvider([
               'query' => $query,
          ]);

          // Important: here is how we set up the sorting
          // The key is the attribute name on our "TourSearch" instance
          $dataProvider->sort->attributes['usr_username'] = [
               // The tables are the ones our relation are configured to
               // in my case they are prefixed with "tbl_"
               'asc' => ['users.usr_username' => SORT_ASC],
               'desc' => ['users.usr_username' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['status'] = [
               'asc' => ['users.usr_status' => SORT_ASC],
               'desc' => ['users.usr_status' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['created'] = [
               'asc' => ['users.usr_created_at' => SORT_ASC],
               'desc' => ['users.usr_created_at' => SORT_DESC],
          ];

          $dataProvider->sort->attributes['type'] = [
               'asc' => ['users.usr_type' => SORT_ASC],
               'desc' => ['users.usr_type' => SORT_DESC],
          ]; 
         // No search? Then return data Provider
          if (!($this->load($params) && $this->validate())) {
               return $dataProvider;
          }

          $query
//                  ->andFilterWhere(['like', 'users.usr_name', $this->name])
                  ->andFilterWhere(['like', 'users.usr_status', $this->status])
                  ->andFilterWhere(['like', 'users.usr_type', $this->type]);


          if (!empty($this->created)) {
               $date = preg_match_all('/\d{4}-\d{2}-\d{2}/', $this->created, $matches);
               if (!empty($matches) && count($matches, COUNT_RECURSIVE) > 2) {
                    $date1 = strtotime(trim($matches[0][0]));
                    $date2 = strtotime(trim($matches[0][1]));
                    $query->andFilterWhere(['between', 'usr_created_at', $date1, $date2]);
               }
          }
          // We have to do some search... Lets do some magic


          return $dataProvider;
     }

}
