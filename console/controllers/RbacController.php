<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use console\rbac\CreatorRule;
use console\rbac\OwnerRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        
//        $permissionAdmin = $auth->createPermission('permissionAdmin');
//        $permissionAdmin->description = 'Permission for administrator to manage RBAC';
//        $auth->add($permissionAdmin);
        $auth->addChild($auth->getRole('admin'), $auth->getPermission('permissionAdmin'));
//
//        // add "createProject" permission
//        $createProject = $auth->createPermission('createProject');
//        $createProject->description = 'Create a project';
//        $auth->add($createProject);
//        
//        // add "updateProject" permission
//        $updateProject = $auth->createPermission('updateProject');
//        $updateProject->description = 'Update project';
//        $auth->add($updateProject);
//
//        // add "serviceman" role and give this role the "createProject" permission
//        $serviceman = $auth->createRole('serviceman');
//        $auth->add($serviceman);
//        $auth->addChild($serviceman, $createProject);
//
//        // add "admin" role and give this role the "updatePost" permission
//        // as well as the permissions of the "serviceman" role
//        $admin = $auth->createRole('admin');
//        $auth->add($admin);
//        $auth->addChild($admin, $updateProject);
//        $auth->addChild($admin, $serviceman);
//
//        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
//        // usually implemented in your User model.
//        $auth->assign($serviceman, 7);
//        $auth->assign($admin, 1);
//        
//        $rule = new CreatorRule();
//        $auth->add($rule);
//        
//        // add the "updateOwnProject" permission and associate the rule with it.
//        $updateOwnProject = $auth->createPermission('updateOwnProject');
//        $updateOwnProject->description = 'Update own project';
//        $updateOwnProject->ruleName = $rule->name;
//        $auth->add($updateOwnProject);
//
//        // "updateOwnPost" will be used from "updatePost"
//        $auth->addChild($updateOwnProject, $updateProject);
//
//        // allow "author" to update their own posts
//        $auth->addChild($serviceman, $updateOwnProject);
        
//        $rule = new OwnerRule();
//        $auth->add($rule);
        
    }
}