-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 25 Paź 2017, 21:17
-- Wersja serwera: 10.1.26-MariaDB
-- Wersja PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `szzsip_empty`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `clients`
--

CREATE TABLE `clients` (
  `cli_id` bigint(20) UNSIGNED NOT NULL,
  `cli_firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_lastname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_acronym` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `cli_phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_nip` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_street_no` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_postcode` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cli_status` enum('active','deleted','locked') COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_attendant_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `cli_price_list_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `cli_created_at` int(11) UNSIGNED NOT NULL,
  `cli_updated_at` int(11) UNSIGNED NOT NULL,
  `cli_type` enum('customer','company') COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_info` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cli_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `cli_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `cli_group_fkey` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `entities_costs`
--

CREATE TABLE `entities_costs` (
  `enc_id` bigint(20) UNSIGNED NOT NULL,
  `enc_entity_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `enc_entity_type` int(11) UNSIGNED DEFAULT NULL,
  `enc_type` int(11) UNSIGNED DEFAULT NULL,
  `enc_description` text COLLATE utf8_unicode_ci,
  `enc_own_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enc_net_value` decimal(15,2) DEFAULT NULL,
  `enc_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `enc_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `enc_created_at` int(11) UNSIGNED DEFAULT NULL,
  `enc_updated_at` int(11) UNSIGNED DEFAULT NULL,
  `enc_status` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `groups`
--

CREATE TABLE `groups` (
  `gro_id` bigint(20) UNSIGNED NOT NULL,
  `gro_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gro_description` text COLLATE utf8_unicode_ci,
  `gro_status` int(1) UNSIGNED DEFAULT NULL,
  `gro_created_at` int(11) UNSIGNED DEFAULT NULL,
  `gro_updated_at` int(11) UNSIGNED DEFAULT NULL,
  `gro_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `gro_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `gro_supervisor_fkey` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `ord_id` bigint(20) UNSIGNED NOT NULL,
  `ord_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ord_number` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ord_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ord_owner_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_group_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_executive_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_client_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ord_created_at` int(11) DEFAULT NULL,
  `ord_updated_at` int(11) DEFAULT NULL,
  `ord_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_status` int(1) UNSIGNED DEFAULT NULL,
  `ord_project_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ord_budget_type` int(2) UNSIGNED DEFAULT NULL,
  `ord_budget_value` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders_tasks`
--

CREATE TABLE `orders_tasks` (
  `ort_id` bigint(20) UNSIGNED NOT NULL,
  `ort_order_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ort_task_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `ort_description` text COLLATE utf8_unicode_ci,
  `ort_created_at` int(11) UNSIGNED DEFAULT NULL,
  `ort_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `ort_updated_at` int(11) UNSIGNED DEFAULT NULL,
  `ort_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `ort_locked` int(1) UNSIGNED DEFAULT NULL,
  `ort_status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `ort_group_fkey` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `price_lists`
--

CREATE TABLE `price_lists` (
  `prl_id` bigint(20) UNSIGNED NOT NULL,
  `prl_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prl_type` int(1) DEFAULT NULL,
  `prl_status` int(1) DEFAULT NULL,
  `prl_description` text COLLATE utf8_unicode_ci,
  `prl_h_in_packet` float DEFAULT NULL,
  `prl_net_price` decimal(7,2) DEFAULT NULL,
  `prl_net_hourly_rate` decimal(7,2) DEFAULT NULL,
  `prl_sla` decimal(7,2) DEFAULT NULL,
  `prl_created_by` bigint(20) DEFAULT NULL,
  `prl_created_at` int(11) DEFAULT NULL,
  `prl_updated_by` bigint(20) DEFAULT NULL,
  `prl_updated_at` int(11) DEFAULT NULL,
  `prl_group_fkey` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `projects`
--

CREATE TABLE `projects` (
  `pro_id` bigint(20) UNSIGNED NOT NULL,
  `pro_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pro_number` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pro_description` text COLLATE utf8_unicode_ci,
  `pro_type` int(2) UNSIGNED DEFAULT NULL,
  `pro_owner_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `pro_group_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `pro_client_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `pro_created_at` int(11) UNSIGNED NOT NULL,
  `pro_updated_at` int(11) UNSIGNED NOT NULL,
  `pro_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `pro_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `pro_status` int(1) UNSIGNED DEFAULT NULL,
  `pro_budget_type` int(2) UNSIGNED DEFAULT NULL,
  `pro_budget_value` decimal(15,2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `projects_orders`
--

CREATE TABLE `projects_orders` (
  `pos_id` bigint(20) UNSIGNED NOT NULL,
  `pos_order_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `pos_project_fkey` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `starts_stops`
--

CREATE TABLE `starts_stops` (
  `sts_id` bigint(20) UNSIGNED NOT NULL,
  `sts_order_task_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `sts_created_at` int(11) UNSIGNED DEFAULT NULL,
  `sts_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `sts_updated_at` int(11) UNSIGNED DEFAULT NULL,
  `sts_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `sts_type` int(1) UNSIGNED DEFAULT NULL,
  `sts_status` int(1) UNSIGNED DEFAULT NULL,
  `sts_group_fkey` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tasks`
--

CREATE TABLE `tasks` (
  `tas_id` bigint(20) UNSIGNED NOT NULL,
  `tas_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tas_number` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tas_description` text COLLATE utf8_unicode_ci,
  `tas_status` int(1) UNSIGNED DEFAULT NULL,
  `tas_created_at` int(11) UNSIGNED DEFAULT NULL,
  `tas_updated_at` int(11) UNSIGNED DEFAULT NULL,
  `tas_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `tas_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `tas_group_fkey` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `usr_username` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `usr_firstname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_lastname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_client_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `usr_phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usr_auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_status` enum('active','deleted','locked') COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_created_at` int(11) UNSIGNED DEFAULT NULL,
  `usr_updated_at` int(11) UNSIGNED DEFAULT NULL,
  `usr_created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `usr_updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `usr_group_fkey` bigint(20) UNSIGNED DEFAULT NULL,
  `usr_type` enum('admin','supervisor','serviceman','client') COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`cli_id`),
  ADD KEY `clients_cli_attendant_fkey` (`cli_attendant_fkey`),
  ADD KEY `clients_cli_created_by` (`cli_created_by`),
  ADD KEY `clients_cli_updated_by` (`cli_updated_by`),
  ADD KEY `clients_cli_price_list_fkey` (`cli_price_list_fkey`);

--
-- Indexes for table `entities_costs`
--
ALTER TABLE `entities_costs`
  ADD PRIMARY KEY (`enc_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`gro_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `orders_tasks`
--
ALTER TABLE `orders_tasks`
  ADD PRIMARY KEY (`ort_id`);

--
-- Indexes for table `price_lists`
--
ALTER TABLE `price_lists`
  ADD PRIMARY KEY (`prl_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `projects_orders`
--
ALTER TABLE `projects_orders`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `starts_stops`
--
ALTER TABLE `starts_stops`
  ADD PRIMARY KEY (`sts_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`tas_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `clients`
--
ALTER TABLE `clients`
  MODIFY `cli_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT dla tabeli `entities_costs`
--
ALTER TABLE `entities_costs`
  MODIFY `enc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT dla tabeli `groups`
--
ALTER TABLE `groups`
  MODIFY `gro_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `ord_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT dla tabeli `orders_tasks`
--
ALTER TABLE `orders_tasks`
  MODIFY `ort_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT dla tabeli `price_lists`
--
ALTER TABLE `price_lists`
  MODIFY `prl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `projects`
--
ALTER TABLE `projects`
  MODIFY `pro_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT dla tabeli `projects_orders`
--
ALTER TABLE `projects_orders`
  MODIFY `pos_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `starts_stops`
--
ALTER TABLE `starts_stops`
  MODIFY `sts_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT dla tabeli `tasks`
--
ALTER TABLE `tasks`
  MODIFY `tas_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `usr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_cli_attendant_fkey` FOREIGN KEY (`cli_attendant_fkey`) REFERENCES `users` (`usr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `clients_cli_created_by` FOREIGN KEY (`cli_created_by`) REFERENCES `users` (`usr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `clients_cli_price_list_fkey` FOREIGN KEY (`cli_price_list_fkey`) REFERENCES `price_lists` (`prl_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `clients_cli_updated_by` FOREIGN KEY (`cli_updated_by`) REFERENCES `users` (`usr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
