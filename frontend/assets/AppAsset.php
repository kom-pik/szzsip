<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {

     public $basePath = '@webroot';
     public $baseUrl = '@web';
     public $css = [
          'css/site.css',
          'css/modals.css',
          'css/helpers.css',
          'plugins/css/simple-hint.css'
     ];
     public $js = [
          'js/js.js',
          'js/modals.js',
          'js/grid.js',
          'plugins/js/tinymce/js/tinymce/tinymce.min.js'
     ];
     public $depends = [
          'yii\web\YiiAsset',
          'yii\bootstrap\BootstrapAsset',
          'yii\widgets\PjaxAsset',
          'yii\widgets\ActiveFormAsset',
          'yii\validators\ValidationAsset',
     ];

}
