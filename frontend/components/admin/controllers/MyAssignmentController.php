<?php

namespace frontend\components\admin\controllers;

use mdm\admin\controllers\AssignmentController;
use yii\filters\AccessControl;

/**
 * Description of AssignementController
 *
 * @author piqs
 */
class MyAssignmentController extends AssignmentController {
    
    public function behaviors()
    {
        return array_merge(parent::behaviors() ,[
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ]
        ]);
        
    }
    
    public function actionIndex() {
        return parent::actionIndex();
    }
    
    public function actionView($id) {
        parent::actionView($id);
    }
    
    public function actionAssign($id) {
        parent::actionAssign($id);
    }
    
    public function actionRevoke($id) {
        parent::actionRevoke($id);
    }
}
