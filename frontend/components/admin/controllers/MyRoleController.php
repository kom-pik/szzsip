<?php

namespace frontend\components\admin\controllers;

use mdm\admin\controllers\RoleController;
use yii\rbac\Item;
use yii\filters\AccessControl;

/**
 * RoleController implements the CRUD actions for AuthItem model.
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class MyRoleController extends RoleController
{

    /**
     * @inheritdoc
     */
    public function labels()
    {
        parent::labels();
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        parent::getType();
    }
}
