<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
     'id' => 'app-frontend',
     'basePath' => dirname(__DIR__),
     'bootstrap' => ['log'],
     'controllerNamespace' => 'frontend\controllers',
     'components' => [
          'i18n' => [
               'translations' => [
                    'app*' => [
                         'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@app/messages',
//                    'sourceLanguage' => 'pl-PL',
                         'fileMap' => [
                              'app' => 'common/messages/app/pl.php',
                              'app/error' => 'error.php',
                         ],
                    ],
                    'yii' => [
                         'class' => 'yii\i18n\PhpMessageSource',
                         'basePath' => '@common/messages/yii',
                         'sourceLanguage' => 'pl-PL',
                    ],
               ],
          ],
          'request' => [
               'csrfParam' => '_csrf-frontend',
          ],
          'user' => [
               'identityClass' => 'common\models\UsersModel',
               'enableAutoLogin' => true,
               'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
          ],
          'session' => [
               // this is the name of the session cookie used for login on the frontend
               'name' => 'szzsip-frontend',
          ],
          'log' => [
               'traceLevel' => YII_DEBUG ? 3 : 0,
               'targets' => [
                    [
                         'class' => 'yii\log\FileTarget',
                         'levels' => ['error', 'warning'],
                    ],
               ],
          ],
          'errorHandler' => [
               'errorAction' => 'sites/error',
          ],
          'urlManager' => [
               'enablePrettyUrl' => true,
               'showScriptName' => false,
               'rules' => [
                    '' => '/sites/index',
                    '/site/index' => '/sites/index',
                    '/site/contact/captcha' => '/sites/contact/captcha',
                    //orders controller
                    '/orders/add-task-to-order/<order_id\d+>/<task_id\d+>' => '/orders/add-task-to-order/',
                    '/orders/add/<project_id\d+>' => '/orders/add/',
                    '/orders/add-property/<id:\d+>/<property:\w+>/<action:\w+>' => '/orders/add-property',
                    '/orders/add-property/<id:\d+>/<property:\w+>' => '/orders/add-property',
                    //projects controller
                    '/projects/add-property/<id:\d+>/<property:\w+>/<action:\w+>' => '/projects/add-property',
                    '/projects/add-property/<id:\d+>/<property:\w+>' => '/projects/add-property',
                    //clients controller
                    //users controller
                    //task controller
                    '/<controller:\w+>/<action:\w+>/<id:\d+>' => '/<controller>/<action>',
//                    '/<controller:\w+>/<action:\w+>/<tab:\w+>' => '/<controller>/<action>',
               ],
          ],
     ],
     'modules' => [
          'gridview' => [
               'class' => '\kartik\grid\Module',
          // enter optional module parameters below - only if you need to  
          // use your own export download action or custom translation 
          // message source
          // 'downloadAction' => 'gridview/export/download',
//             'i18n' =>     [
//                'class' => 'yii\i18n\PhpMessageSource',
//                'basePath' => '@kvgrid/messages',
//                'forceTranslation' => true
//            ],                                                          
          ]
     ],
     'params' => $params,
];
