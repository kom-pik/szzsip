<?php

return [
     '' => '/sites/index',
     '/site/index' => '/sites/index',
     '/site/contact/captcha' => '/site/contact/captcha',
     //orders controller
     '/orders/add-property/<id:\d+>/<property:\w+>/<action:\w+>' => '/orders/add-property',
     '/orders/view/<id:\d+>' => '/orders/view',
     '/orders/delete/<id:\d+>' => '/orders/delete',
     '/orders/update/<id:\d+>' => '/orders/update',
     //projects controller
     '/projects/add-property/<id:\d+>/<property:\w+>/<action:\w+>' => '/projects/add-property',
     '/projects/view/<id:\d+>' => '/projects/view',
     '/projects/delete/<id:\d+>' => '/projects/delete',
     '/projects/update/<id:\d+>' => '/projects/update',
     //clients controller
     '/clients/view/<id:\d+>' => '/clients/view',
     //users controller
     '/users/view/<id:\d+>' => '/users/view',
];

