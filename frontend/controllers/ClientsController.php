<?php

namespace frontend\controllers;

use Yii;
use common\controllers\MyWebController;
use yii\filters\AccessControl;
use common\models\ProjectsModel;
use common\models\ProjectsSearchModel;
use common\models\UsersModel;
use common\models\UsersSearchModel;
use common\models\ClientsSearchModel;
use common\models\ClientsModel;
use common\models\GroupsModel;
use common\models\OrdersSearchModel;
use common\models\OrdersModel;
use Exception;
use common\models\ReportModel;
use common\models\PriceListsModel;
use frontend\services\ServiceClients;

class ClientsController extends MyWebController {

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return [
               'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                         [
                              'actions' => [],
                              'allow' => true,
                              'roles' => ['?'],
                         ],
                         [
                              'actions' => ['index', 'add', 'update', 'delete', 'view', 'add-attendant', 'add-property'],
                              'allow' => true,
                              'roles' => ['@'],
                         ],
                    ],
               ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
               'error' => [
                    'class' => 'yii\web\ErrorAction',
               ],
               'captcha' => [
                    'class' => 'yii\captcha\CaptchaAction',
                    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
               ],
          ];
     }
     
     public function beforeAction($action) {
          if (parent::beforeAction($action)) {
               if ($this->user->isClient()) {
                    if (!in_array($action->id, array('view', 'add-property'))) {
                         $this->goBack('error', $this->messageNotAllowed);
                         return false;
                    }
               }
               return true;
          }
          return false;
     }

     /**
      * Displays homepage.
      *
      * @return mixed
      */
     public function actionIndex() {
          $searchModel = new ClientsSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->group_id);
          $userList = UsersModel::findAllOperators($this->group_id);
          $clientsList = ClientsModel::findAllClients($this->client_ids, $this->group_id);
          $projectsNames = ProjectsModel::getAllProjectsNames();
          return $this->render('index', [
                       'dataProvider' => $dataProvider,
                       'searchModel' => $searchModel,
                       'userList' => $userList,
                       'clientsList' => $clientsList,
                       'projectsNames' => $projectsNames
          ]);
     }

     public function actionView($id) {
          //if not empty allowed client_ids and id not in allowed client_ids
          if (!empty($this->client_ids) && !in_array($id, $this->client_ids)) {
               return $this->goBack('error', $this->messageNotAllowed);
          }

          if (empty($id)) {
               return $this->goBack('error', $this->messageNoParams);
          }
          //get client and redirect if empty
          $client = ClientsModel::getClient($id, $this->group_id);
          if (empty($client)) {
               return $this->goBack('error', 'Klient nie istnieje');
          }

          $reportModel = new ReportModel();
          $projectSearchModel = new ProjectsSearchModel();
          $projectDataProvider = $projectSearchModel->search(Yii::$app->request->queryParams, $client_id = $id);
          $clientAllProjectsNames = ProjectsModel::getAllProjectsNames($client_id = $id);
          $orderSearchModel = new OrdersSearchModel();
          $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams, null, $client_id = $id);
          $clientAllOrdersNames = OrdersModel::getAllOrdersNames('ord_name', $client_id = $id, false, false);
          $userSearchModel = new UsersSearchModel();
          $userDataProvider = $userSearchModel->search(Yii::$app->request->queryParams, $client_id = $id);
          $usersList = UsersModel::findAllUsers(['usr_client_fkey' => $client_id]);
          $this->view->title = 'Klient ' . $client->name;
          return $this->render('view', [
                       'client' => $client,
                       'projectDataProvider' => $projectDataProvider,
                       'projectSearchModel' => $projectSearchModel,
                       'clientAllProjectsNames' => $clientAllProjectsNames,
                       'orderDataProvider' => $orderDataProvider,
                       'orderSearchModel' => $orderSearchModel,
                       'clientAllOrdersNames' => $clientAllOrdersNames,
                       'userSearchModel' => $userSearchModel,
                       'userDataProvider' => $userDataProvider,
                       'usersList' => $usersList,
                       'reportTabData' => [
                       'reportModel' => $reportModel
                       ]
          ]);
     }

     public function actionAdd() {
          $client = new ClientsModel();
          if ($this->request->isPost) {
               $client = ServiceClients::saveClient($client, $this->request, $this->group->gro_id);
                    
               if ($this->request->isAjax && $this->request->post('hiddenForm') !== null){
                    Yii::$app->response->format = 'json';
                    return array(
                         'success' => $client->hasErrors() ? false : true,
                         'clients' => ClientsModel::findAllClients($this->client_ids, $this->group_id),
                         'cli_id' => $client->cli_id,
                         'errors' => $client->errors
                         );
               }
               Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano klienta'));
               return $this->redirect(['/clients/view', 'id' => $client->cli_id]);
          }
          $userList = UsersModel::findAllOperators($this->group_id);
          $groupsList = [];
          if ($this->user->isAdmin()){
               $groupsList = GroupsModel::getGroups();
          }
          $priceLists = PriceListsModel::getAllPriceListsNames(null, 'prl_id');
          $params = ['partialData' => [
               'client' => $client,
               'userList' => $userList,
               'groupsList' => $groupsList,
               'priceLists' => $priceLists
                  ]];
          if ($this->request->isAjax){
               return $this->renderAjax('add', $params);
          }
          return $this->render('add', $params);
     }

     public function actionUpdate($id) {
          if (empty($id)) {
               return $this->goBack('error', $this->messageNoParams);
          }

          //get client and redirect if empty
          $client = ClientsModel::getClient($id, $this->group_id);
          if (empty($client)) {
               return $this->goBack('error', 'Klient nie istnieje');
          }

          if ($client->load($this->request->post()) && $client->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['view', 'id' => $client->cli_id]);
          }
          $userList = UsersModel::findAllOperators($this->group_id);
          $groupsList = [];
          if ($this->user->isAdmin()){
               $groupsList = GroupsModel::getGroups();
          }
          $priceLists = PriceListsModel::getAllPriceListsNames(null, 'prl_id');
          return $this->render('update', ['partialData' => [
               'client' => $client,
               'userList' => $userList,
               'groupsList' => $groupsList,
               'priceLists' => $priceLists
                  ]]);
     }

     public function actionAddProperty($id, $property = null) {
          if ($this->user->isClient() && !in_array($property, ClientsModel::getPropertiesAllowedToChangeByClientRole())) {
               if (Yii::$app->request->isAjax) {
                    return json_encode(false);
               }
               return $this->goBack('error', $this->messageNotAllowed);
          }

          if (empty($id) || empty($property)) {
               return $this->goBack('error', $this->messageNoParams);
          }

          //get client and redirect if empty
          $client = ClientsModel::getClient($id, $this->group_id);
          if (empty($client)) {
               return $this->goBack('error', 'Klient nie istnieje');
          }

          if ($client->load(Yii::$app->request->post()) && $client->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['view', 'id' => $id]);
          }
          $usersList = UsersModel::findAllOperators($this->group_id);
          return $this->renderAjax('partials/modal-add-property', [
                       'client' => $client,
                       'usersList' => $usersList,
                       'property' => $property
          ]);
     }

}
