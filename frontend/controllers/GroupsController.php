<?php

namespace frontend\controllers;

use Yii;
use common\controllers\MyWebController;
use yii\filters\AccessControl;
use common\models\GroupsSearchModel;
use common\models\GroupsModel;
use common\models\UsersModel;
use common\models\UsersSearchModel;
use common\models\ReportModel;

/**
 * Description of GroupsController
 *
 * @author Marcin Pikul
 */
class GroupsController extends MyWebController {
     
     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => [],
                          'allow' => true,
                          'roles' => ['?'],
                      ],
                      [
                          'actions' => [
                               'index',
                               'add',
                               'delete',
                               'view',
                               'update',
                               'reporting'
                               ],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
          ];
     }
     
     public function beforeAction($action) {
          if (parent::beforeAction($action)){
               if (!$this->user->isAdmin()){
                    if (!$this->user->isSupervisor() && !in_array($action->id, array('view'))){
                         Yii::$app->session->addFlash('error', Yii::t('app', $this->messageNotAllowed));
                         $this->redirect('/site/index');
                         return false;                         
                    }
//                    dd(!empty($this->request->getBodyParam('id')));
                    if (!empty($this->request->get('id')) || !empty($this->request->post('id'))){
                         $this->goBack('error', $this->messageNotAllowed);
                         return false;
                    }
               }
               return true;
          }
          return false;
     }


     public function actionIndex(){
          $searchModel = new GroupsSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $userList = UsersModel::findAllUsers(['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          
          $this->view->title = 'Grupy';
          $this->view->params['breadcrumbs'][] = $this->view->title;
          $params = array(
               'dataProvider' => $dataProvider,
               'userList' => $userList,
               'searchModel' => $searchModel
          );
          return $this->render('index', $params);
     }
     
     public function actionAdd(){
          $group = new GroupsModel();
          if ($this->request->isPost && $group->load($this->request->post()) && $group->save()){
               $supervisor = $group->supervisor;
               $supervisor->usr_group_fkey = $group->gro_id;
               $supervisor->save();
               return $this->goBack('success', 'Dodano grupę');
          }
          
          $userList = UsersModel::findAllUsers($this->group_id, ['in', 'usr_type', array(UsersModel::TYPE_ADMIN, UsersModel::TYPE_SUPERVISOR)]);
          $params = array(
               'group' => $group,
               'userList' => $userList
          );
          return $this->renderAjax('add', $params);
     }
     
     public function actionUpdate($id = null){
          
          
     }
     
     public function actionView($id = null){
          if (empty($id)){
               $id = $this->group_id;
          }
          $group = (new GroupsModel())->findGroupById($id);

          if (empty($group)){
               return $this->goBack('error', $this->messageNotFound);
          }

          $membersSearchModel = new UsersSearchModel();
          $membersDataProvider = $membersSearchModel->search(Yii::$app->request->queryParams, null, $id, true);
          $membersList = UsersModel::findAllOperators($id);
          $reportModel = new ReportModel();
          
          $this->view->title = 'Grupa '. $group->gro_name;
          $params = array(
               'group' => $group,
               'membersTab' => array(
                    'group' => $group,
                    'membersSearchModel' => $membersSearchModel,
                    'membersDataProvider' => $membersDataProvider,
                    'membersList' => $membersList
               ),
               'reportsTab' => array(
                    'group' => $group,
                    'reportModel' => $reportModel
               )
          );
          
          return $this->render('view', $params);
     }

}
