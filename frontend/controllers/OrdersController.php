<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\controllers\MyWebController;
use common\models\ProjectsModel;
use common\models\UsersModel;
use common\models\OrdersSearchModel;
use common\models\ClientsModel;
use common\models\OrdersModel;
use common\models\TasksModel;
use common\models\GroupsModel;
use common\models\OrdersTasksModel;
use common\models\OrderTaskSearch;
use common\models\EntitiesCostsModel;
use common\models\EntitiesCostsModelSearch;
use frontend\models\Task2OrderForm;
use Exception;

class OrdersController extends MyWebController {

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return [
               'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                         [
                              'actions' => [],
                              'allow' => true,
                              'roles' => ['?'],
                         ],
                         [
                              'actions' => [
                                   'index',
                                   'add',
                                   'delete',
                                   'add-task-to-order',
                                   'add-property',
                                   'view',
                                   'remove-task',
                                   'order-task-desc',
                                   'update',
                                   'add-cost',
                                   'edit-cost',
                                   'delete-cost'],
                              'allow' => true,
                              'roles' => ['@'],
                         ],
                    ],
               ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
               'error' => [
                    'class' => 'yii\web\ErrorAction',
               ],
               'captcha' => [
                    'class' => 'yii\captcha\CaptchaAction',
                    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
               ],
          ];
     }

     public function beforeAction($action) {
          if (parent::beforeAction($action)) {
               if ($this->user->isClient()) {
                    if (!in_array($action->id, array('view', 'add'))) {
                         $this->goBack('error', $this->messageNotAllowed);
                         return false;
                    }
               }
               return true;
          }
          return false;
     }

     /**
      * Displays homepage.
      *
      * @return mixed
      */
     public function actionIndex() {
          $searchModel = new OrdersSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, $this->client_ids, null, $this->group_id);
          $userList = UsersModel::findAllOperators($this->group_id);
          $clientsList = ClientsModel::findAllClients($this->client_ids, $this->group_id);
          $orderNames = OrdersModel::getAllOrdersNames('ord_name', null, $this->client_ids, null, $this->group_id);
          $projects = ProjectsModel::getAllProjects($this->client_ids, $this->group_id);

          return $this->render('index', [
                       'dataProvider' => $dataProvider,
                       'searchModel' => $searchModel,
                       'userList' => $userList,
                       'clientsList' => $clientsList,
                       'orderNames' => $orderNames,
                       'projects' => $projects
          ]);
     }

     public function actionView($id) {
          if ($this->user->isClient() && !$this->request->isAjax) {
               return $this->goBack('error', $this->messageNotAllowed);
          }
          if (empty($id)) {
               return $this->goBack('error', $this->messageNoParams);
          } else {
               $order = OrdersModel::findOne([$id]);
               if (empty($order)) {
                    return $this->goBack('error', $this->messageNotFound . 'zlecenia.');
               }
          }

          $searchModel = new OrderTaskSearch();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
          $userList = UsersModel::findAllOperators($this->group_id);
          $tasksNames = TasksModel::getAllTasksNames($id, $this->group_id);

          $costSearchModel = new EntitiesCostsModelSearch();
          $entitiesCostsDataProvider = $costSearchModel->search(Yii::$app->request->queryParams, $id, EntitiesCostsModel::ENTITY_TYPE_ORDERS);
          $costsTabParams = [
               'entitiesCostsDataProvider' => $entitiesCostsDataProvider,
               'costSearchModel' => $costSearchModel
          ];
          
          $addWorkTimeModel = new \frontend\models\AddWorktimeForm();
          $addWorkTimeModel->orderTaskFkey = $id;
          $params = [
               'order' => $order,
               'dataProvider' => $dataProvider,
               'searchModel' => $searchModel,
               'userList' => $userList,
               'tasksNames' => $tasksNames,
               'user' => $this->user,
               'costsTabParams' => $costsTabParams,
               'addWorkTimeModel' => $addWorkTimeModel
          ];

          $this->view->title = 'Zlecenie ' . $order->ord_name;
          if ($this->request->isAjax && $this->user->isClient()) {
               $params['modal'] = true;
               return $this->renderAjax('view', $params);
          }
          return $this->render('view', $params);
     }

     public function actionAdd($project_id = null) {
          $order = new OrdersModel();
          if ($order->load(Yii::$app->request->post()) && $order->validate()) {
               $transaction = Yii::$app->db->beginTransaction();
               try {
                    if (empty($order->ord_group_fkey)){
                         $order->ord_group_fkey = $this->group_id;
                    }
                    $order->save();
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano zlecenie'));
                    $url = '/orders/index';
                    if ($this->user->isClient()) {
                         $url = ['/clients/view', 'id' => $this->user->company->cli_id];
                    } elseif (!empty($project_id)) {
                         $url = ['/projects/view', 'id' => $project_id];
                    }
                    return $this->redirect($url);
               } catch (ActionException $ex) {
                    $transaction->rollBack();
                    $order->addError('name', $ex->getMessage());
               } catch (Exception $ex) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', $ex->getMessage());
               }
          }
          if ($this->user->isClient()) {
               $userList = $this->user->company->getNewOrderOwner();
               $clientsList[$this->user->company->cli_id] = $this->user->company->cli_acronym;
               $orderStatus[OrdersModel::STATUS_NEW] = OrdersModel::listOpenStatuses()[OrdersModel::STATUS_NEW];
               $projects = ProjectsModel::getAllProjects($this->client_ids, $this->group_id);
               $order->ord_client_fkey = $this->user->usr_id;
          } else {
               $userList = UsersModel::findAllOperators($this->group_id);
               $clientsList = ClientsModel::findAllClients(null, $this->group_id);
               $orderStatus = OrdersModel::listOpenStatuses();
               $projects = ProjectsModel::getAllProjects($this->client_ids, $this->group_id);
          }
          
          $project = !empty($project_id) ? (new ProjectsModel())->findProject($project_id, $this->group_id) : new ProjectsModel();
          if (!empty($project)){
               $order->ord_project_fkey = $project_id;
               $order->ord_group_fkey = $project->pro_group_fkey;
          }

          $groupsList = GroupsModel::getGroups();
          $params = [
               'order' => $order,
               'userList' => $userList,
               'clientsList' => $clientsList,
               'orderStatus' => $orderStatus,
               'projects' => $projects,
               'project' => $project,
               'groupsList' => $groupsList
          ];
          if ($this->request->isAjax){
               return $this->renderAjax('add', $params);
          }
          return $this->renderPartial('add', $params);
     }

     public function actionUpdate($id) {
          $order = OrdersModel::findOne([$id]);

          if ($order->load(Yii::$app->request->post()) && $order->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['view', 'id' => $id]);
          }
          $userList = UsersModel::findAllUsers($this->group_id, ['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $orderStatus = OrdersModel::listStatuses();
          $projects = ProjectsModel::getAllProjects(null, $this->group_id);
          return $this->render('update', [
                       'order' => $order,
                       'userList' => $userList,
                       'clientsList' => $clientsList,
                       'orderStatus' => $orderStatus,
                       'projects' => $projects
          ]);
     }

     /**
      * 
      * @param integer $order_id
      * @param integer $task_id
      * @return type
      * 
      */
     public function actionAddTaskToOrder($order_id = null, $task_id = null) {
          if (!empty($order_id)) {
               $order = OrdersModel::findOne([$order_id]);
          } else {
               $order = new OrdersModel();
          }

          if (!empty($task_id)) {
               $task = TasksModel::findOne([$task_id]);
          } else {
               $task = new TasksModel();
          }
          if ($this->request->isPost && !empty($this->request->post('taskIds'))){
               $taskIds = $this->request->post('taskIds');
               $tasks = TasksModel::findAll($taskIds);
          }
          $task2order = new Task2OrderForm();
          
          $post = $this->request->post();
          if (!empty($post["Task2OrderForm"]['task_ids']) && is_string($post["Task2OrderForm"]['task_ids'])){
               
               $post["Task2OrderForm"]['task_ids'] = explode(',', $post["Task2OrderForm"]['task_ids']);
          }
          if ($task2order->load($post) && $task2order->validate()) {
               
               $transaction = Yii::$app->db->beginTransaction();
               try {
                    $task2order->addTask2Order($this->group_id);
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano zadania do zlecenia'));
                    return $this->goBack();
               } catch (ActionException $ex) {
                    $transaction->rollBack();
                    $task2order->addError('name', $ex->getMessage());
               } catch (Exception $ex) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', $ex->getMessage());
               }
          }

          $tasksList = TasksModel::getAllTasksNames(null, $this->group_id);
          $ordersList = OrdersModel::getAllOrdersNames('ord_id', false, false, false);
          $this->view->title = $order->ord_id ? 'Dodaj zadania do zlecenia' : 'Dodaj zadanie do zlecenia';
          $params = [
               'order' => $order,
               'task' => $task,
               'task2order' => $task2order,
               'tasksList' => $tasksList,
               'ordersList' => $ordersList,
               'taskIds' => !empty($taskIds) ? $taskIds : array(),
               'tasks' => !empty($tasks) ? $tasks : array(),
          ];
          if ($this->request->isAjax) {
               return $this->renderAjax('add-task-to-order', $params);
          }
          return $this->render('add-task-to-order', $params);
     }

     public function actionRemoveTask($id = null) {
          if (empty($id) && !Yii::$app->request->isPost) {
               return $this->goBack('error', $this->messageNoParams);
          }

          if (Yii::$app->request->isPost && !empty(Yii::$app->request->post('ids'))) {
               $ids = Yii::$app->request->post('ids');
               $orderTask = OrdersTasksModel::findAll($ids);
          } else {
               $orderTask[] = OrdersTasksModel::findOne([$id]);
          }

          if (!empty($orderTask)) {
               $messageSuccess = Yii::t('app', 'Usunięto zadania ze zlecenia: ');
               $messageError = Yii::t('app', 'Nie udało się usunąć następujących zadań: ');
               $oTdeleted = 0;
               $oTnotDeleted = 0;
               foreach ($orderTask as $oT) {
                    if ($oT->delete()) {
                         $oTdeleted++;
                         $messageSuccess .= '<br>' . $oT->ort_id;
                    } else {
                         $oTnotDeleted++;
                         $messageError .= '<br>' . $oT->ort_id;
                    }
               }
               if ($oTdeleted > 0) {
                    Yii::$app->session->addFlash('success', $messageSuccess);
               }
               if ($oTnotDeleted > 0) {
                    Yii::$app->session->addFlash('error', $messageError);
               }
               return $this->redirect(['view', 'id' => $orderTask[0]->ort_order_fkey]);
          }
          return $this->goBack('error', $this->messageNotFound . 'zadania');
     }

     public function actionAddProperty($id, $property = null, $action = 'view') {
          $order = OrdersModel::findOne([$id]);

          if ($order->load(Yii::$app->request->post()) && $order->save(false)) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect([$action, 'id' => $id]);
          }
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $projectsList = ProjectsModel::getAllProjectsNames(null, null, ProjectsModel::TABLE_FIELD_PREFIX . 'id', $this->group_id);

          return $this->renderAjax('partials/modal-add-property', [
                       'order' => $order,
                       'clientsList' => $clientsList,
                       'projectsList' => $projectsList,
                       'property' => $property
          ]);
     }

     public function actionOrderTaskDesc($id = null) {

          if (!empty($id)) {
               $orderTask = OrdersTasksModel::findOne([$id]);
          } else {
               $orderTask = new OrdersTasksModel();
          }

          if ($orderTask->load(Yii::$app->request->post()) && $orderTask->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->renderPartial('partials/order-description-row', [
                            'orderTask' => $orderTask,
               ]);
          }

          $orderTask = OrdersTasksModel::findOne(Yii::$app->request->post('expandRowKey'));
          if ($orderTask) {
               return $this->renderPartial('partials/order-description-row', [
                            'orderTask' => $orderTask
               ]);
          } else {
               return '<div class="alert alert-danger">No data found</div>' . Yii::$app->request->post('id');
          }
     }

     public function actionAddCost($entityId, $entityType = EntitiesCostsModel::ENTITY_TYPE_ORDERS) {
          return parent::actionAddCost($entityId, $entityType);
     }

}
