<?php

namespace frontend\controllers;

use Yii;
use common\controllers\MyWebController;
use common\models\PriceListsModel;
use common\models\PriceListsSearchModel;
use common\models\ClientsModel;
use common\models\UsersModel;
use yii\filters\AccessControl;

/**
 * Description of ProfileController
 *
 * @author piqs
 */
class PriceListsController extends MyWebController {

     public function behaviors() {
          return [
               'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                         [
                              'actions' => [],
                              'allow' => false,
                              'roles' => ['?'],
                         ],
                         [
                              'actions' => ['index', 'add', 'update', 'delete', 'view', 'add-property'],
                              'allow' => true,
                              'roles' => ['@'],
                         ],
                    ],
               ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
               'error' => [
                    'class' => 'yii\web\ErrorAction',
               ],
               'captcha' => [
                    'class' => 'yii\captcha\CaptchaAction',
                    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
               ],
          ];
     }

     public function actionIndex() {
          $searchModel = new PriceListsSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $priceLists = PriceListsModel::getAllPriceListsNames();
          $userList = UsersModel::findAllUsers(['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = ClientsModel::findAllClients();
          return $this->render('index', [
                       'dataProvider' => $dataProvider,
                       'searchModel' => $searchModel,
                       'priceLists' => $priceLists,
                       'userList' => $userList,
                       'clientsList' => $clientsList,
          ]);
     }

     public function actionView($id) {
          $priceList = PriceListsModel::findOne([$id]);
          $this->view->title = 'Cennik '.$priceList->prl_name;
          return $this->render('add', [
                       'priceList' => $priceList,
          ]);
     }

     public function actionUpdate($id) {
          $priceList = PriceListsModel::findOne([$id]);
          if ($priceList->load($this->request->post()) && $priceList->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany'));
               return $this->redirect(['view', 'id' => $id]);
          }
          $this->view->title = 'Edytuj cennik';
          return $this->render('add', ['priceList' => $priceList]);
     }

     public function actionAdd() {
          $priceList = new PriceListsModel();
          if ($priceList->load(\Yii::$app->request->post()) && $priceList->validate()) {
               if (empty($priceList->prl_group_fkey)){
                    $priceList->prl_group_fkey = $this->group_id;
               }
               $priceList->save();
               Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano cennik ' . $priceList->name));
               return $this->redirect(['view', 'id' => $priceList->prl_id]);
          }
          $this->view->title = 'Dodaj cennik';
          return $this->render('add', ['priceList' => $priceList]);
     }

     public function actionAddProperty($property = null) {
          $user = \Yii::$app->user->identity;

          if ($user->load(Yii::$app->request->post()) && $user->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['view']);
          }

          return $this->renderPartial('_modal-add-property', [
                       'user' => $user,
                       'property' => $property
          ]);
     }

}
