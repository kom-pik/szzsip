<?php

namespace frontend\controllers;

use Yii;
use common\controllers\MyWebController;
use common\models\UsersModel;
use common\models\OrdersModel;
use common\models\OrdersSearchModel;
use common\models\ProjectsModel;
use common\models\ProjectsSearchModel;
use common\models\GroupsModel;
use yii\filters\AccessControl;

/**
 * Description of ProfileController
 *
 * @author piqs
 */
class ProfilesController extends MyWebController {

     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => [],
                          'allow' => true,
                          'roles' => ['?'],
                      ],
                      [
                          'actions' => ['update', 'view', 'add-property'],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
              'error' => [
                  'class' => 'yii\web\ErrorAction',
              ],
              'captcha' => [
                  'class' => 'yii\captcha\CaptchaAction',
                  'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
              ],
          ];
     }

     public function actionView() {
          $projectSearchModel = new ProjectsSearchModel();
          $projectDataProvider = $projectSearchModel->search(Yii::$app->request->queryParams, $client_id = null, $owner_id = $this->user->usr_id);
          $userAllProjectsNames = ProjectsModel::getAllProjectsNames($client_id = null, $owner_id = $this->user->usr_id);
          $orderSearchModel = new OrdersSearchModel();
          $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams, null, $client_id = null, $owner_id = $this->user->usr_id);
          $userAllOrdersNames = OrdersModel::getAllOrdersNames('ord_name', false, false, $this->user->usr_id);
          $this->view->title = 'Użytkownik ' . $this->user->name;
          return $this->render('view', [
                      'user' => $this->user,
                      'projectDataProvider' => $projectDataProvider,
                      'projectSearchModel' => $projectSearchModel,
                      'userAllProjectsNames' => $userAllProjectsNames,
                      'orderDataProvider' => $orderDataProvider,
                      'orderSearchModel' => $orderSearchModel,
                      'userAllOrdersNames' => $userAllOrdersNames
          ]);
     }

     public function actionUpdate() {

//        $user->scenario = User::SCENARIO_UPDATE;
          $user = UsersModel::findOne([$this->user->usr_id]);
          
          if (\Yii::$app->request->isPost){
               $post = \Yii::$app->request->post();
               if (!$this->user->isAdmin() && !empty($post['UsersModel']['usr_username'])){
                    unset($post['UsersModel']['usr_username']);
               }
               if ($user->load($post) && $user->validate()) {
                    
                    if (!empty($user->newPassword)) {
                         if (!empty($user->currentPassword)) {
                              if ($user->validatePassword($user->currentPassword)) {
                                   $user->setPassword($user->newPassword);
                              } else {
                                   $user->addError('currentPassword', \Yii::t('app', 'Podane hasło jest nieprawidłowe'));
                                   return $this->render('update', ['user' => $user]);
                              }
                         } else {
                              $user->addError('currentPassword', \Yii::t('app', 'Podaj obecne hasło, aby je zmienić.'));
                              return $this->render('update', ['user' => $user]);
                         }
                    }
                    if ($user->save(false)){
                         Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany'));
                         return $this->redirect('view');
                    }
                    return $this->redirect('view');
               }
               return $this->redirect('update');
          }

          $this->view->title = 'Edytuj swoje dane';
          $this->user->currentPassword = '';
          $this->user->newPassword = '';
          $this->user->newPasswordConfirm = '';
          $groupsList = GroupsModel::getGroups();
          
          $this->view->params['breadcrumbs'][] = $this->view->title;
          return $this->render('/partials/user-form', ['user' => $user, 'groupsList' => $groupsList]);
     }

     public function actionAddProperty($property = null) {
          if ($this->user->load(Yii::$app->request->post()) && $this->user->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['view']);
          }

          return $this->renderPartial('partials/modal-add-property', [
                      'user' => $this->user,
                      'property' => $property
          ]);
     }
}
