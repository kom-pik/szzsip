<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\ProjectsModel;
use common\models\UsersModel;
use common\models\ProjectsSearchModel;
use common\models\ClientsModel;
use common\models\OrdersSearchModel;
use common\models\OrdersModel;
use common\models\GroupsModel;
use common\controllers\MyWebController;
use common\models\EntitiesCostsModel;
use common\models\EntitiesCostsModelSearch;
use frontend\services\ServiceClients;
use Exception;

class ProjectsController extends MyWebController {

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => ['index', 'add', 'delete', 'view', 'add-property', 'add-cost', 'edit-cost', 'delete-cost', 'update'],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                      [
                          'allow' => true,
                          'actions' => [''],
                          'roles' => ['?']
                      ]
                  ],
              ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
              'error' => [
                  'class' => 'yii\web\ErrorAction',
              ],
              'captcha' => [
                  'class' => 'yii\captcha\CaptchaAction',
                  'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
              ],
          ];
     }
     
     public function beforeAction($action) {
          if (parent::beforeAction($action)) {
               if ($this->user->isClient()) {
                    if (!in_array($action->id, array('view', 'add'))) {
                         $this->goBack('error', $this->messageNotAllowed);
                         return false;
                    }
               }
               return true;
          }
          return false;
     }

     /**
      * Displays homepage.
      *
      * @return mixed
      */
     public function actionIndex() {
          $searchModel = new ProjectsSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->client_ids, null, $this->group_id);
          $userList = UsersModel::findAllUsers($this->group_id, ['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $projectsNames = ProjectsModel::getAllProjectsNames($this->client_ids, null, 'pro_name', $this->group_id);

          $this->view->title = 'Projekty';
          return $this->render('index', [
                      'dataProvider' => $dataProvider,
                      'searchModel' => $searchModel,
                      'userList' => $userList,
                      'clientsList' => $clientsList,
                      'projectsNames' => $projectsNames
          ]);
     }

     public function actionView($id) {
          if (!Yii::$app->request->isAjax && $this->user->isClient()){
               return $this->goBack('error', $this->messageNotAllowed);
          }
          if (empty($id)){
               return $this->goBack('error', $this->messageNoParams);
          }
          $projectsModel = new ProjectsModel();
          $project = $projectsModel->findProject($id, $this->group_id);
          if (empty($project)) {
               return $this->goBack('error', $this->messageNotFound.'projektu');
          }
          $searchModel = new OrdersSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $project_id = $id, null);
          $userList = UsersModel::findAllUsers($this->group_id,['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $orderNames = OrdersModel::getAllOrdersNames('ord_name', $project_id = $id, false, false, $this->group_id);
          $orders = !empty($project->orders) ? $project->orders : array();
          $this->view->title = 'Projekt ' . $project->pro_name;
          
          $projectOrdersIds = $project->getOrders()->column();
          if (!empty($projectOrdersIds)){
               $id = array_merge((array) $id, $projectOrdersIds);
          }
          $costSearchModel = new EntitiesCostsModelSearch();
          $entitiesCostsDataProvider = $costSearchModel->search(Yii::$app->request->queryParams, $id, [EntitiesCostsModel::ENTITY_TYPE_PROJECTS, EntitiesCostsModel::ENTITY_TYPE_ORDERS]);
          $costsTabParams = [
               'entitiesCostsDataProvider' => $entitiesCostsDataProvider,
               'costSearchModel' => $costSearchModel
          ];
          $params = [                      
                      'project' => $project,
                      'searchModel' => $searchModel,
                      'dataProvider' => $dataProvider,
                      'userList' => $userList,
                      'clientsList' => $clientsList,
                      'orderNames' => $orderNames,
                      'orders' => $orders,
                      'costsTabParams' => $costsTabParams
                  ];
          
          if (Yii::$app->request->isAjax){
               $params['modal'] = true; 
               return $this->renderAjax('view', $params);
          }
          return $this->render('view', $params);
     }

     public function actionAdd($client_id = null) {
          $project = new ProjectsModel();
          $project->scenario = ProjectsModel::SCENARIO_ADD_PROJECT;
          if ($project->load(Yii::$app->request->post()) && $project->validate()) {
               $transaction = Yii::$app->db->beginTransaction();
               try {
                    if (empty($project->pro_group_fkey)){
                         $project->pro_group_fkey = $this->group_id;
                    }
                    $project->save();
                    $transaction->commit();
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano projekt'));
                    return $this->redirect(['view', 'id' => $project->pro_id]);
               } catch (ActionException $ex) {
                    $transaction->rollBack();
                    $project->addError('pro_name', $ex->getMessage());
               } catch (Exception $ex) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', $ex->getMessage());
               }
          }
          if (!empty($client_id)){
               $client = ServiceClients::findClientIndexById($client_id, $this->group_id);
               if (!empty($client)){
                    $project->pro_client_fkey = $client_id;
                    $project->pro_group_fkey = $project->client->cli_group_fkey;
               }
          }
          
          $userList = UsersModel::findAllUsers($this->group_id, ['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = !empty($client) ? $client : ClientsModel::findAllClients($this->client_ids, $this->group_id);
          $groupsList = GroupsModel::getGroups();
          return $this->renderAjax('add', [
                      'project' => $project,
                      'userList' => $userList,
                      'clientsList' => $clientsList,
                      'groupsList' => $groupsList
          ]);
     }

     public function actionUpdate($id) {
          $project = (new ProjectsModel())->findProject($id, $this->group_id);
          if (!empty($project->group)){
               $project->scenario = ProjectsModel::SCENARIO_UPDATE_PROJECT;
          }
          if (empty($project)){
               return $this->goBack('error', $this->messageNotFound.' projektu');
          }
          if (!$this->user->isAdmin() && $this->group_id != $project->pro_group_fkey){
               return $this->goBack('error', $this->messageNotAllowed);
          }
          if ($project->load(Yii::$app->request->post()) && $project->save(false)) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['view', 'id' => $project->pro_id]);
          }
          $this->view->title = 'Edytuj projekt';
          $userList = UsersModel::findAllUsers($this->group_id, ['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $groupsList = GroupsModel::getGroups();
          $params = [
               'project' => $project,
               'userList' => $userList,
               'clientsList' => $clientsList,
               'groupsList' => $groupsList
          ];
          if ($this->request->isAjax){
               return $this->renderAjax('update', $params);
          }
          return $this->render('update', $params);
     }

     public function actionAddProperty($id, $property = null, $action = 'view') {
          $project = (new ProjectsModel())->findProject($id, $this->group_id);
          if ($project->load(Yii::$app->request->post()) && $project->save(false)) {
               return $this->goBack('success', Yii::t('app', 'Zapisano zmiany.'));
          }

          $view = 'partials/modal-add-property';
          $params = [
              'project' => $project,
              'property' => $property,
              'action' => $action,
          ];
          return $this->renderAjax($view, $params);
     }
     
     public function actionAddCost($entityId, $entityType = EntitiesCostsModel::ENTITY_TYPE_PROJECTS){
          return parent::actionAddCost($entityId, $entityType);
     }
     
     public function actionCostsList($entityId, $entityType){
          return parent::actionCostsList($entityId, $entityType);
     }

}
