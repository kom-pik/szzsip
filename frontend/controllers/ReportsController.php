<?php

namespace frontend\controllers;

use common\controllers\MyWebController;
use yii\filters\AccessControl;
use common\models\ReportModel;

/**
 * Description of ReportsController
 *
 * @author Marcin Pikul
 */
class ReportsController  extends MyWebController {
     
     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => [],
                          'allow' => true,
                          'roles' => ['?'],
                      ],
                      [
                          'actions' => [
                               'prepare'
                               ],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
          ];
     }
     
     public function actionPrepare(){
          if ($this->request->isPost) {
               $reportModel = new ReportModel();
               if ($reportModel->load($this->request->post())) {
                    $report = $reportModel->generateReport();
                    return $this->renderPartial($report['view'], $report['params']);
               }
          }
     }
}
