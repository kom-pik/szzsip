<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\controllers;

use common\controllers\MyWebController;
use yii\filters\AccessControl;
use common\models\MigrationsModel;
use Yii;

/**
 * Description of SupportCOntroller
 *
 * @author Marcin Pikul
 */
class SupportController extends MyWebController {
     
     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => [],
                          'allow' => false,
                          'roles' => ['?'],
                      ],
                      [
                          'actions' => ['index', 'migrations'],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
              'error' => [
                  'class' => 'yii\web\ErrorAction',
              ],
              'captcha' => [
                  'class' => 'yii\captcha\CaptchaAction',
                  'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
              ],
          ];
     }

     public function beforeAction($action) {
          if (parent::beforeAction($action)) {
               if (!$this->user->isAdmin()) {
                    $this->redirect('/sites/index');
                    return false;
               }
               return true;
          }
          return false;
     }
     
     public function actionIndex() {
          
     }
     
     public function actionMigrations() {
          $migration = new MigrationsModel();
          
          if ($migration->load($this->request->post()) && $migration->saveMigration()) {
               return $this->goBack('success', 'Migracja zapisana');
          }
          $params = array(
               'migration' => $migration,
               'migrations' => scandir(Yii::getAlias("@sql"), SCANDIR_SORT_DESCENDING)
          );
          return $this->render('migrations', $params);
     }
}
