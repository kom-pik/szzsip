<?php

namespace frontend\controllers;

use Yii;
use common\controllers\MyWebController;
use yii\filters\AccessControl;
use common\models\UsersModel;
use common\models\TasksSearchModel;
use common\models\TasksModel;
use common\models\OrdersTasksModel;
use common\models\StartsStopsModel;

class TasksController extends MyWebController {

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => [],
                          'allow' => false,
                          'roles' => ['?'],
                      ],
                      [
                          'actions' => [
                               'index',
                               'add',
                               'update',
                               'delete',
                               'view',
                               'add-worktime',
                               'start-work',
                               'stop-work',
                               'pause-work'
                               ],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
              'error' => [
                  'class' => 'yii\web\ErrorAction',
              ],
              'captcha' => [
                  'class' => 'yii\captcha\CaptchaAction',
                  'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
              ],
          ];
     }

     public function beforeAction($action) {
          if (parent::beforeAction($action)) {
               if ($this->user->isClient()) {
                    $this->goBack('error', $this->messageNotAllowed);
                    return false;
               }
               return true;
          }
          return false;
     }

     /**
      * Displays homepage.
      *
      * @return mixed
      */
     public function actionIndex() {
          $searchModel = new TasksSearchModel();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, $this->group_id);
          $userList = UsersModel::findAllUsers(['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $tasksNames = TasksModel::getAllTasksNames(null, $this->group_id);
          return $this->render('index', [
                      'dataProvider' => $dataProvider,
                      'searchModel' => $searchModel,
                      'userList' => $userList,
                      'tasksNames' => $tasksNames
          ]);
     }

     public function actionView($id) {
          if (!empty($id)) {
               $task = TasksModel::findOne([$id]);
          } else {
               return $this->goBack('error', $this->messageNotFound);
          }

          if (!empty($task)) {
               $this->view->title = "Zadanie " . $task->tas_name;
               return $this->render('view', ['task' => $task]);
          }
          return $this->goBack('error', $this->messageNotFound);
     }

     public function actionAdd() {
          $task = new TasksModel();
          if ($task->load(Yii::$app->request->post()) && $task->validate()) {
               $task->tas_status = TasksModel::STATUS_ACTIVE;
               if (empty($task->tas_group_fkey)){
                    $task->tas_group_fkey = $this->group_id;
               }
               $task->save();
               Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano zadanie'));
               return $this->redirect('/tasks/index');
          }

          return $this->renderAjax('add', [
                      'task' => $task,
          ]);
     }

     public function actionUpdate($id) {
          $task = TasksModel::findOne([$id]);

          if ($task->load(Yii::$app->request->post()) && $task->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['index']);
          }
          return $this->render('update', [
                      'task' => $task
          ]);
     }

     public function actionStartWork($id, $type = 'start') {
          $orderTask = OrdersTasksModel::findOne([$id]);
          $startStop = new StartsStopsModel();

          $startStop->sts_order_task_fkey = $orderTask->ort_id;
          $startStop->sts_type = $type == 'start' ? StartsStopsModel::TYPE_START : StartsStopsModel::TYPE_RESUME;
          $startStop->sts_status = StartsStopsModel::STATUS_ACTIVE;

          if ($orderTask->lock() && $startStop->save(false)) {
               $header = Yii::t('app', 'Rozpocząłeś pracę nad zadaniem');
               $body = Yii::t('app', 'Nazwa zadania "' . $orderTask->task->tas_name . '".');
          } else {
               $header = Yii::t('app', 'Nie możesz rozpocząć pracy nad zadaniem');
               $body = Yii::t('app', 'Nazwa zadania "' . $orderTask->task->tas_name . '".<br>Sprawdź czy już ktoś nad nim nie pracuje.');
          }
          return $this->renderAjax('partials/modal-action-info', ['orderTask' => $orderTask, 'header' => $header, 'body' => $body]);
     }

     public function actionStopWork($id) {
          $orderTask = OrdersTasksModel::findOne([$id]);
          $startStop = new StartsStopsModel();

          $startStop->sts_order_task_fkey = $orderTask->ort_id;
          $startStop->sts_type = StartsStopsModel::TYPE_STOP;

          if ($orderTask->unlock() && $startStop->save()) {
               $header = Yii::t('app', 'Zakończyłeś pracę z zadaniem');
               $body = Yii::t('app', 'Nazwa zadania "' . $orderTask->task->tas_name . '".');
          } else {
               $header = Yii::t('app', 'Nie można zakończyć pracy nad zadaniem');
               $body = Yii::t('app', 'Nazwa zadania "' . $orderTask->task->tas_name . '".\nSpróbuj jeszcze raz.');
          }
          return $this->renderPartial('partials/modal-action-info', ['orderTask' => $orderTask, 'header' => $header, 'body' => $body]);
     }

     public function actionPauseWork($id) {
          $orderTask = OrdersTasksModel::findOne([$id]);
          $startStop = new StartsStopsModel();

          $startStop->sts_order_task_fkey = $orderTask->ort_id;
          $startStop->sts_type = StartsStopsModel::TYPE_PAUSE;

          if ($orderTask->pause() && $startStop->save()) {
               $header = Yii::t('app', 'Zatrzymałeś czas pracy nad zadaniem ');
               $body = Yii::t('app', 'Nazwa zadania "' . $orderTask->task->tas_name . '".');
          } else {
               $header = Yii::t('app', 'Nie można zatrzymać czasu pracy nad zadaniem');
               $body = Yii::t('app', 'Nazwa zadania "' . $orderTask->task->tas_name . '".\nSpróbuj jeszcze raz.');
          }
          return $this->renderPartial('partials/modal-action-info', ['orderTask' => $orderTask, 'header' => $header, 'body' => $body]);
     }
     
     public function actionAddWorktime($id) {
          $addWorkTimeModel = new \frontend\models\AddWorktimeForm();
          if ($addWorkTimeModel->load($this->request->post()) && $addWorkTimeModel->saveWorkTime($id)) {
               
          }
          return $this->renderAjax('add-worktime', array(
               'addWorkTimeModel' => $addWorkTimeModel
          ));
     }

}
