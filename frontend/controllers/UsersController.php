<?php

namespace frontend\controllers;

use Yii;
use common\controllers\MyWebController;
use yii\filters\AccessControl;
use common\models\ProjectsModel;
use common\models\ProjectsSearchModel;
use common\models\UsersModel;
use common\models\UsersSearchModel;
use common\models\ClientsModel;
use common\models\OrdersSearchModel;
use common\models\OrdersModel;
use frontend\services\ServiceGroups;
use Exception;

class UsersController extends MyWebController {

     /**
      * @inheritdoc
      */
     public function behaviors() {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'rules' => [
                      [
                          'actions' => [],
                          'allow' => true,
                          'roles' => ['?'],
                      ],
                      [
                          'actions' => ['index', 'add', 'update', 'delete', 'view', 'add-property'],
                          'allow' => true,
                          'roles' => ['@'],
                      ],
                  ],
              ],
          ];
     }

     /**
      * @inheritdoc
      */
     public function actions() {
          return [
              'error' => [
                  'class' => 'yii\web\ErrorAction',
              ],
              'captcha' => [
                  'class' => 'yii\captcha\CaptchaAction',
                  'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
              ],
          ];
     }

     public function beforeAction($action) {
          if (parent::beforeAction($action)) {
               if (!$this->user->isAdmin()) {
                    if (in_array($action->id, array('index'))) {
                         $this->goBack('error', $this->messageNotAllowed);
                         return false;
                    }
                    if (!$this->user->isSupervisor()) {
                         if (in_array($action->id, array('add', 'delete'))) {
                              $this->goBack('error', $this->messageNotAllowed);
                              return false;
                         }
                    }
                    if ($this->user->isClient()) {
                         if (!$this->request->isAjax && $action->id == 'view') {
                              $this->goBack('error', $this->messageNotAllowed);
                              return false;
                         }
                    }
               }
               return true;
          }
          return false;
     }

     /**
      * Displays homepage.
      *
      * @return mixed
      */
     public function actionIndex() {
          $searchModel = new UsersSearchModel();
//          dd(Yii::$app->request->queryParams);
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          return $this->render('index', [
                      'dataProvider' => $dataProvider,
                      'searchModel' => $searchModel,
          ]);
     }

     public function actionView($id) {
          if ($id == $this->user->usr_id) {
               return $this->redirect('/profiles/view');
          }
          $user = UsersModel::findOne([$id]);
          $projectSearchModel = new ProjectsSearchModel();
          $projectDataProvider = $projectSearchModel->search(Yii::$app->request->queryParams, null, $id);
          $userAllProjectsNames = ProjectsModel::getAllProjectsNames(null, $id);
          $orderSearchModel = new OrdersSearchModel();
          $orderDataProvider = $orderSearchModel->search(Yii::$app->request->queryParams, null, null, $id);
          $userAllOrdersNames = OrdersModel::getAllOrdersNames('ord_name', false, false, $id);
          $this->view->title = 'Użytkownik ' . $user->name;
          $params = [
              'user' => $user,
              'projectDataProvider' => $projectDataProvider,
              'projectSearchModel' => $projectSearchModel,
              'userAllProjectsNames' => $userAllProjectsNames,
              'orderDataProvider' => $orderDataProvider,
              'orderSearchModel' => $orderSearchModel,
              'userAllOrdersNames' => $userAllOrdersNames
          ];
          if ($this->request->isAjax) {
               $params['modal'] = true;
               return $this->renderAjax('view', $params);
          }
          return $this->render('view', $params);
     }

     public function actionAdd() {
          $user = new UsersModel();
          $user->scenario = UsersModel::SCENARIO_ADD;

          if ($user->load(Yii::$app->request->post()) && $user->validate()) {
               $transaction = Yii::$app->db->beginTransaction();
               try {
                    $user->setPassword($user->newPassword);
                    $user->usr_status = UsersModel::STATUS_ACTIVE;
                    if (empty($user->usr_group_fkey)) {
                         $user->usr_group_fkey = $this->group_id;
                    }
                    $user->save();
                    $transaction->commit();
                    $auth = Yii::$app->authManager;
                    $auth->assign($auth->getRole($user->usr_type), $user->getId());

                    Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano użytkownika'));
                    return $this->redirect(['/users/view', 'id' => $user->usr_id]);
               } catch (ActionException $ex) {
                    $transaction->rollBack();
                    $user->addError('usr_firstname', $ex->getMessage());
               } catch (Exception $ex) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', $ex->getMessage());
               }
          }
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $groupsList = ServiceGroups::findGroupsIndexById($this->group_id);
          $params = array(
              'user' => $user,
              'clientsList' => $clientsList,
              'groupsList' => $groupsList
          );
          if ($this->request->isAjax) {
               return $this->renderAjax('add', $params);
          }
          return $this->render('add', $params);
     }

     public function actionUpdate($id) {
          $user = UsersModel::findOne([$id]);
          if ($user->load(Yii::$app->request->post()) && $user->validate()) {
               if (!empty($user->newPassword)) {
                    $user->setPassword($user->newPassword);
               }
               $user->save();
               $auth = Yii::$app->authManager;
               $auth->revokeAll($user->getId());
               $auth->assign($auth->getRole($user->usr_type), $user->getId());
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return $this->redirect(['index']);
          }
          $usersList = UsersModel::findAllUsers(['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          $clientsList = ClientsModel::findAllClients(null, $this->group_id);
          $groupsList = ServiceGroups::findGroupsIndexById($this->group_id);
          $params = [
              'user' => $user,
              'usersList' => $usersList,
              'clientsList' => $clientsList,
              'groupsList' => $groupsList,
              'isAjax' => $this->request->isAjax
          ];
          
          
          return $this->request->isAjax ? $this->renderAjax('update', $params) : $this->render('update', $params);
     }

     public function actionAddProperty($id, $property = null) {
          $user = UsersModel::findOne([$id]);

          if ($user->load(Yii::$app->request->post()) && $user->save()) {
               Yii::$app->session->addFlash('success', Yii::t('app', 'Zapisano zmiany.'));
               return \yii\helpers\Json::encode(['success' => true, 'message' => 'Zapisano zmiany']);
          }
          $usersList = UsersModel::findAllUsers($this->group_id, ['!=', 'usr_type', UsersModel::TYPE_CLIENT]);
          return $this->renderAjax('partials/modal-add-property', [
                      'user' => $user,
                      'usersList' => $usersList,
                      'property' => $property
          ]);
     }

}
