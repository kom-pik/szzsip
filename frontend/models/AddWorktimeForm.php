<?php

//use Yii;

namespace frontend\models;

/**
 * Description of AddWorktimeForm
 *
 * @author Marcin Pikul
 */
class AddWorktimeForm extends \common\models\StartsStopsModel {
     public $startWork;
     public $endWork;
     public $groupFkey;
     public $userFkey;
     public $orderTaskFkey;
     
     public function rules() {
          return [
               [['startWork', 'endWork'], 'string'],
               [['startWork', 'endWork', 'groupFkey', 'userFkey'], 'required'],
               [['groupFkey', 'userFkey', 'orderTaskFkey'], 'integer'],
          ];
     }
     
     public function saveWorkTime($id) {
          $startsStopsModel = new \common\models\StartsStopsModel();
          $transaction = \Yii::$app->db->beginTransaction();
          try {
               $startsStopsModel->sts_created_at = strtotime($this->startWork);
               $startsStopsModel->saveStartStop($id);

               $startsStopsModel->sts_created_at = strtotime($this->endWork);
               $startsStopsModel->saveStartStop($id, 'stop-work');
               $transaction->commit();
               return true;
          } catch (Exception $ex) {
               $transaction->rollBack();
               return false;
          }
     }
}
