<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\UsersModel;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $usr_email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['usr_email', 'trim'],
            ['usr_email', 'required'],
            ['usr_email', 'email'],
            ['usr_email', 'exist',
                'targetClass' => '\common\models\UsersModel',
                'filter' => ['usr_status' => UsersModel::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user UsersModel */
        $user = UsersModel::find()->where([
            'usr_status' => UsersModel::STATUS_ACTIVE,
            'usr_email' => $this->usr_email,
        ])->one();
//        dd($user);

        if (!$user) {
            return false;
        }
        
        if (!UsersModel::isPasswordResetTokenValid($user->usr_password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->usr_email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }
}
