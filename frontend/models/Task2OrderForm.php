<?php

namespace frontend\models;

use common\models\OrdersTasksModel;
use yii\base\Model;
use Yii;

class Task2OrderForm extends Model {

     public $order_id;
     public $task_ids;

     public function rules() {
          return [
               ['order_id', 'each', 'rule' => ['integer'], 'when' => function ($model) {
                         return is_array($model->order_id);
                    }],
               ['task_ids', 'each', 'rule' => ['integer'], 'when' => function ($model) {
                         return is_array($model->task_ids);
                    }],
               [['order_id', 'task_ids'], 'required', 'message' => 'Pole {attribute} nie może być puste']
          ];
     }

     public function attributeLabels() {
          return [
               'order_id' => Yii::t('app', 'Zlecenie'),
               'task_ids' => Yii::t('app', 'Zadania'),
               'task_id' => Yii::t('app', 'Zadanie'),
          ];
     }

     /**
      * 
      * @param integer $group_id
      * @return boolean
      */
     public function addTask2Order($group_id) {
          $orderTask = new OrdersTasksModel();
          $orderTask->ort_group_fkey = $group_id;

          if (is_array($this->order_id)) {
               foreach ($this->order_id as $order_id) {
                    $orderTask->ort_order_fkey = $order_id;
                    $orderTask->ort_id = NULL;
                    $orderTask->isNewRecord = TRUE;
                    if (is_array($this->task_ids)) {
                         foreach ($this->task_ids as $task_id) {
                              $orderTask->ort_id = NULL;
                              $orderTask->isNewRecord = TRUE;
                              $orderTask->ort_task_fkey = $task_id;
                              $orderTask->save();
                         }
                    } else {
                         $orderTask->ort_task_fkey = $this->task_ids;
                         $orderTask->save();                         
                    }
               }
               return true;
          }
          $orderTask->ort_order_fkey = $this->order_id;
          if (is_array($this->task_ids)) {
               foreach ($this->task_ids as $task_id) {
                    $orderTask->ort_id = NULL;
                    $orderTask->isNewRecord = TRUE;
                    $orderTask->ort_task_fkey = $task_id;
                    $orderTask->save();
               }
               return true;
          }
          $orderTask->ort_task_fkey = $this->task_ids;
          $orderTask->save();
          return true;
     }

}
