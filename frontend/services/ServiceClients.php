<?php

namespace frontend\services;

use Yii;
use common\models\ClientsModel;
use Exception;

/**
 * Description of ServiceClients
 *
 * @author Marcin Pikul
 */
class ServiceClients {
     
     public static function findClientById($id, $group_id = null){
          $client = ClientsModel::find()
                  ->where(['cli_id' => $id])
                  ->onCondition(['!=', 'cli_status', ClientsModel::STATUS_DELETED]);
          if (!empty($group_id)){
               $client->andWhere(['cli_group_fkey' => $group_id]);
          }
          return $client->one();
     }

     public static function findClientIndexById($client_id, $group_id = null){
          $client = ClientsModel::find()
                  ->select('cli_acronym')
                  ->onCondition(['cli_id' => $client_id])
                  ->where(['cli_status' => ClientsModel::STATUS_ACTIVE]);
          if (!empty($group_id)){
               $client->andWhere(['cli_group_fkey' => $group_id]);
          }
          return $client->indexBy('cli_id')->column();
     }
     
     public function saveClient($client, $request, $gro_id) {
          if ($client->load($request->post()) && $client->validate()) {
               $transaction = Yii::$app->db->beginTransaction();
               try {
                    if (empty($client->cli_group_fkey)){
                         $client->cli_group_fkey = $gro_id;                         
                    }
                    $client->save();
                    $transaction->commit();
               } catch (ActionException $ex) {
                    $transaction->rollBack();
//                    $client->addError('cli_firstname', $ex);
               } catch (Exception $ex) {
                    $transaction->rollBack();
//                    $client->addError('cli_firstname', $ex);
               }
          }
          return $client;
     }
}
