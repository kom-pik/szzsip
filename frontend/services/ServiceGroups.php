<?php

namespace frontend\services;

use common\models\GroupsModel;
use yii\db\Query;
/**
 * Description of ServiceGroups
 *
 * @author Marcin Pikul
 */
class ServiceGroups {
     
     public static function findGroupsIndexById($group_id = null){
          $groups = (new Query())->select(['gro_name'])
                  ->from(GroupsModel::tableName());
          if (!empty($group_id)){
               $groups->where(['gro_id' => $group_id]);
          }
          $groups->andWhere(['!=', 'gro_status', GroupsModel::STATUS_DELETED]);
          return $groups->indexBy('gro_id')->column();
     }
}
