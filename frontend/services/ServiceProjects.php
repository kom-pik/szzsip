<?php

namespace frontend\services;

use Yii;
/**
 * Description of ServiceProjects
 *
 * @author Marcin Pikul
 */
class ServiceProjects {
     
     public static function saveProject($project, $input, $group_id = null){
          if ($project->load($input) && $project->validate()) {
               $transaction = Yii::$app->db->beginTransaction();
               try {
                    $project->pro_group_fkey = $group_id;
                    $project->save();
                    $id = $transaction->commit();
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Dodano projekt'));
                    return $project;
               } catch (ActionException $ex) {
                    $transaction->rollBack();
                    $project->addError('pro_name', $ex->getMessage());
               } catch (Exception $ex) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', $ex->getMessage());
               }
          }
          return $project;
     }
}
