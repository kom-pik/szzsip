<?php

namespace frontend\services;

use common\models\ReportModel;
/**
 * Description of ServiceReporting
 *
 * @author Marcin Pikul
 */
class ServiceReporting {
     
     public static function prepareProjectsData($projects, $reportModel, $includeTimeAndCost = false){
          $projectsCost = 0;
          $projectsTime = 0;
          $projectsData = array_map(function(&$project) use ($reportModel, &$projectsCost, &$projectsTime) {
               $project->getBudgetUse($reportModel->start_date, $reportModel->end_date);
               $project->getCostsValue($reportModel->start_date, $reportModel->end_date);
               $project->getProjectTime($reportModel->start_date, $reportModel->end_date);
               $projectsCost += $project->costsValue;
               $projectsTime += $project->projectTime;
               return $project;
          }, $projects);
          if ($includeTimeAndCost){
               $projectsData['projectsCost'] = $projectsCost;
               $projectsData['projectsTime'] = $projectsTime;
          }
          return $projectsData;
     }
     
     public static function prepareOrdersData($orders, $reportModel, $includeTimeAndCost = false){
          $ordersCost = 0;
          $ordersTime = 0;
          $ordersData = array_map(function(&$order) use ($reportModel, &$ordersCost, &$ordersTime) {
               $order->getBudgetUse($reportModel->start_date, $reportModel->end_date);
               $order->getCostsValue($reportModel->start_date, $reportModel->end_date);
               $order->getOrderTime($reportModel->start_date, $reportModel->end_date);
               $ordersCost += $order->costsValue;
               $ordersTime += $order->orderTime;
               return $order;
          }, $orders);
          if ($includeTimeAndCost){
               $ordersData['ordersCost'] = $ordersCost;
               $ordersData['ordersTime'] = $ordersTime;
          }
          return $ordersData;
     }
}
