<?php

namespace frontend\services;

use common\models\UsersModel;
use common\models\MyModel;
use common\models\ClientsModel;
use yii\db\Query;
use Yii;

/**
 * Description of ServiceUtilities
 *
 * @author Marcin Pikul
 */
class ServiceUtilities {
     
     const CONTROLLER_CLIENTS = 'clients';
     const CONTROLLER_GROUPS = 'groups';
     const CONTROLLER_ORDERS = 'orders';
     const CONTROLLER_PRICELISTS = 'pricelists';
     const CONTROLLER_PROFILES = 'profiles';
     const CONTROLLER_PROJECTS = 'projects';
     const CONTROLLER_REPORTS = 'reports';
     const CONTROLLER_SITES = 'sites';
     const CONTROLLER_TASKS = 'tasks';
     const CONTROLLER_USERS = 'users';
     
     public static function getClassForDeleteActionByController(){
          return array(
               self::CONTROLLER_CLIENTS => ClientsModel::className(),
               self::CONTROLLER_GROUPS => \common\models\GroupsModel::className(),
               self::CONTROLLER_ORDERS => \common\models\OrdersModel::className(),
               self::CONTROLLER_PRICELISTS => \common\models\PriceListsModel::className(),
               self::CONTROLLER_PROJECTS => \common\models\ProjectsModel::className(),
               self::CONTROLLER_TASKS => \common\models\TasksModel::className(),
               self::CONTROLLER_USERS => UsersModel::className()
          );
     }
     
     public static function massDelete($ids, MyModel $model, UsersModel $user){
          if (!empty($ids)){
               if ($model->className() == UsersModel::className()){
                    $ids = array_filter($ids, function($id) use ($user){
                         return $id != $user->getPrimaryKey();
                    });
                    if (empty($ids)){
                         return array('type' => 'error', 'message' => 'Nie możesz usunąć własnego konta');
                    }
                    
               }
               $objects = static::getByFields($model, array($model->idAttribute => (array) $ids), array($model->idAttribute, $model->groupAttribute), $user);
               if (!empty($objects)){
                    $objects= array_map(function($object) use ($model){
                         return $object[$model->idAttribute];
                    }, $objects);
                    $query = Yii::$app->db->createCommand()->update($model->tableName(), array(
                         $model->statusAttribute => $model::STATUS_DELETED,
                         $model->updatedByAttribute => $user->usr_id,
                         $model->updatedAtAttribute => time()
                            ), ['in', $model->idAttribute, $ids])->execute();
                    if ($query > 0){
                         return array('type' => 'success', 'message' => 'Usunięto wybrane wpisy');
                    }
                    return array('type' => 'error', 'message' => 'Usunięcie nie powiodło się.');
               }
               return array('type' => 'error', 'message' => 'Nie posiadasz uprawnień do usunięcia danych');
          }
          return array('type' => 'error', 'message' => 'Brak danych');
     }
     
     public static function getByFields(MyModel $model, $where, $fields, UsersModel $user, $fetchType = null){
          $query = (new Query())->select($fields)->from($model->tableName());
          foreach ($where as $field => $value){
               if (is_array($value)){
                    $query->where(['in', $field, $value]);
               } elseif (is_bool($value) || is_null($value)) {
                    $query->where(['is', $field, $value]);
               } else {
                    $query->where([$field => $value]);
               }
          }
          if (!$user->isAdmin()){
               $query->andWhere([$model->groupAttribute => $user->getAttribute($user->groupAttribute)]);
          }
          $query->andWhere(['!=', $model->statusAttribute, $model::STATUS_DELETED]);
          return $query->createCommand()->queryAll($fetchType);
     }
     

}
