<?php
use common\models\ClientsModel;
use yii\web\View;


/* @var $this View*/
/* @var $client ClientsModel*/

$this->title = 'Dodaj klienta';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left">Dodaj klienta</h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
               <?= $this->render('partials/client-form', [
                    'partialData' => $partialData
               ])?>
    </div>
</div>
