<?php
//

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\ClientsModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Klienci';
$this->params['breadcrumbs'][] = $this->title;
$user = $this->params['user'];
?>

<div class="site-index">

     <?=
     GridView::widget([
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-king"></i> Klienci',
          ], 'toolbar' => [
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj klienta', Url::to(['add']), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
               ],
               [
                    'content' => !$user->isClient() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczonych klientów'), Url::to(['delete']), ['class' => 'btn btn-danger grid-mass-delete disabled', 'data-pjax' => 0]) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['index'], [
                         'class' => 'btn btn-default reset-grid',
                         'title' => Yii::t('app', 'Resetuj widok')
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
               [
                    'class' => '\kartik\grid\CheckboxColumn'
               ],
               [
                    'class' => '\kartik\grid\SerialColumn'
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'acronym',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->cli_acronym, Url::to(['view', 'id' => $model->cli_id]), ['data-pjax' => 0, 'title' => $model->getName()]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $clientsList,
                         'options' => [
                              'placeholder' => 'filtruj po nazwie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true,
                         ]
                    ],
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->name, Url::to(['view', 'id' => $model->cli_id]), ['data-pjax' => 0, 'title' => $model->name]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => ClientsModel::listTypes(),
                         'options' => [
                              'placeholder' => 'filtruj po typie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'type',
                    'format' => 'raw',
                    'value' => function($model) {
                         return ClientsModel::listTypes()[$model->cli_type];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => ClientsModel::listStatuses(),
                         'options' => [
                              'placeholder' => 'filtruj po statusie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'status',
                    'value' => function($model) {
                         return ClientsModel::listStatuses()[$model->cli_status];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [
                         'presetDropdown' => 'true',
                         'pluginOptions'=> [
                              'locale' => [
                                   'format'=>'YYYY-MM-DD',
                              ]
                         ]
                    ],
                    'attribute' => 'created',
                    'value' => function ($model) {
                         return gmdate('d-m-Y H:i', $model->cli_created_at);
                    },
               ],
               [
                    'class' => '\kartik\grid\ActionColumn',
                    'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
                    'header' => 'Akcje',
                    'viewOptions' => [
                         'title' => 'Pokaż szczegóły'
                    ],
                    'updateOptions' => [
                         'title' => 'Edytuj'
                    ],
                    'deleteOptions' => [
                         'title' => 'Usuń'
                    ],
               ],
          ],
          'showPageSummary' => true,
     ])
     ?>
</div>