<?php

use common\models\ClientsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $client ClientsModel */
/* @var $disabled bool */
$client = $partialData['client'];
$groupsList = $partialData['groupsList'];
$userList = $partialData['userList'];
$priceLists = $partialData['priceLists'];
$options = array(empty($disabled) ? null : 'disabled' => 'disabled');
?>

<?php
$form = ActiveForm::begin([
     'options' => ['id' => 'client-form', 'class' => !empty($disabled) ? 'hidden-form' : ''],
     'action' => empty($disabled) ? '' : '/clients/add'
        ]);

if (!empty($disabled)){
     echo Html::hiddenInput('hiddenForm', true);
}
?>
<div class="modal-body">
     <div class="row">
          <?php echo !empty($disabled) ? Html::a('<i class="glyphicon glyphicon-chevron-left" ></i> Wróć', '', ['class' => 'show-client-form hide-form']) : ''?>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($client, 'cli_type')->dropDownList(ClientsModel::listTypes(), array_merge($options, [])) ?></div>
          <div class="col-sm-6"><?= $form->field($client, 'cli_acronym')->textInput(array_merge($options, [$client->isNewRecord ? null : 'readonly' => 'readonly'])) ?></div>
          
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($client, 'cli_firstname')->textInput(array_merge($options, [])) ?></div>
          <div class="col-sm-6"><?= $form->field($client, 'cli_lastname')->textInput(array_merge($options, [])) ?></div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($client, 'cli_nip')->textInput(array_merge($options, [])) ?></div>
          <div class="col-sm-6"><?= $form->field($client, 'cli_street')->textInput(array_merge($options, [])) ?></div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($client, 'cli_street_no')->textInput(array_merge($options, [])) ?></div>
          <div class="col-sm-6"><?= $form->field($client, 'cli_postcode')->textInput(array_merge($options, [])) ?></div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($client, 'cli_city')->textInput(array_merge($options, [])) ?></div>
          <div class="col-sm-6"><?= $form->field($client, 'cli_email')->textInput(array_merge($options, [])) ?></div>
     </div>
     <div class="row">
          <?php if ($this->params['user']->isAdminOrSupervisor()) :?>
          <div class="col-sm-6"><?= $form->field($client, 'cli_attendant_fkey')->dropDownList($userList, array_merge($options, [])) ?></div>
          <?php endif; ?>
          <div class="col-sm-6"><?= $form->field($client, 'cli_price_list_fkey')->dropDownList($priceLists, array_merge($options, [])) ?></div>
     </div>
     <div class="row">
          <div class="<?php echo $this->params['user']->isAdmin() ? 'col-sm-6' : 'col-sm-12' ?>"><?= $form->field($client, 'cli_info')->textarea(array_merge($options, ['class' => 'tinymce'])) ?></div>
          <?php if ($this->params['user']->isAdmin()):?>
          <div class="col-sm-6"><?= $form->field($client, 'cli_group_fkey')->dropDownList($groupsList, array_merge($options, [])) ?></div>
          <?php endif; ?>
     </div>
</div>

<div class="modal-footer">
     <?=
     Html::submitButton(Yii::t('app', 'Zapisz'), [
          'class' => 'btn btn-primary col-sm-12'
     ])
     ?>
</div>


<?php $form->end(); ?>


