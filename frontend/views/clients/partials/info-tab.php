<?php

use common\models\ClientsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $client ClientsModel */
?>
<div class="col-sm-12">
     <div class="row">
          <hr> 
     </div>
     <div class="row">
          <div class="col-sm-12">
               <label class=""><?= $client->getAttributeLabel('name') ?></label>
               <div><?= $client->name ?></div>
               <hr>
          </div>

          <?php if (!$this->params['user']->isClient()) : ?>
               <div class="col-sm-12">
                    <label><?= $client->getAttributeLabel('cli_info') ?></label>
                    <div><?= $client->cli_info ?></div>
                    <hr>
               </div>
          <?php endif; ?>

     </div>
     <div class="row">
          <div class="col-sm-6">
               <label><?= $client->getAttributeLabel('cli_phone') ?></label>
               <div><?php
                    if (!empty($client->cli_phone)) {
                         echo $client->cli_phone;
                         echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['add-property', 'id' => $client->cli_id, 'property' => 'phone'], ['class' => 'add-property m-l-5', 'title' => 'Zmień']);
                    } else {
                         echo Html::a(Yii::t('app', 'dodaj telefon'), ['add-property', 'id' => $client->cli_id, 'property' => 'phone'], ['class' => 'add-property']);
                    }
                    ?>
               </div>
          </div>
          <div class="col-sm-6">
               <label><?= $client->getAttributeLabel('cli_email') ?></label>
               <div><?php
                    if (!empty($client->cli_email)) {
                         echo Html::mailto(Yii::t('app', $client->cli_email), $client->cli_email, []);
                         echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['add-property', 'id' => $client->cli_id, 'property' => 'email'], ['class' => 'add-property m-l-5', 'title' => 'Zmień']);
                    } else {
                         echo Html::a(Yii::t('app', 'Dodaj e-mail'), Url::to(['add-property', 'id' => $client->cli_id, 'property' => 'email']), ['class' => 'add-property']);
                    }
                    ?>
               </div>
          </div>
     </div>
     <hr>
     <div class="row my-col-sm-2">
          <div class="col-sm-2">
               <label><?= $client->getAttributeLabel('cli_acronym') ?></label>
               <div><?= $client->cli_acronym ?></div><hr>
          </div>
          <div class="col-sm-2">
               <label><?= $client->getAttributeLabel('cli_type') ?></label>
               <div><?= ClientsModel::listTypes()[$client->cli_type] ?></div><hr>
          </div>
          <div class="col-sm-2">
               <label><?= $client->getAttributeLabel('cli_attendant_fkey') ?></label>
               <div>
                    <?php
                    if (!empty($client->cli_attendant_fkey)) {
                         echo $client->attendant->usr_username;
                         if ($this->params['user']->isAdminOrSupervisor()) {
                              echo Html::a('<i class="glyphicon glyphicon-refresh"></i>', ['add-property', 'id' => $client->cli_id, 'property' => 'attendant'], [
                                   'class' => 'add-property m-l-5',
                                   'title' => 'Zmień opiekuna klienta '
                              ]);
                         }
                    } elseif (!$this->params['user']->isClient()) {
                         echo Html::a('dodaj opiekuna', ['add-property', 'id' => $client->cli_id, 'property' => 'attendant'], [
                              'class' => 'add-property',
                              'title' => 'Dodaj opiekuna dla klienta ' . $client->cli_acronym
                         ]);
                    } else {
                         echo '-';
                    }
                    ?>
               </div>
               <hr>
          </div>

          <div class="col-sm-2">
               <label><?= $client->getAttributeLabel('cli_created_at') ?></label>
               <div><?= date('d-m-Y H:i', $client->cli_created_at) ?></div><hr>
          </div>

          <div class="col-sm-2">
               <label><?= $client->getAttributeLabel('cli_status') ?></label>
               <div><?=
                    ClientsModel::listStatuses()[$client->cli_status] . '' .
                    ($this->params['user']->isClient() ? '' : Html::a(Yii::t('app', '<i class="glyphicon glyphicon-refresh"></i>'), Url::to(['add-property', 'id' => $client->cli_id, 'property' => 'status']), [
                                 'class' => 'add-property m-l-5',
                                 'title' => 'Zmień status'
                    ]));
                    ?>
               </div><hr>
          </div>
     </div>
</div>

