<?php

use common\models\ClientsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $client ClientsModel*/
/* @var $this View */
/* @var $property string*/
$form = new ActiveForm();

switch ($property){
    case 'attendant':
        $header = 'Dodaj opiekuna dla klienta '.$client->name;
        $field = $form->field($client, 'cli_attendant_fkey')->dropDownList($usersList);
        break;
    case 'email':
        $header = 'Dodaj e-mail dla klienta '.$client->name;
        $field = $form->field($client, 'cli_email')->input('email');
        break;
    case 'status':
        $header = 'Zmień status klienta '.$client->name;
        $field = $form->field($client, 'cli_status')->dropDownList(ClientsModel::listStatuses());
        break;
    case 'phone':
        $header = 'Dodaj telefon klienta '.$client->name;
        $field = $form->field($client, 'cli_phone')->textInput();
        break;
}
?> 
<div class="modal-dialog">
    <div class="modal-content">
        <?php $form->begin([
            'options' => ['id' => 'add-property-form'],
        ]); ?>
        <div class="modal-header">
               <h3 class="pull-left"><?= $header?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        
        <div class="modal-body">
            <?= $field ?>
        </div>

        <div class="modal-footer">
            <?= Html::submitButton(Yii::t('app', 'Zapisz'), [
                'class' => 'btn btn-primary col-sm-12'
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>