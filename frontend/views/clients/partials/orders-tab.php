<?php

use common\models\ClientsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\OrdersModel;
use common\helpers\TimeHelper;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;

/* @var $this View */
/* @var $user UsersModel */
/* @var $client ClientsModel */

$user = $this->params['user'];
$columns = [
     [
          'class' => '\kartik\grid\CheckboxColumn'
     ],
     [
          'class' => '\kartik\grid\SerialColumn'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => $clientAllOrdersNames,
               'options' => [
                    'placeholder' => 'filtruj po nazwie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
               ]
          ],
          'attribute' => 'name',
          'format' => 'raw',
          'value' => function($model) use ($user) {
               return Html::a($model->getShortName(), Url::to(['/orders/view', 'id' => $model->ord_id]), ['data-pjax' => 0, 'title' => $model->ord_name, 'class' => $user->isClient() ? 'modal-preview' : '']);
          }
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => OrdersModel::listStatuses(),
               'options' => [
                    'placeholder' => 'filtruj po statusie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true
               ]
          ],
          'attribute' => 'status',
          'value' => function($model) {
               return OrdersModel::listStatuses()[$model->ord_status];
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_DATE_RANGE,
          'attribute' => 'created',
          'filterWidgetOptions' => [
               'presetDropdown' => true,
               'pluginOptions' => [
                    'locale' => [
                         'format' => 'YYYY-MM-DD',
                    ]
               ],
               'pluginEvents' => [
                    "apply.daterangepicker" => "function() { apply_filter('date') }",
               ]
          ],
          'value' => function ($model) {
               return date('d-m-Y H:i', $model->ord_created_at);
          },
          'label' => 'Utworzony'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'attribute' => 'time',
          'filter' => false,
          'value' => function ($model) {
               return TimeHelper::HourMinSec($model->getOrderTime());
          },
          'label' => 'Liczba godzin'
     ],
];
if (!$user->isClient()) {
     $columns[] = [
          'class' => '\kartik\grid\ActionColumn',
          'header' => 'Akcje',
          'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
          'viewOptions' => [
               'title' => 'Pokaż szczegóły'
          ],
          'updateOptions' => [
               'title' => 'Edytuj'
          ],
          'deleteOptions' => [
               'title' => 'Usuń'
          ],
     ];
}
?>

<div class="site-index">

     <?=
     GridView::widget([
          'id' => 'client-orders-tab-grid',
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-list"></i> Zlecenia',
          ],
          'showPageSummary' => true,
          'toolbar' => [
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj zlecenie', Url::to(['/orders/add']), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
               ],
               [
                    'content' => $user->isAdminOrSupervisor() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone zlecenia'), Url::to(['/orders/delete']), ['class' => 'btn btn-danger grid-mass-delete']) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $client->cli_id], [
                         'class' => 'btn btn-default grid-reset',
                         'title' => Yii::t('app', 'Resetuj widok'),
                         'data-pjax-target' => '#client-orders-tab-grid-pjax'
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $orderDataProvider,
          'filterModel' => $orderSearchModel,
          'columns' => $columns
     ])
     ?>
</div>

