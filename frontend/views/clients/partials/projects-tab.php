<?php

use common\models\ClientsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\ProjectsModel;
use common\models\UsersModel;
use common\helpers\TimeHelper;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;

/* @var $this View */
/* @var $user UsersModel */
/* @var $client ClientsModel */

$user = $this->params['user'];
$columns = [
     [
          'class' => '\kartik\grid\CheckboxColumn'
     ],
     [
          'class' => '\kartik\grid\SerialColumn'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => $clientAllProjectsNames,
               'options' => [
                    'placeholder' => 'filtruj po nazwie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
               ]
          ],
          'attribute' => 'name',
          'format' => 'raw',
          'value' => function($model) use ($user) {
               return Html::a($model->getShortName(), Url::to(['/projects/view', 'id' => $model->pro_id]), ['data-pjax' => 0, 'title' => $model->pro_name, 'class' => $user->isClient() ? 'modal-preview' : '']);
          }
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => ProjectsModel::listStatuses(),
               'options' => [
                    'placeholder' => 'filtruj po statusie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true
               ]
          ],
          'attribute' => 'status',
          'value' => function($model) {
               return ProjectsModel::listStatuses()[$model->pro_status];
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_DATE_RANGE,
          'attribute' => 'created',
          'filterWidgetOptions' => [
               'presetDropdown' => true,
               'pluginOptions' => [
                    'locale' => [
                         'format' => 'YYYY-MM-DD',
                    ]
               ],
               'pluginEvents' => [
                    "apply.daterangepicker" => "function() { apply_filter('date') }",
               ]
          ],
          'value' => function ($model) {
               return date('d-m-Y H:i', $model->pro_created_at);
          },
          'label' => 'Utworzony'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filter' => false,
          'attribute' => 'time',
          'format' => 'raw',
          'value' => function ($model) {
             return TimeHelper::HourMinSec($model->getProjectTime());
          },
          'label' => 'Liczba godzin'
     ],
];

if (!$user->isClient()) {
     $columns[] = [
          'class' => '\kartik\grid\ActionColumn',
          'header' => 'Akcje',
          'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
          'viewOptions' => [
               'title' => 'Pokaż szczegóły'
          ],
          'updateOptions' => [
               'title' => 'Edytuj',
               'visible' => 'false'
          ],
          'deleteOptions' => [
               'title' => 'Usuń'
          ],
     ];
}
?>

<div class="site-index">

     <?=
     GridView::widget([
          'id' => 'client-projects-tab-grid',
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-briefcase"></i> Projekty',
          ],
          'showPageSummary' => true,
          'toolbar' => [
               [
                    'content' => $user->isClient() ? '' : Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj projekt', Url::to(['/projects/add', 'client_id' => $client->cli_id]), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
               ],
               [
                    'content' => $user->isAdminOrSupervisor() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone projekty'), Url::to(['/projects/delete']), ['class' => 'btn btn-danger grid-mass-delete']) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $client->cli_id], [
                         'class' => 'btn btn-default grid-reset',
                         'title' => Yii::t('app', 'Resetuj widok'),
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $projectDataProvider,
          'filterModel' => $projectSearchModel,
          'columns' => $columns
     ])
     ?>
</div>
