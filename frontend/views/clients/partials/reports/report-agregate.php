<?php
use common\models\ReportModel;
use common\models\ClientsModel;
use common\models\ProjectsModel;
use common\models\OrdersModel;
use common\helpers\TimeHelper;
use yii\helpers\Html;

/* @var $reportModel ReportModel*/
/* @var $client ClientsModel*/
/* @var $project ProjectsModel */
/* @var $order OrdersModel */

if (!empty($projectsData)) :
?>
<div>
     <div class="report-label-div m-t-5"><label>Projekty ze zleceniami</label></div>
     <?php foreach ($projectsData as $project): ?>
     <div class="row">
          <div class="col-sm-12">
               <label><?php echo $project->getAttributeLabel('pro_name')?>:&nbsp;</label><?php echo Html::a($project->pro_name, ['projects/view', 'id' => $project->pro_id], ['class' => 'report-entity-name']) ?>
          </div>
     </div>
     <div class="row">
          <div class="col-sm-2">
               <label>Budżet</label>
               <div class="text-center"><?php echo !empty($project->pro_budget_type) && !empty($project->pro_budget_value) ? $project::listBudgetTypes()[$project->pro_budget_type] ." ({$project->pro_budget_value})": '-'?></div>
          </div>
          <div class="col-sm-2">
               <label>% wykorzystania</label>
               <div class="text-center"><?php echo !empty($project->pro_type) && !empty($project->pro_budget_value) ? number_format($project->budgetUse*100, 2, ',', ' ').'%' : '-'?></div>
          </div>
          <div class="col-sm-2">
               <label>Koszty</label>
               <div class="text-center"><?php echo number_format($project->costsValue, 2, ',', ' ')?> PLN</div>
          </div>
          <div class="col-sm-2">
               <label>Liczba godzin</label>
               <div class="text-center"><?php echo TimeHelper::HourMinSec($project->projectTime)?></div>
          </div>
     </div>
     <?php if (!empty($project->ordersData)) :?>
          <?php $i = 1;?>
     <hr>
     <ol>
          <?php foreach ($project->ordersData as $order):?>
          <div class="row report-order-row">
               <li class="m-t-5">
                    <div class="col-sm-3">
                         <label><?php echo $order->getAttributeLabel('ord_name')?>:&nbsp;</label><?php echo Html::a($order->ord_name, ['orders/view', 'id' => $order->ord_id], ['class' => 'report-entity-name', 'target' => '_blank']) ?>
                    </div>
                    <div class="col-sm-2">
                         <label>Budżet</label>
                         <div class="text-center"><?php echo !empty($order->ord_budget_type) ? $order::listBudgetTypes()[$order->ord_budget_type] : ' brak typu '?> (<?php echo !empty($order->ord_budget_value) ? $order->ord_budget_value : 'brak wartości'?>)</div>
                    </div>
                    <div class="col-sm-2">
                         <label>% wykorzystania</label>
                         <div class="text-center"><?php echo !empty($order->ord_budget_type) && !empty($order->pro_budget_value) ? number_format($order->budgetUse*100, 2, ',', ' ').'%' : '-'?></div>
                    </div>
                    <div class="col-sm-2">
                         <label>Koszty</label>
                         <div class="text-center"><?php echo number_format($order->costsValue, 2, ',', ' ')?> PLN</div>
                    </div>
                    <div class="col-sm-2">
                         <label>Liczba godzin</label>
                         <div class="text-center"><?php echo TimeHelper::HourMinSec($order->orderTime)?></div>
                    </div>
               </li>
          </div>
          <hr>
          <?php $i++;?>
          <?php endforeach;?>
     </ol>     
     
     <?php endif;?>
     <hr>
     <?php endforeach;?>
     <?php if (!empty($ordersData)) : ?>
          <?php echo $this->render('report-orders', ['clientOrdersData' => $ordersData, 'summary' => false, 'client' => $client])?>
     <?php endif;?>
     <div class="report-label-div m-t-5"><label>Podsumowanie</label></div>
     <div class="report-summary row">
          <div class="col-sm-2">
               
          </div>
          <div class="col-sm-2">
               
          </div>
          <div class="col-sm-2">
               <label>Koszty razem</label>
               <div class="text-center"><?php echo number_format($client->costsValue, 2, ',', ' ')?> PLN</div>
          </div>
          <div class="col-sm-2">
               <label>Liczba godzin razem</label>
               <div class="text-center"><?php echo TimeHelper::HourMinSec($client->clientTime)?></div>
          </div>
     </div>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif; 
