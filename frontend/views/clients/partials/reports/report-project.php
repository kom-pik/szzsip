<?php
use common\models\ReportModel;
use common\models\ClientsModel;
use common\helpers\TimeHelper;
use common\models\ProjectsModel;
use yii\helpers\Html;

/* @var $reportModel ReportModel*/
/* @var $client ClientsModel*/
/* @var $clientProjectsData ProjectsModel[]*/

if (!empty($clientProjectsData)) :
?>
<div>
     <div class="report-label-div m-t-5"><label>Projekty</label></div>
     <?php foreach ($clientProjectsData as $project): ?>
     <div class="row">
          <div class="col-sm-12">
               <label><?php echo $project->getAttributeLabel('pro_name')?>:&nbsp;</label><?php echo Html::a($project->pro_name, ['projects/view', 'id' => $project->pro_id], ['class' => 'report-entity-name']) ?>
          </div>
     </div>
     <div class="row">
          <div class="col-sm-2">
               <label>Budżet</label>
               <div class="text-center"><?php echo !empty($project->pro_budget_type) && !empty($project->pro_budget_value) ? $project::listBudgetTypes()[$project->pro_budget_type] ." ({$project->pro_budget_value})": '-'?></div>
          </div>
          <div class="col-sm-2">
               <label>% wykorzystania</label>
               <div class="text-center"><?php echo !empty($project->pro_type) && !empty($project->pro_budget_value) ? number_format($project->budgetUse*100, 2, ',', ' ').'%' : '-'?></div>
          </div>
          <div class="col-sm-2">
               <label>Koszty</label>
               <div class="text-center"><?php echo number_format($project->costsValue, 2, ',', ' ')?> PLN</div>
          </div>
          <div class="col-sm-2">
               <label>Liczba godzin</label>
               <div class="text-center"><?php echo TimeHelper::HourMinSec($project->projectTime)?></div>
          </div>
     </div>
     <hr>
     <?php endforeach;?>
     <div class="report-summary row">
          <div class="report-label-div m-t-5"><label>Podsumowanie</label></div>
          <div class="col-sm-2">
               
          </div>
          <div class="col-sm-2">
               
          </div>
          <div class="col-sm-2">
               <label>Koszty razem</label>
               <div class="text-center"><?php echo number_format($client->costsValue, 2, ',', ' ')?> PLN</div>
          </div>
          <div class="col-sm-2">
               <label>Liczba godzin razem</label>
               <div class="text-center"><?php echo TimeHelper::HourMinSec($client->clientTime)?></div>
          </div>
     </div>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif; 
