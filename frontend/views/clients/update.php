<?php
use common\models\ClientsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $client ClientsModel */
/* @var $this View*/

$this->title = 'Edytuj klienta';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('partials/client-form', [
     'partialData' => $partialData
])?>


