<?php

use yii\helpers\Html;
use yii\web\View;
use common\models\ClientsModel;
use yii\bootstrap\Tabs;

/* @var $client ClientsModel */
/* @var $this View*/


$this->params['breadcrumbs'][] = $this->title;
?>
<div>
     <?php 
     echo $this->params['user']->isClient() ? '' : Html::a(Yii::t('app', '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edytuj dane klienta'), 
             ['update', 'id' => $client->cli_id ], 
             [
                 'class' => 'btn btn-success',
                 'id' => 'btn-edit'
                 ]);
             
     echo Tabs::widget([
         'items' => [
             [
                 'label' => 'Informacje ogólne',
                 'active' => 'true',
                 'options' => [
                     'id' => 'info-tab'
                 ],
                 'content' => $this->render('partials/info-tab', ['client' => $client])
             ],
             [
                 'label' => 'Projekty',
                 'options' => [
                    'id' => 'projects-tab',
                 ],
                 'content' => $this->render('partials/projects-tab', [
                     'client' => $client,
                     'projectDataProvider' => $projectDataProvider,
                     'projectSearchModel' => $projectSearchModel,
                     'clientAllProjectsNames' => $clientAllProjectsNames
                         ])
             ],
             [
                 'label' => 'Zlecenia',
                 'options' => [
                    'id' => 'orders-tab',
                 ],
                 'content' => $this->render('partials/orders-tab', [
                     'client' => $client,
                     'orderDataProvider' => $orderDataProvider,
                     'orderSearchModel' => $orderSearchModel,
                     'clientAllOrdersNames' => $clientAllOrdersNames
                         ])
             ],
             [
                 'label' => 'Użytkownicy',
                 'options' => [
                    'id' => 'users-tab',
                 ],
                 'content' => $this->render('partials/users-tab', [
                     'client' => $client,
                     'userDataProvider' => $userDataProvider,
                     'userSearchModel' => $userSearchModel,
                     'usersList' => $usersList
                         ])
             ],
             [
                 'label' => 'Raporty',
                 'options' => [
                     'id' => 'reports-tab'
                 ],
                 'content' => $this->render('partials/reports-tab', [
                     'client' => $client,
                     'data' => $reportTabData
                 ])
             ]
         ]
     ]);
     ?>
</div>