<?php

use common\models\GroupsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use common\model\UsersModel;

/* @var $group GroupsModel */
/* @var $this View */
/* @var $user UsersModel */

$this->title = 'Dodaj grupę';
$this->params['breadcrumbs'][] = $this->title;
$user = $this->params['user'];
?>
<div class="modal-dialog modal-lg">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left"><i class="glyphicon glyphicon-plus"></i> Dodaj grupę</h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <?php
          $form = ActiveForm::begin([
                       'options' => [
                            'id' => 'group-form',
                       ],
                  ])
          ?>
          <div class="modal-body">
               <div class="row">
                    <div class="col-md-8">
                         <?= $form->field($group, 'gro_name')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                         <?php
//                         echo $form->field($order, 'ord_status')->dropDownList($orderStatus, [
//                              'prompt' => $user->isClient() ? null : 'Wybierz status (domyślnie nowy)',
//                         ])
                         ?>
                    </div>
               </div>

               <div class="row">
                    <div class="col-md-6">
                         <?=
                              $form->field($group, 'gro_supervisor_fkey')
                              ->dropDownList($userList, [
                                   'prompt' => 'Wybierz przełożonego',
                         ]);
                         ?>
                    </div>
               </div>
               <div class="row">
               </div>   
               <div class="row">
                    <div class="col-md-12">
                         <?= $form->field($group, 'gro_description')->textArea(['class' => 'tinymce']) ?>
                    </div>
               </div>


          </div>
          <div class="modal-footer">
               <?=
               Html::submitButton(Yii::t('app', 'Zapisz'), [
                    'class' => 'btn btn-primary col-sm-12'
               ])
               ?>
               <?php $form->end(); ?> 
          </div>
     </div>


<script>
     tinymce.init(tinymceDefaultOptions);
</script>