<?php

use yii\web\View;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this View */

$user = $this->params['user'];
?>
<div class="site-index">

     <?=
     GridView::widget([
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-bullhorn"></i> Grupy',
          ], 'toolbar' => [
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj grupę', Url::to(['add']), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
               ],
               [
                    'content' => $user->isAdmin() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone grupy'), Url::to(['delete']), ['class' => 'btn btn-danger grid-mass-delete disabled', 'data-pjax' => 0]) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['index'], [
                         'class' => 'btn btn-default reset-grid',
                         'title' => Yii::t('app', 'Resetuj widok')
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
               [
                    'class' => '\kartik\grid\CheckboxColumn'
               ],
               [
                    'class' => '\kartik\grid\SerialColumn'
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
//                    'filterWidgetOptions' => [
//                        'data' => ClientsModel::findAllClients(),
//                        'options' => [
//                            'placeholder' => 'filtruj po nazwie ...',
//                            'initValueText' => ''
//                            ],
//                        'pluginOptions' => [
//                            'allowClear' => true,
////                            'minimumInputLength' => 3,
//                        ]
//                    ],
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->gro_name, Url::to(['view', 'id' => $model->gro_id]), ['data-pjax' => 0, 'title' => $model->gro_name]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
//                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
//                        'data' => $userList,
                         'options' => [
                              'placeholder' => 'filtruj po kierowniku ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true,
//                            'minimumInputLength' => 3,
                         ]
                    ],
                    'attribute' => 'supervisor',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->supervisor->getName(), Url::to(['/users/view', 'id' => $model->supervisor->usr_id]), ['data-pjax' => 0, 'title' => $model->getName()]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'attribute' => 'created',
                    'filterWidgetOptions' => [
                         'presetDropdown' => true,
                         'pluginOptions' => [
                              'locale' => [
                                   'format' => 'YYYY-MM-DD',
                              ]
                         ],
                         'pluginEvents' => [
                              "apply.daterangepicker" => "function() { apply_filter('date') }",
                         ]
                    ],
                    'value' => function ($model) {
                         return gmdate('d-m-Y', $model->gro_created_at);
                    },
               ],
               [
                    'class' => '\kartik\grid\ActionColumn',
                    'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
                    'header' => 'Akcje',
                    'viewOptions' => [
                         'title' => 'Pokaż szczegóły'
                    ],
                    'updateOptions' => [
                         'title' => 'Edytuj'
                    ],
                    'deleteOptions' => [
                         'title' => 'Usuń'
                    ],
               ],
          ],
          'showPageSummary' => true,
     ])
     ?>
</div>

