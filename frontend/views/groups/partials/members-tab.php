<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\UsersModel;
use common\models\GroupsModel;
use kartik\grid\GridView;

/* @var $this View */
/* @var $user UsersModel */
/* @var $group GroupsModel */

$group = $membersTab['group'];

$user = $this->params['user'];
$columns = [
     [
          'class' => '\kartik\grid\CheckboxColumn'
     ],
     [
          'class' => '\kartik\grid\SerialColumn'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => $membersTab['membersList'],
               'options' => [
                    'placeholder' => 'filtruj po nazwie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
               ]
          ],
          'attribute' => 'name',
          'format' => 'raw',
          'value' => function($model) use ($user) {
               return Html::a($model->getName(), Url::to(['/users/view', 'id' => $model->usr_id]), ['data-pjax' => 0, 'title' => $model->getName(), 'class' => $user->isClient() ? 'modal-preview' : '']);
          }
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => UsersModel::listStatuses(),
               'options' => [
                    'placeholder' => 'filtruj po statusie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true
               ]
          ],
          'attribute' => 'status',
          'value' => function($model) {
               return UsersModel::listStatuses()[$model->usr_status];
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_DATE_RANGE,
          'attribute' => 'created',
          'filterWidgetOptions' => [
               'presetDropdown' => true,
               'pluginOptions' => [
                    'locale' => [
                         'format' => 'YYYY-MM-DD',
                    ]
               ],
               'pluginEvents' => [
                    "apply.daterangepicker" => "function() { apply_filter('date') }",
               ]
          ],
          'value' => function ($model) {
               return date('d-m-Y H:i', $model->usr_created_at);
          },
          'label' => 'Utworzony'
     ],
];
if ($user->isAdminOrSupervisor()) {
     $columns[] = [
          'class' => '\kartik\grid\ActionColumn',
          'header' => 'Akcje',
          'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
          'controller' => 'users',
          'viewOptions' => [
               'title' => 'Pokaż szczegóły'
          ],
          'updateOptions' => [
               'title' => 'Edytuj'
          ],
          'deleteOptions' => [
               'title' => 'Usuń'
          ],
     ];
}
?>

<div class="site-index">

     <?=
     GridView::widget([
          'id' => 'client-users-tab-grid',
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-user"></i> Członkowie',
          ],
          'showPageSummary' => true,
          'toolbar' => [
               [
                    'content' => $user->isAdminOrSupervisor() ? Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj użytkownika', Url::to(['/users/add']), ['class' => 'btn btn-success add-object', 'data-pjax' => 0]) : ''
               ],
               [
                    'content' => $user->isAdminOrSupervisor() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczonych użytkowników'), Url::to(['/users/delete']), ['class' => 'btn btn-danger grid-mass-delete disabled', 'data-pjax' => 0]) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $user->isAdmin() ? $group->gro_id : null], [
                         'class' => 'btn btn-default grid-reset',
                         'title' => Yii::t('app', 'Resetuj widok'),
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $membersTab['membersDataProvider'],
          'filterModel' => $membersTab['membersSearchModel'],
          'columns' => $columns
     ])
     ?>
</div>

