<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use common\models\ReportModel;
use common\models\GroupsModel;

/* @var $reportModel ReportModel */
/* @var $group GroupsModel */

$reportModel = $reportsTab['reportModel'];
$group = $reportsTab['group'];
?>
<div class="site-index m-t-5">
     <div class="row">
          <?php
          $form = ActiveForm::begin([
                       'id' => 'report-form',
                       'action' => '/groups/reporting',
                       'method' => 'POST',
                       'enableClientValidation' => false,
                       'options' => [
                            'data-pjax' => true,
                            'tag' => null
                       ]
          ]);
          echo Html::hiddenInput('ReportModel[gro_id]', $group->gro_id);
          ?>
          <div class="col-sm-12">
               <div class="col-sm-4">

                    <label>Wybierz zakres dat:</label>
                    <?php
                    echo DatePicker::widget([
                         'form' => $form,
                         'model' => $reportModel,
                         'type' => DatePicker::TYPE_RANGE,
                         'size' => 'sm',
                         'readonly' => true,
                         'attribute' => 'start_date',
                         'attribute2' => 'end_date',
                         'options' => [
                              'placeholder' => $reportModel->attributeLabels()['start_date'],
                              'value' => date('Y-m-d', strtotime('first day of this month')),
                              'style' => 'border: none',
                         ],
                         'options2' => [
                              'placeholder' => $reportModel->attributeLabels()['end_date'],
                              'value' => date('Y-m-d', strtotime('today')),
                              'style' => 'border: none',
                         ],
                         'pluginOptions' => [
                              'format' => 'yyyy-mm-dd',
                              'separator' => 'do',
                              'convertFormat' => true,
                              'autoclose' => true,
                              'todayHighlight' => true,
                              'todayBtn' => true,
                         ],
                         'layout' => '<span class="input-group-addon"> Od </span>{input1} <span class="input-group-addon"> do </span> {input2}'
                    ]);
                    ?>
               </div>
               <div class="col-sm-4">
                    <?php
                    echo $form->field($reportModel, 'type')->dropDownList(ReportModel::$reportsTypes, ['prompt' => 'Wybierz rodzaj raportu']);
                    ?>

               </div>
               <label class="control-label">&nbsp;</label>
               <div class="col-sm-4">
                    <?php
                    echo Html::button('Zatwierdź', [
                         'id' => 'report-submit',
                         'class' => 'btn btn-success',
                    ]);
                    ?>
               </div>
               <?php
               ActiveForm::end();
               ?>
          </div>
     </div>
     <div id="report-data" class="row">
     </div>
</div>