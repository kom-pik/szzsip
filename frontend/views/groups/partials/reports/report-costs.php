<?php
use common\models\ReportModel;
use common\models\ClientsModel;
use common\helpers\TimeHelper;
use common\models\EntitiesCostsModel;

/* @var $reportModel ReportModel*/
/* @var $client ClientsModel*/

if (!empty($client)) :
?>
<div>
     <div class="report-label-div m-t-5"><label>Koszty wg typu</label></div>
     <?php if (!empty($client->costs)) :?>
          <div class="report-summary row">
               <div class="col-sm-2">
                    <label>Rodzaj kosztu</label>
               </div>
               <div class="col-sm-2 text-center">
                    <label>Wartość</label>
               </div>
               <div class="col-sm-2">

               </div>
               <div class="col-sm-2">

               </div>
          </div>
          <?php foreach ($client->costs as $cost):?>
               <div class="report-summary row">
                    <div class="col-sm-2">
                         <label><?php echo EntitiesCostsModel::listCostsTypes()[$cost['enc_type']]?></label>
                    </div>
                    <div class="col-sm-2 text-right">
                         <?php echo $cost['cost_group_value']?> PLN
                    </div>
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-2">
                    </div>
               </div>
          <?php endforeach;?>
     <?php endif;?>
     <div class="report-summary row">
          <div class="report-label-div m-t-5"><label>Podsumowanie</label></div>
          <div class="col-sm-2">

          </div>

          <div class="col-sm-2 text-center">
               <label>Koszty netto razem</label>
               <div class="text-right"><?php echo number_format($client->costsValue, 2, ',', ' ')?> PLN</div>
          </div>
     </div>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif; 
