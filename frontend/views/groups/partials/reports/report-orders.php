<?php
use common\models\ReportModel;
use common\models\GroupsModel;
use common\helpers\TimeHelper;
use common\models\OrdersModel;
use yii\helpers\Html;

/* @var $reportModel ReportModel*/
/* @var $group GroupsModel*/
/* @var $groupOrdersData OrdersModel[]*/
$summary = !isset($summary) || (bool)$summary!== false ? true : $summary;
if (!empty($groupOrdersData)) :
?>
<div>
     <div class="report-label-div m-t-5"><label>Zlecenia</label></div>
     <?php foreach ($clientOrdersData as $order): ?>
     <div class="row">
          <div class="col-sm-12">
               <label><?php echo $order->getAttributeLabel('ord_name')?>:&nbsp;</label><?php echo Html::a($order->ord_name, ['orders/view', 'id' => $order->ord_id], ['class' => 'report-entity-name', 'target' => '_blank']) ?>
          </div>
          <div class="col-sm-12">
               <label><?php echo $order->getAttributeLabel('ord_project_fkey')?>:&nbsp;</label><?php echo !empty($order->project) ? Html::a($order->project->pro_name, ['projects/view', 'id' => $order->project->pro_id], ['class' => 'report-entity-name', 'target' => '_blank']) : ' - ' ?>
          </div>
     </div>
     <div class="row">
          <div class="col-sm-2">
               <label>Budżet</label>
               <div class="text-center"><?php echo !empty($order->ord_budget_type) ? $order::listBudgetTypes()[$order->ord_budget_type] : ' brak typu '?> (<?php echo !empty($order->ord_budget_value) ? $order->ord_budget_value : 'brak wartości'?>)</div>
          </div>
          <div class="col-sm-2">
               <label>% wykorzystania</label>
               <div class="text-center"><?php echo !empty($order->ord_budget_type) && !empty($order->pro_budget_value) ? number_format($order->budgetUse*100, 2, ',', ' ').'%' : '-'?></div>
          </div>
          <div class="col-sm-2">
               <label>Koszty</label>
               <div class="text-center"><?php echo number_format($order->costsValue, 2, ',', ' ')?> PLN</div>
          </div>
          <div class="col-sm-2">
               <label>Liczba godzin</label>
               <div class="text-center"><?php echo TimeHelper::HourMinSec($order->orderTime)?></div>
          </div>
     </div>
     <hr>
     <?php endforeach;?>
     <?php if ($summary):?>
     <div class="report-summary row">
          <div class="report-label-div m-t-5"><label>Podsumowanie</label></div>
          <div class="col-sm-2">
               
          </div>
          <div class="col-sm-2">
               
          </div>
          <div class="col-sm-2">
               <label>Koszty razem</label>
               <div class="text-center"></div>
          </div>
          <div class="col-sm-2">
               <label>Liczba godzin razem</label>
               <div class="text-center"></div>
          </div>
     </div>
     <?php endif; ?>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif; 
