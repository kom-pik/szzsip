<?php

use yii\helpers\Html;
use yii\web\View;
use common\models\GroupsModel;
use yii\bootstrap\Tabs;

/* @var $group GroupsModel */
/* @var $this View */


$this->params['breadcrumbs'][] = $this->title;
//dd($this->params['breadcrumbs']);
?>
<div>
     <?php
     echo!$this->params['user']->isAdmin() ? '' : Html::a(Yii::t('app', 'Edytuj grupę'), ['update', 'id' => $group->gro_id], [
                  'class' => 'btn btn-success',
                  'id' => 'btn-edit'
     ]);

     $items = [
          [
               'label' => 'Członkowie',
               'options' => [
                    'id' => 'members-tab',
               ],
               'content' => $this->render('partials/members-tab', [
                    'membersTab' => $membersTab
               ])
          ],
     ];

     if ($this->params['user']->isAdminOrSupervisor()) {
          $items[] = [
               'label' => 'Raporty',
               'options' => [
                    'id' => 'reports-tab'
               ],
               'content' => $this->render('partials/reports-tab', [
                    'reportsTab' => $reportsTab
               ])
          ];
     }


     echo Tabs::widget([
          'items' => $items
     ]);
     ?>
</div>