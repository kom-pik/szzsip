<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\Modal;

if (empty($this->params['user'])){
     $this->params['user'] = Yii::$app->user->identity;
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
     <head>
          <meta charset="<?= Yii::$app->charset ?>">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <?= Html::csrfMetaTags() ?>
          <title><?= Html::encode($this->title) ?></title>
          <?php $this->head() ?>
     </head>
     <body>
          <?php $this->beginBody() ?>
          <div class="wrap">
               <div class="row">
                    <div class="col-md-2">
                         <?= $this->render('sidebar', ['user' => $this->params['user']]) ?>
                    </div>

                    <div class="col-md-10">
                         <?=
                         Breadcrumbs::widget([
                             'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                         ])
                         ?>
                         <?= Alert::widget() ?>
                         <?= $content ?>

                    </div>
               </div>
          </div>

          <footer class="footer">
               <div class="container">
                    <p class="pull-left">&copy; Marcin Pikul <?= date('Y') ?></p>

                    <p class="pull-right"><?= Yii::powered() ?></p>
               </div>
          </footer>
          <?php
          Modal::begin([
              'id' => 'modal',
          ]);
          Modal::end();
          
          Modal::begin([
              'id' => 'modal-large',
              'size' => Modal::SIZE_LARGE,
          ]);
          Modal::end();
          ?>
          <div class="modal fade" id="modal-confirm-delete" role="dialog" aria-labelledby="myModalLabel" <?= Yii::$app->user->isGuest ? '' : 'data-keyboard="false" data-backdrop="static"'?>>
               <div class="modal-dialog" role="document">
                    <?php echo $this->render('@app/views/partials/modal-confirm-delete');?>
               </div>
          </div>
<?php $this->endBody() ?>
     </body>
</html>
<?php $this->endPage() ?>
