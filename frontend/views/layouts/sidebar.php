<?php

use yii\helpers\Url;
use kartik\sidenav\SideNav;
use common\models\UsersModel;
use yii\web\View;

/* @var $this View */
/* @var $user UsersModel */

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
//dd(Yii::$app->request->url);
?>

<div class="pjax-clock text-center"><div><?php echo date('H:i:s', time()) ?></div><div><?php echo date('d-m-Y', time()) ?></div></div>
<div class="nav" id="side-nav">
     <?php
     if (Yii::$app->user->isGuest) {
          $heading = '<i class="glyphicon glyphicon-tasks"></i> Menu';
          $items = [
               [
                    'label' => 'Strona główna', 'icon' => 'home', 'url' => Url::to(['sites/index']),
                    'active' => $controller == 'site' && $this->context->action && $this->context->action->id == 'index'
               ],
               [
                    'label' => 'O aplikacji', 'icon' => 'list-alt', 'url' => ['/sites/about'],
                    'active' => $controller == 'site' && $this->context->action && $this->context->action->id == 'about'
               ],
               [
                    'label' => 'Kontakt', 'icon' => 'envelope', 'url' => ['/sites/contact'],
                    'active' => $controller == 'site' && $this->context->action && $this->context->action->id == 'contact'
               ],
               [
                    'label' => 'Zaloguj się', 'icon' => 'log-in', 'url' => Url::to(['sites/login']),
                    'active' => $controller == 'site' && $this->context->action && $this->context->action->id == 'login'
               ],
          ];
     } else {
          $heading = '<i class="glyphicon glyphicon-tasks"></i> Menu (zalogowany: ' . $user->usr_username . ')';
          $items = [
               [
                    'label' => 'Strona główna', 'icon' => 'home', 'url' => Url::to(['/sites/index']),
                    'active' => $controller == 'sites' && $action == 'index'
               ],
               [
                    'label' => 'Moje konto', 'icon' => 'briefcase', 'url' => Url::to(['/clients/view', 'id' => $user->usr_client_fkey]),
                    'active' => $controller == 'clients',
                    'visible' => $user->usr_type == UsersModel::TYPE_CLIENT
               ],
               [
                    'label' => 'Projekty', 'icon' => 'briefcase', 'url' => Url::to(['/projects/index']),
                    'active' => $controller == 'projects',
                    'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
               ],
               [
                    'label' => 'Zlecenia', 'icon' => 'list', 'url' => Url::to(['/orders/index']),
                    'active' => $controller == 'orders',
                    'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
               ],
               [
                    'label' => 'Zadania', 'icon' => 'th', 'url' => Url::to(['/tasks/index']),
                    'active' => $controller == 'tasks',
                    'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
               ],
               [
                    'label' => 'Klienci', 'icon' => 'king', 'url' => Url::to(['/clients/index']),
                    'active' => $controller == 'clients',
                    'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
               ],
               [
                    'label' => 'Cenniki', 'icon' => 'euro', 'url' => Url::to(['/price-lists/index']),
                    'active' => $controller == 'price-lists',
                    'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
               ],
               [
                    'label' => 'Użytkownicy', 'icon' => 'user', 'url' => Url::to(['/users/index']),
                    'active' => $controller == 'users',
                    'visible' => $user->isAdmin()
               ],
               [
                    'label' => 'Grupy', 'icon' => 'bullhorn', 'url' => Url::to(['/groups/index']),
                    'active' => $controller == 'groups',
                    'visible' => $user->isAdmin()
               ],
               [
                    'label' => 'Moja grupa', 'icon' => 'bullhorn', 'url' => Url::to(['/groups/view']),
                    'active' => $controller == 'groups' || (!$user->isAdmin() && $controller == 'users'),
                    'visible' => $user->isSupervisor() || $user->isServiceman()
               ],
               [
                    'label' => 'Mój profil', 'icon' => 'edit', 'items' =>
                    [
                         [
                              'label' => 'Wyświetl',
                              'icon' => 'eye-open',
                              'url' => Url::to(['/profiles/view']),
                              'active' => Yii::$app->request->url == Url::to(['/profiles/view']),
                         ],
                         [
                              'label' => 'Edytuj',
                              'icon' => 'pencil',
                              'url' => Url::to(['/profiles/update']),
                              'active' => ($action == 'update' && $controller == 'profiles')
                         ],
                         [
                              'label' => 'Moje projekty',
                              'icon' => 'briefcase',
                              'url' => Url::to(['/profiles/view', 'tab' => 'projects']),
                              'active' => Yii::$app->request->url == Url::to(['/profiles/view', 'tab' => 'projects']),
                              'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
                         ],
                         [
                              'label' => 'Moje zlecenia',
                              'icon' => 'list',
                              'url' => Url::to(['/profiles/view', 'tab' => 'orders']),
                              'active' => Yii::$app->request->url == Url::to(['/profiles/view', 'tab' => 'orders']),
                              'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
                         ],
                         [
                              'label' => 'Moi klienci',
                              'icon' => 'king',
                              'url' => Url::to(['/profiles/view', 'tab' => 'clients']),
                              'active' => Yii::$app->request->url == Url::to(['/profiles/view', 'tab' => 'clients']),
                              'visible' => $user->usr_type != UsersModel::TYPE_CLIENT
                         ]
                    ],
                    'active' => $controller == 'profiles'
               ],
               [
                    'label' => 'Wyloguj się', 'icon' => 'log-out',
                    'url' => Url::to(['/sites/logout']),
                    'template' => '<a href="{url}" data-method="post">{icon}{label}</a>',
               ],
          ];

          if ($user->isAdmin()) {
               $items[] = [
                    'label' => 'Zarządzanie RBAC', 'icon' => 'wrench', 'items' => [
                         ['label' => 'Przypisz uprawnienia', 'url' => '/admin', 'active' => ($controller == 'assignment')],
                         ['label' => 'Role', 'url' => '/admin/role', 'active' => ($controller == 'role')],
                         ['label' => 'Pozwolenia', 'url' => '/admin/permission', 'active' => ($controller == 'permission')],
                         ['label' => 'Ścieżki', 'url' => '/admin/route', 'active' => ($controller == 'route')],
                         ['label' => 'Reguły', 'url' => '/admin/rule', 'active' => ($controller == 'rule')],
                         ['label' => 'Pomoc', 'url' => '/admin/default', 'active' => ($controller == 'default')],
                    ]];
               $items[] = [
                    'label' => 'Support', 'icon' => 'cog', 'items' => [
                         ['label' => 'Migracje', 'url' => '/support/migrations', 'active' => ($controller == 'support' && $action == 'migrations')],
                    ]
               ];
          }
     }
     echo SideNav::widget([
          'encodeLabels' => false,
          'heading' => $heading,
          'items' => $items
     ]);
     ?>
</div>

