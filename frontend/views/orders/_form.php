<?php
use frontend\models\OrderForm;
use common\models\ProjectsModel;
use common\models\ClientsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $orderForm OrderForm */
/* @var $project ProjectsModel */


?>
    <?php $form = ActiveForm::begin([
        'options' => ['id' => 'order-form'],
    ]); ?>
<div class="row">
    <div class="col-md-8">
        <?= $form->field($orderForm, 'name')->textInput() ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($orderForm, 'status')->dropDownList($orderStatus, ['prompt'=>'Wybierz status (domyślnie nowy)']) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($orderForm, 'owner_id')
            ->dropDownList($userList, 
                    ['prompt'=>'Wybierz właściciela zlecenia (domyślnie: '.Yii::$app->user->identity->username.')']) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($orderForm, 'executive_id')
            ->dropDownList($userList, 
                    ['prompt'=>'Wybierz wykonawcę zlecenia (domyślnie: '.Yii::$app->user->identity->username.')']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($orderForm, 'client_id')
            ->dropDownList($clientsList, [
                'options' => [
                        $project->client_id => ['Selected' => true]
                    ],
                'prompt'=>'Wybierz klienta',
                !$project->id ? '' : 'disabled'=> true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($orderForm, 'project_id')->
            dropDownList($projects, [
                'options' => [
                    $project->id => ['Selected' => true]
                    ],
                'prompt'=>'Wybierz projekt',
                !$project->id ? '' : 'disabled'=> true]) ?>
    </div>
</div>    

    <?= $form->field($orderForm, 'description')->textArea(['class' => 'tinymce']) ?>

        
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Zapisz'), [
            'class' => 'btn btn-primary col-sm-12'
        ]) ?>
    </div>

        
    <?php $form->end(); ?>

