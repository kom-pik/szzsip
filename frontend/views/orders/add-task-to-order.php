<?php

use common\models\OrdersModel;
use common\models\TasksModel;
use frontend\models\Task2OrderForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\select2\Select2;

/* @var $order OrdersModel */
/* @var $task TasksModel */
/* @var $task2order Task2OrderForm */
/* @var $this View */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog">
     <div class="modal-content"> 
          <div class="modal-header">
               <h3 class="pull-left">Dodaj zadania do zlecenia <?php echo!empty($order->ord_name) ? $order->ord_name : '' ?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <?php
          $form = ActiveForm::begin([
                      'id' => 'task-2-order-form',
                      'options' => [
                          'data-pjax' => true,
                      ]
          ]);
          if (!empty($taskIds)){
               echo Html::hiddenInput('Task2OrderForm[task_ids]', implode(',', $taskIds));
          } else {
               echo Html::hiddenInput('Task2OrderForm['.(!empty($order->ord_id) ? 'order_id' : 'task_ids').']', !empty($order->ord_id) ? $order->ord_id : $task->tas_id);
          }
          ?>
          <div class="modal-body">
               <label class=""><?= !empty($order->ord_id) ? $order->getAttributeLabel('ord_name') : $task->getAttributeLabel('tas_name') ?></label>
               <?php if (!empty($tasks)): ?>
               <div>
                    <?php 
                              foreach ($tasks as $task){
                                   echo $task->tas_name . ', ';
                              }
                    ?>
               </div>
               <?php else: ?>
               <div><?= !empty($order->ord_name) ? $order->ord_name : $task->tas_name ?></div>
               <?php endif; ?>
               <?=
               $form->field($task2order, !empty($order->ord_id) ? 'task_ids' : 'order_id')->widget(Select2::className(), [
                   'data' => !empty($order->ord_id) ? $tasksList : $ordersList,
                   'options' => [
                       'placeholder' => !empty($order->ord_id) ? 'Wybierz zadania' : 'Wybierz zlecenie',
                       'multiple' => true,
                       'initValueText' => ''
                   ],
                   'pluginOptions' => [
                       'allowClear' => true,
                   ]
               ])
               ?>
          </div> 
          <div class="modal-footer">
               <?=
               Html::button(Yii::t('app', 'Zapisz'), [
                   'class' => 'btn btn-primary col-sm-12 submit-form'
               ])
               ?>
          </div>
<?php ActiveForm::end(); ?>
     </div>
</div>
