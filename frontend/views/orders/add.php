<?php

use common\models\OrdersModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use common\models\ProjectsModel;
use common\model\UsersModel;

/* @var $order OrdersModel */
/* @var $project ProjectsModel */
/* @var $this View */
/* @var $user UsersModel */

$this->title = 'Dodaj zlecenie';
$this->params['breadcrumbs'][] = $this->title;
$user = $this->params['user'];
?>
<div class="modal-dialog modal-lg">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left"><i class="glyphicon glyphicon-plus"></i> Dodaj zlecenie<?= empty($project->pro_id) ? '' : ' do projektu' ?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <?php
          $form = ActiveForm::begin([
                       'options' => [
                            'id' => 'order-form',
                       ],
                  ]);
          if (!empty($order->ord_project_fkey) && !empty($order->ord_group_fkey)){
               echo Html::hiddenInput('OrdersModel[ord_project_fkey]', $order->ord_project_fkey);
               echo Html::hiddenInput('OrdersModel[ord_group_fkey]', $order->ord_group_fkey);               
          }
          ?>
          <div class="modal-body">

               <div class="row">
                    <div class="col-md-8">

                         <?= $form->field($order, 'ord_name')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                         $form->field($order, 'ord_status')->dropDownList($orderStatus, [
                              'prompt' => $user->isClient() ? null : 'Wybierz status (domyślnie nowy)',
                         ])
                         ?>
                    </div>
               </div>

               <div class="row">
                    <div class="col-md-6">
                         <?=
                                 $form->field($order, 'ord_owner_fkey')
                                 ->dropDownList($userList, [
                                      'prompt' => $user->isClient() ? null : 'Wybierz właściciela zlecenia (domyślnie: ' . $user->usr_username . ')',
                                      'value' => $user->isClient() ? array_keys($userList)[0] : '',
                         ]);
                         ?>
                    </div>
                    <div class="col-md-6">
                         <?=
                                 $form->field($order, 'ord_executive_fkey')
                                 ->dropDownList($userList, [
                                      'prompt' => $user->isClient() ? null : 'Wybierz wykonawcę zlecenia (domyślnie: ' . $user->usr_username . ')'
                                 ])
                         ?>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-4">
                         <?=
                                 $form->field($order, 'ord_client_fkey')
                                 ->dropDownList($clientsList, [
                                      'options' => [
                                           $project->pro_client_fkey => ['Selected' => true]
                                      ],
                                       $user->isClient() ? '' : 'prompt' =>'Wybierz klienta'])
                         ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                                 $form->field($order, 'ord_project_fkey')->
                                 dropDownList($projects, [
                                      'options' => [
                                           $project->pro_id => ['Selected' => true],
                                           'value' => $project->pro_id
                                      ],
                                      'prompt' => 'Wybierz projekt',
                                      !$project->pro_id ? '' : 'disabled' => true])
                         ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                                 $this->params['user']->isAdmin() ? $form->field($order, 'ord_group_fkey')
                                 ->dropDownList($groupsList, [
                                     'options' => [
                                         $project->pro_group_fkey => ['Selected' => true]
                                     ],
                                     'prompt' => 'Wybierz grupę',
                                     !$project->pro_id ? '' : 'disabled' => TRUE
                                 ]) : ''
                         ?>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-4">
                         <?=
                                 $form->field($order, 'ord_type')
                                 ->dropDownList(OrdersModel::listTypes(), [
                                      'options' => [
                                      ],
                                      'prompt' => 'Wybierz typ zlecenia'])
                         ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                                 $form->field($order, 'ord_budget_type')
                                 ->dropDownList(OrdersModel::listBudgetTypes(), [
                                      'options' => [
                                      ],
                                      'prompt' => 'Wybierz rodzaj budżetu'])
                         ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                                 $form->field($order, 'ord_budget_value')
                                 ->input('number', [
                                           'min' => '0',
                                           'step' => '0.01'
                                      ])
                         ?>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-12">
                         <?= $form->field($order, 'ord_description')->textArea(['class' => 'tinymce']) ?>
                    </div>
               </div>


          </div>
          <div class="modal-footer">

<?=
Html::submitButton(Yii::t('app', 'Zapisz'), [
     'class' => 'btn btn-primary col-sm-12'
])
?>
<?php $form->end(); ?> 

          </div>
     </div>
<script>
     tinymce.init(tinymceDefaultOptions);
</script>


