<?php
use common\models\OrdersModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\TasksModel;
use common\models\OrdersTasksModel;
use common\models\TasksSearchModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\models\UsersModel;
use common\helpers\TimeHelper;

/* @var $order OrdersModel */
/* @var $this View*/
/* @var $dataProvider ActiveDataProvider*/
/* @var $user UsersModel */

$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (!empty($modal)) : ?>
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h3 class="pull-left">Zlecenie <?= $order->ord_name?></h3>
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
<?php endif; ?>
<div class="col-sm-12">
     <?php if (!$user->isClient()) :?>
    <div class="row m-t-5">
        <?=    Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Dodaj zadania do zlecenia'), 
                            Url::to(['add-task-to-order', 'order_id' => $order->ord_id]), ['class' => 'btn btn-success add-object']); ?>
        <?=    Html::a(Yii::t('app', '<i class="glyphicon glyphicon-pencil"></i> Edytuj zlecenie'), ['update', 'id' => $order->ord_id], ['class' => 'btn btn-success'])?>
       <hr> 
    </div>
     <?php endif;?>
    <div class="row">
        <div class="col-sm-12">
            <label class=""><?= $order->getAttributeLabel('ord_name') ?></label>
            <div><?= $order->ord_name ?></div>
            <hr>
        </div>
        
        <div class="col-sm-12">
            <label><?= $order->getAttributeLabel('ord_description') ?></label>
            <div><?= $order->ord_description ?></div>
            <hr>
        </div>
    </div>
    

    <div class="row my-col-sm-2">
        <div class="col-sm-2 col-xs-12">
            <label><?= $order->getAttributeLabel('ord_client_fkey') ?></label>
            <div><?= $order->client ? 
                Html::a($order->client->cli_acronym, 
                        Url::to(['/clients/view', 'id' => $order->client->cli_id])) : 
                Html::a(Yii::t('app', 'dodaj klienta'), 
                        Url::to(['add-property', 'id' => $order->ord_id, 'property' => 'client']), ['class' => 'add-property']) ?></div><hr>
        </div>
         <?php if (!$user->isClient()):?>
        <div class="col-sm-2 col-xs-12">
            <label><?= $order->getAttributeLabel('ord_project_fkey') ?></label>
            <div><?= $order->project ? Html::a($order->project->getShortName(), Url::to(['/projects/view', 'id' => $order->project->pro_id])) :
                        Html::a(Yii::t('app', 'dodaj projekt'), 
                                Url::to(['add-property', 'id' => $order->ord_id, 'property' => 'project']), ['class' => 'add-property'])    ?>
            </div><hr>
        </div>
        <div class="col-sm-2 col-xs-12">
            <label><?= $order->getAttributeLabel('ord_owner_fkey') ?></label>
            <div><?= !empty($order->owner) ? Html::a($order->owner->usr_username, Url::to(['/users/view', 'id' => $order->ord_owner_fkey])) : 'brak'?></div><hr>
        </div>
         <?php endif;?>
        <div class="col-sm-2 col-xs-12">
            <label><?= $order->getAttributeLabel('ord_created_at') ?></label>
            <div><?= date('d-m-Y H:i', $order->ord_created_at) ?></div><hr>
        </div>

        <div class="col-sm-2 col-xs-12">
            <label><?= $order->getAttributeLabel('status') ?></label>
            <div><?= OrdersModel::listStatuses()[$order->ord_status] ?>
                    <?= $user->isClient() ? '' : Html::a(Yii::t('app', ' <i class="glyphicon glyphicon-refresh"></i>'),
                            Url::to(['add-property', 'id' => $order->ord_id, 'property' => 'status']),
                            [
                                'class' => 'add-property m-l-5',
                                'title' => 'Zmień status'
                                ]) ?>
            </div>
            <hr>
        </div>
    </div>
     <div class="row my-col-sm-2">
          <div class="col-sm-2">
               <label><?= $order->getAttributeLabel('time') ?></label>
               <div><?= TimeHelper::HourMinSec($order->getOrderTime())?></div>
               <hr>
          </div>
          
     </div>
    
    <div class="row">
        <?php 
          if (empty($modal) && !$user->isClient()){
               $params = [
                    'order' => $order,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'userList' => $userList,
                    'tasksNames' => $tasksNames,
                    'user' => $this->params['user'],
                    'modal' => !empty($modal) ? $modal : null
               ];
               echo $this->render('/orders/partials/view/order-grid', $params);
          }
        ?>        
     </div>
</div>
<?php if (!empty($modal)) : ?>
                    <div class="modal-footer"></div>
               </div>
          </div>
     </div>
<?php endif; ?>

          <div class="modal fade" id="modal-add-worktime">
                    <?php      
                             echo $this->renderFile('@app/views/tasks/add-worktime.php', ['addWorkTimeModel' => $addWorkTimeModel]);
                    ;?>
          </div>