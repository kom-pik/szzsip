<?php

use common\models\OrdersModel;
use common\models\ClientsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $order OrdersModel */
/* @var $this View */
/* @var $property string */
$form = ActiveForm::begin([
             'options' => ['id' => 'add-property-form'],
        ]);

switch ($property) {
     case 'client':
          $header = 'Dodaj klienta';
          $field = $form->field($order, 'ord_client_fkey')->dropDownList($clientsList, ['prompt' => 'Wybierz klienta']);
          break;
     case 'project':
          $header = 'Dodaj projekt';
          $field = $form->field($order, 'ord_project_fkey')->dropDownList($projectsList, ['prompt' => 'Wybierz projekt']);
          break;
     case 'status':
          $header = 'Zmień status';
          $field = $form->field($order, 'ord_status')->dropDownList($order->listStatuses());
          break;
}
?>
<div class="modal-dialog">
     <div class="modal-content"> 

          <div class="modal-header">
               <h3 class="pull-left"><?= $header ?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">

               <?= $field ?>

          </div> 
          <div class="modal-footer">
               <?=
               Html::submitButton(Yii::t('app', 'Zapisz'), [
                    'class' => 'btn btn-primary col-sm-12'
               ])
               ?>
          </div>


<?php $form->end(); ?>
     </div>
</div>