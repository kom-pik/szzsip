<?php

use common\models\OrdersTasksModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\widgets\Alert;

/* @var $orderTask OrdersTasksModel*/
/* @var $this View */
/* @var $property string*/

$disabled = $orderTask->ort_locked && $orderTask->ort_updated_by == $this->params['user']->usr_id ? null : 'disabled';

?>
<div id="orderDescRow">
    <?= Alert::widget() ?>
        <?php $form = ActiveForm::begin([
            'options' => ['id' => 'order-form'],
            'action' => Url::to(['order-task-desc', 'id' => $orderTask->ort_id]),
        ]); ?>
        <div class="modal-header">
            Dodaj opis
        </div>
        <div class="modal-body">

                <?= $form->field($orderTask, 'ort_description')->textarea([$disabled , 'class' => 'tinymce']) ?>

        </div> 
        <div class="modal-footer">
            <?= Html::submitButton(Yii::t('app', 'Zapisz'), [
                'class' => 'btn btn-primary col-sm-12',
                'action' => Url::to(['order-task-desc', 'id' => $orderTask->ort_id]),
                'id' => 'modal-submit-button'
            ]) ?>
        </div>


        <?php $form->end(); ?>
</div>

<script>
     //MY_TODO tinymce after ajax load
     $(document).ready(function(){
          setTimeout(function() {tinymce.init(tinymceDefaultOptions);}, 500);
     })
     
</script>