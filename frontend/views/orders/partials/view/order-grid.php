<?php

use common\models\OrdersModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\OrdersTasksModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\models\UsersModel;
use common\helpers\TimeHelper;

/* @var $order OrdersModel */
/* @var $this View */
/* @var $dataProvider ActiveDataProvider */
/* @var $user UsersModel */

echo GridView::widget([
     'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-th"></i> Zadania',
     ],
     'toolbar' => [
          [
               'content' => !$user->isClient() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone zadania'), ['remove-task'], ['class' => 'btn btn-danger grid-mass-delete disabled', 'data-pjax' => 0]) : ''
          ],
          [
               'content' => Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Dodaj zadania do zlecenia'), Url::to(['add-task-to-order', 'order_id' => $order->ord_id, 'task_id' => 0]), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
          ],
          [
               'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $order->ord_id], ['class' => 'btn btn-default grid-reset', 'title' => Yii::t('app', 'Resetuj widok')])
          ],
          '{export}',
          '{toggleData}'
     ],
     'pjax' => true,
     'pjaxSettings' => [
          'neverTimeout' => true,
          'enablePushState' => false,
     ],
     'dataProvider' => $dataProvider,
     'filterModel' => $searchModel,
     'hover' => true,
     'columns' => [
          [
               'class' => '\kartik\grid\ExpandRowColumn',
               'value' => function () {
                    return GridView::ROW_COLLAPSED;
               },
//                    'enableRowClick' => 'true',
               'detailUrl' => Url::to(['order-task-desc']),
          ],
          [
               'class' => '\kartik\grid\CheckboxColumn',
               'name' => 'order-tasks[]',
               'checkboxOptions' => function($model) {
                    return ['value' => $model->ort_id];
               }
          ],
          [
               'class' => '\kartik\grid\SerialColumn'
          ],
          [
               'class' => '\kartik\grid\DataColumn',
               'filterType' => GridView::FILTER_SELECT2,
               'filterWidgetOptions' => [
                    'data' => $tasksNames,
                    'options' => [
                         'placeholder' => 'filtruj po nazwie ...',
                         'initValueText' => ''
                    ],
                    'pluginOptions' => [
                         'allowClear' => true,
//                            'minimumInputLength' => 3,
                    ]
               ],
               'attribute' => 'task.name',
               'format' => 'raw',
               'value' => function($model) {
                    return Html::a($model->task->shortName, Url::to(['/tasks/view', 'id' => $model->task->tas_id]), ['data-pjax' => 0, 'title' => $model->task->tas_name]);
               }
          ],
          [
               'class' => '\kartik\grid\DataColumn',
               'filterType' => GridView::FILTER_SELECT2,
               'filterWidgetOptions' => [
                    'data' => $userList,
                    'options' => [
                         'placeholder' => 'filtruj po właścicielu ...',
                         'initValueText' => ''
                    ],
                    'pluginOptions' => [
                         'allowClear' => true
                    ]
               ],
               'attribute' => 'creator',
               'format' => 'raw',
               'value' => function($model) {
                    return Html::a($model->creator->usr_username, Url::to(['/users/view', 'id' => $model->creator->usr_id]), ['data-pjax' => 0]);
               },
          ],
          [
               'class' => '\kartik\grid\DataColumn',
               'filterType' => GridView::FILTER_SELECT2,
               'filterWidgetOptions' => [
                    'data' => OrdersTasksModel::listStatuses(),
                    'options' => [
                         'placeholder' => 'filtruj po statusie ...',
                         'initValueText' => ''
                    ],
                    'pluginOptions' => [
                         'allowClear' => true
                    ]
               ],
               'attribute' => 'status',
               'value' => function($model) {
                    return OrdersTasksModel::listStatuses()[$model->ort_status];
               },
          ],
          [
               'class' => '\kartik\grid\DataColumn',
               'filterType' => GridView::FILTER_DATE_RANGE,
               'attribute' => 'created',
               'filterWidgetOptions' => [
                    'presetDropdown' => true,
                    'pluginOptions' => [
                         'locale' => [
                              'format' => 'YYYY-MM-DD',
                         ]
                    ],
                    'pluginEvents' => [
                         "apply.daterangepicker" => "function() { apply_filter('date') }",
                    ]
               ],
               'value' => function ($model) {
                    return date('d-m-Y H:i', $model->task->tas_created_at);
               },
          ],
          [
               'class' => '\kartik\grid\DataColumn',
               'attribute' => 'time',
               'value' => function ($model) {
                    return TimeHelper::HourMinSec($model->time);
               },
          ],
          [
               'class' => '\kartik\grid\ActionColumn',
               'header' => 'Akcje',
               'template' => OrdersTasksModel::template(),
               'buttons' => [
//                    'add-worktime' => function ($url, $model) {
//                         return Html::a('<span class="glyphicon glyphicon-time"></span>', $url, [
//                                      'title' => Yii::t('app', 'Dodaj czas pracy'),
//                                      'class' => 'm-l-5',
//                                      'data-pjax' => 0,
//                                      'data-toggle' => "modal" ,
//                                      'data-target'=>"#modal-add-worktime",
//                         ]);
//                    },
                    'work' => function ($url, $model) {
                         return OrdersTasksModel::workButton($url, $model);
                    },
                    'view' => function ($url, $model) {
                         return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                      'title' => Yii::t('app', 'Pokaż szczegóły'),
                                      'class' => 'm-l-5',
                                      'data-pjax' => 0
                         ]);
                    },
                    'update' => function ($url, $model) {
                         return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                      'title' => Yii::t('app', 'Edytuj'),
                                      'class' => 'm-l-5',
                                      'data-pjax' => 0
                         ]);
                    },
                    'delete' => function ($url, $model) {
                         return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                      'title' => Yii::t('app', 'Usuń'),
                                      'class' => 'm-l-5' . ($model->ort_locked ? ' disabled' : ''),
                                      'data-pjax' => 0,
                         ]);
                    },

               ],
               'urlCreator' => function ($action, $model) {
                    $url = '';
                    switch ($action) {
                         case 'work':
                              if ($model->ort_locked == OrdersTasksModel::LOCKED) {
                                   if ($model->ort_updated_by == $this->params['user']->usr_id) {
                                        $url = Url::to(['tasks/pause-work', 'id' => $model->ort_id]);
                                   } else {
                                        $url = Url::to(['view', 'id' => $model->ort_order_fkey]);
                                   }
                              } else {
                                   $url = Url::to(['tasks/start-work', 'id' => $model->ort_id, 'type' => $model->ort_locked == OrdersTasksModel::PAUSED ? 'resume' :  'start']);
                              }
                              break;
                         case 'view':
                              $url = Url::to(['tasks/view', 'id' => $model->task->tas_id]);
                              break;                         
                         case 'update':
                              $url = Url::to(['tasks/update', 'id' => $model->task->tas_id]);
                              break;                         
                         case 'delete':
                              $url = Url::to(['tasks/remove-task', 'id' => $model->task->tas_id]);
                              break;
                         case 'add-worktime':
                              $url = Url::to(['#modal-add-worktime', 'id' => $model->task->tas_id]);
                              break;
                    }
                    return $url;
               }
          ],
     ],
     'showPageSummary' => true,
])
?>
