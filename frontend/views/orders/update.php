<?php
use frontend\models\order;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $order Order */
/* @var $this View*/

$this->title = 'Edytuj zlecenie';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?php $form = ActiveForm::begin([
        'options' => ['id' => 'order-form'],
    ]); ?>
    
    <?= $form->field($order, 'ord_name')->textInput() ?>
    <?= $form->field($order, 'ord_owner_fkey')->dropDownList($userList, ['prompt'=>'Wybierz właściciela zlecenia (domyślnie: '.Yii::$app->user->identity->usr_username.')']) ?>
    <?= $form->field($order, 'ord_executive_fkey')->dropDownList($userList, ['prompt'=>'Wybierz wykonawcę zlecenia (domyślnie: '.Yii::$app->user->identity->usr_username.')']) ?>
    <?= $form->field($order, 'ord_client_fkey')->dropDownList($clientsList, ['prompt'=>'Wybierz klienta']) ?>
    <?= $form->field($order, 'ord_project_fkey')->dropDownList($projects, ['prompt'=>'Wybierz projekt']) ?>
    <?= $form->field($order, 'ord_status')->dropDownList($orderStatus, ['prompt'=>'Wybierz status (domyślnie nowy)']) ?>
    <?= $form->field($order, 'ord_description')->textArea(['class' => 'tinymce']) ?>

        
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Zapisz'), [
            'class' => 'btn btn-primary col-sm-12'
        ]) ?>
    </div>

        
    <?php $form->end(); ?>


<script>
     tinymce.init(tinymceDefaultOptions);
</script>