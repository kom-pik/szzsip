<?php

use yii\web\View;
use common\models\OrdersModel;
use yii\bootstrap\Tabs;

/* @var $order OrdersModel */
/* @var $this View */

$user = $this->params['user'];
?>

<?php if (!empty($modal)) : ?>
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h3 class="pull-left">Zlecenie <?= $order->ord_name ?></h3>
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
<?php endif; ?>
               <?php
               $items = [
                    [
                    'label' => 'Informacje ogólne',
                    'options' => [
                         'id' => 'info-tab'
                    ],
                    'active' => 'true',
                    'content' => $this->render('partials/info-tab', [
                         'order' => $order,
                         'searchModel' => $searchModel,
                         'dataProvider' => $dataProvider,
                         'userList' => $userList,
                         'tasksNames' => $tasksNames,
                         'user' => $user,
                         'addWorkTimeModel' => $addWorkTimeModel
                            ])
                    ],
                    [
                         'label' => 'Koszty',
                         'options' => [
                              'id' => 'costs-tab'
                         ],
                         'content' => $this->render('/partials/costs-tab', [
                              'user' => $user,
                              'entity' => $order,
                              'costsTabParams' => $costsTabParams
                         ])
                    ],
               ];

               echo Tabs::widget([
                    'items' => $items
               ]);
               ?>
<?php if (!empty($modal)) : ?>
                    <div class="modal-footer"></div>
               </div>
          </div>
     </div>
<?php endif; ?>
