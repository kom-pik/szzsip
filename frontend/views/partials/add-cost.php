<?php

use common\models\EntitiesCostsModel;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this View */
/* @var $entityCostModel EntitiesCostsModel */
?>
<div class="modal-dialog">
     <div class="modal-content">
          <?php
          $form = ActiveForm::begin([
                       'options' => [
                            'class' => 'ajax-form'
                       ]
          ]);
          echo Html::hiddenInput('EntitiesCostsModel[enc_entity_fkey]', $entityCostModel->enc_entity_fkey);
          echo Html::hiddenInput('EntitiesCostsModel[enc_entity_type]', $entityCostModel->enc_entity_type);
          ?>
          <div class="modal-header">
               <h3 class="pull-left"><?php echo $entityCostModel->isNewRecord ? 'Dodaj koszt' : 'Edytuj koszt'?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
               <?= $form->field($entityCostModel, 'enc_type')->dropDownList($entityCostModel::listCostsTypes()) ?>
               <?= $form->field($entityCostModel, 'enc_net_value')->input('number', ['step' => '0.01', 'min' => '0.01']) ?>
               <?= $form->field($entityCostModel, 'enc_description')->textArea(['class' => 'tinymce']) ?>
               <?= $form->field($entityCostModel, 'enc_own_number')->textInput() ?>

          </div>
          <div class="modal-footer">
               <?=
               Html::submitButton(Yii::t('app', 'Zapisz'), [
                    'class' => 'btn btn-primary col-sm-12',
//                'id' => 'modal-submit-button',
               ])
               ?>
          </div>
          <?php $form->end(); ?>
     </div>
</div>
