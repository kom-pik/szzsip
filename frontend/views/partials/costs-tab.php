<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\UsersModel;
use common\models\EntitiesCostsModel;
use common\models\MyModel;
use common\models\ProjectsModel;
use common\models\OrdersModel;

/* @var $this View */
/* @var $user UsersModel */
/* @var $entity MyModel*/
//dd($entity);

$columns = [
     [
          'class' => '\kartik\grid\CheckboxColumn'
     ],
     [
          'class' => '\kartik\grid\SerialColumn'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterInputOptions' => [
               'placeholder' => 'szukaj po numerze własnym',
               'class' => 'form-control'
          ],
          'attribute' => 'number',
          'format' => 'raw',
          'value' => function($model){
               return !empty($model->enc_own_number) ? $model->enc_own_number : '-';
          },
          'pageSummary' => function(){
               return 'Koszty razem';
          },
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filter' => false,
          'attribute' => 'entityName',
          'format' => 'raw',
          'value' => function($model) {
               return !empty($model->entity) ? $model->entity->attributes[$model->entity->nameAttribute] : '';
          },
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterInputOptions' => [
               'placeholder' => 'szukaj po wartości',
               'class' => 'form-control'
          ],
          'attribute' => 'value',
          'format' => 'raw',
          'pageSummary' => function($summary){
               return is_numeric($summary) ? number_format($summary, 2, '.', ' ') : '';
          },
          'pageSummaryFunc' => GridView::F_SUM,
          'value' => function($model) {
               return is_numeric($model->enc_net_value) ? number_format($model->enc_net_value, 2, '.', '') : '';
          },
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterInputOptions' => [
               'placeholder' => 'szukaj po twórcy',
               'class' => 'form-control'
          ],
          'attribute' => 'creator',
          'format' => 'raw',
          'value' => function ($model){
               return $model->creator->getName();
          },
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filter' => EntitiesCostsModel::listCostsTypes(),
          'filterInputOptions' => [
               'prompt' => 'szukaj po typie',
               'class' => 'form-control'
          ],
          'attribute' => 'costType',
          'format' => 'raw',
          'value' => function($model){
               return EntitiesCostsModel::listCostsTypes()[$model->enc_type];
          },
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterInputOptions' => [
               'placeholder' => 'szukaj po opisie',
               'class' => 'form-control'
          ],
          'attribute' => 'description',
          'format' => 'raw',
          'value' => function($model){
               return $model->enc_description;
          },
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterInputOptions' => [
               'placeholder' => 'szukaj po dacie dodania',
               'class' => 'form-control'
          ],
          'attribute' => 'enc_created_at',
          'format' => ['date', 'php:d-m-yy H:i'],
          'hAlign' => GridView::ALIGN_RIGHT
     ],
     [
          'class' => '\kartik\grid\ActionColumn',
          'template' => '{update} {delete}',
          'deleteOptions' => [
               'message' => 'Potwierdź usunięcie kosztu'
          ],
          'buttons' => [
               'update' => function ($url){
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['data-pjax' => 0, 'class' => 'modal-preview m-r-5', 'title' => 'Edytuj']);
               },
               'delete' => function ($url){
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-pjax' => 0, 'class' => 'm-r-5', 'title' => 'Usuń']);
               }
          ],
          'urlCreator' => function ($action, $model){
               switch ($action){
                    case 'update':
                         $url = Url::to(['edit-cost', 'id' => $model->enc_id]);
                         break;
                    case 'delete':
                         $url = Url::to(['delete-cost', 'id' => $model->enc_id]);
                         break;
               }
               return $url;
          },

     ]
];

echo GridView::widget([
     'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-usd"></i> Koszty '.($entity::className() == ProjectsModel::className() ? 'projektu' : ($entity::className() == OrdersModel::className() ? 'zlecenia' : 'zadania')),
     ],
     'toolbar' => [
          [
               'content' => Html::a('<i class="glyphicon glyphicon-usd"></i> Dodaj koszt', Url::to(['add-cost', 'entityId' => $entity->primaryKey]), ['class' => 'btn btn-success add-object']),
          ],
          [
               'content' => $user->isAdminOrSupervisor() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone koszty'), Url::to(['add']), ['class' => 'btn btn-danger']) : ''
          ],
          [
               'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $entity->primaryKey], [
                    'class' => 'btn btn-default grid-reset',
                    'title' => Yii::t('app', 'Resetuj widok')
               ])
          ],
          '{export}',
          '{toggleData}'
     ],
     'pjax' => true,
     'pjaxSettings' => [
          'neverTimeout' => true,
          'enablePushState' => false,
     ],
     'dataProvider' => $costsTabParams['entitiesCostsDataProvider'],
     'filterModel' => $costsTabParams['costSearchModel'],
     'columns' => $columns,
     'showPageSummary' => true,
]);
