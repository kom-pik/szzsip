
<div class="modal-dialog">
     <div class="modal-content">
          <div class="modal-header">
               <div class="bootstrap-dialog-title">Potwierdzenie</div>
          </div>
          <div class="modal-body">
               <div class="bootstrap-dialog-message">Czy na pewno usunąć zaznaczone wpisy?</div>
          </div>
          <div class="modal-footer">
               <button class="btn btn-default" id="cancel-delete" data-dismiss="modal" aria-label="Close"><span class="glyphicon glyphicon-ban-circle"></span> Anuluj</button>
               <button class="btn btn-warning" id="confirm-delete"><span class="glyphicon glyphicon-ok"></span> Ok</button>
          </div>
     </div>
</div>