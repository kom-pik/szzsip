
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
             <h3 class="pull-left"><?= !empty($header) ? $header : ''?></h3>
            <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <?= !empty($field) ? $field : ''?>

        </div>
        <div class="modal-footer">
            <?=
            Html::submitButton(Yii::t('app', 'Zapisz'), [
                'class' => 'btn btn-primary col-sm-12',
                
            ])
            ?>
        </div>


<?php $form->end(); ?>
    </div>
</div>