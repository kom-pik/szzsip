<?php
use common\models\PriceListsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $priceList PriceListsModel */
/* @var $this View*/

$this->params['breadcrumbs'][] = $this->title;
?>

    <?php $form = ActiveForm::begin([
         'id' => 'priceList-form',
         'action' => Url::to($priceList->isNewRecord ? ['add'] : ['update', 'id' => $priceList->prl_id])
    ]); ?>
    
    <?= $form->field($priceList, 'prl_name')->textInput() ?>
    <?= $form->field($priceList, 'prl_type')->dropDownList(PriceListsModel::listTypes(), ['prompt'=>'Wybierz rodzaj cennika (domyślnie standardowy)']) ?>
    <?= $form->field($priceList, 'prl_h_in_packet')->input('number', ['value' => $priceList->isNewRecord ? 0 : $priceList->prl_h_in_packet,'min' => 0, 'max' => 168]) ?>
    <?= $form->field($priceList, 'prl_net_price')->input('number', ['value' => $priceList->isNewRecord ? 0 : $priceList->prl_net_price,'min' => 0]) ?>
    <?= $form->field($priceList, 'prl_net_hourly_rate')->input('number', ['value' => $priceList->prl_net_hourly_rate ? $priceList->prl_net_hourly_rate : 50,'min' => 15, 'step' => 5]) ?>
    <?= $form->field($priceList, 'prl_sla')->input('number', ['value' => $priceList->prl_sla ? $priceList->prl_sla : 0,'min' => 0, 'step' => 0.5]) ?>   
    <?= $form->field($priceList, 'prl_status')->dropDownList(PriceListsModel::listStatuses(), ['prompt'=>'Wybierz status (domyślnie aktywny)']) ?>
    <?= $form->field($priceList, 'prl_description')->textArea(['class' => 'tinymce']) ?>

        
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Zapisz'), [
            'class' => 'btn btn-primary col-sm-12'
        ]) ?>
    </div>

        
    <?php ActiveForm::end(); ?>

