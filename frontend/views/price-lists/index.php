<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\PriceListsModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;

/* @var $this View */
/* @var $projects Project[] */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Cenniki';
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->isGuest ?: Yii::$app->user->identity;
?>

<div class="site-index">

     <?=
     GridView::widget([
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-euro"></i> Cenniki',
          ],
          'showPageSummary' => false,
          'toolbar' => [
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj cennik', Url::to(['add']), ['class' => 'btn btn-success', 'data-pjax' => 0])
               ],
               [
                    'content' => !$user->isClient() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone cenniki'), Url::to(['delete']), ['class' => 'btn btn-danger grid-mass-delete disabled', 'data-pjax' => 0]) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['index'], [
                         'class' => 'btn btn-default reset-grid',
                         'title' => Yii::t('app', 'Resetuj widok')
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
               [
                    'class' => '\kartik\grid\CheckboxColumn'
               ],
               [
                    'class' => '\kartik\grid\SerialColumn'
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $priceLists,
                         'options' => [
                              'placeholder' => 'filtruj po nazwie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true,
                              'minimumInputLength' => 3,
                         ]
                    ],
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->getShortName(), Url::to(['view', 'id' => $model->prl_id]), ['data-pjax' => 0, 'title' => $model->prl_name]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $userList,
                         'options' => [
                              'placeholder' => 'filtruj po właścicielu ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'creator',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->creator->usr_username, Url::to(['/users/view', 'id' => $model->creator->usr_id]), ['data-pjax' => 0]);
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => PriceListsModel::listStatuses(),
                         'options' => [
                              'placeholder' => 'filtruj po statusie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'status',
                    'value' => function($model) {
                         return PriceListsModel::listStatuses()[$model->prl_status];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'attribute' => 'created',
                    'filterWidgetOptions' => [
                         'presetDropdown' => true,
                         'pluginOptions' => [
                              'locale' => [
                                   'format' => 'YYYY-MM-DD',
                              ]
                         ],
                         'pluginEvents' => [
                              "apply.daterangepicker" => "function() { apply_filter('date') }",
                         ]
                    ],
                    'value' => function ($model) {
                         return date('d-m-Y H:i', $model->prl_created_at);
                    },
               ],
               [
                    'class' => '\kartik\grid\ActionColumn',
                    'header' => 'Akcje',
                    'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
                    'viewOptions' => [
                         'title' => 'Pokaż szczegóły'
                    ],
                    'updateOptions' => [
                         'title' => 'Edytuj'
                    ],
                    'deleteOptions' => [
                         'title' => 'Usuń',
                         'message' => 'Potwierdź usunięcie'
                    ],
               ],
          ],
     ])
     ?>
</div>

<script type="text/javascript">
     function apply_filter() {

          $('.grid-view').yiiGridView('applyFilter');

     }
</script>