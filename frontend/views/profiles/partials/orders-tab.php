<?php

use common\models\ClientsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\OrdersModel;
use common\models\UsersModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;

/* @var $user UsersModel */
/* @var $model OrdersModel */

$deleteButton = $user->isAdminOrSupervisor() ?
        Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone zlecenia'), Url::to(['/orders/delete']), ['class' => 'btn btn-danger']) : '';
?>

<div class="site-index">

     <?=
     GridView::widget([
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-list"></i> Zlecenia',
          ],
          'showPageSummary' => true,
          'toolbar' => [
               [
                    'content' => implode('', [
                         Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj zlecenie', Url::to(['/orders/add']), ['class' => 'btn btn-success']),
                         $deleteButton,
                         Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $user->usr_id], [
                              'class' => 'btn btn-default',
                              'title' => Yii::t('app', 'Resetuj widok')
                         ])
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
          ],
          'dataProvider' => $orderDataProvider,
          'filterModel' => $orderSearchModel,
          'columns' => [
               [
                    'class' => '\kartik\grid\CheckboxColumn'
               ],
               [
                    'class' => '\kartik\grid\SerialColumn'
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $userAllOrdersNames,
                         'options' => [
                              'placeholder' => 'filtruj po nazwie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true,
                              'minimumInputLength' => 3,
                         ]
                    ],
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->getShortName(), Url::to(['/orders/view', 'id' => $model->ord_id]), ['data-pjax' => 0, 'title' => $model->ord_name]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => OrdersModel::listStatuses(),
                         'options' => [
                              'placeholder' => 'filtruj po statusie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'status',
                    'value' => function($model) {
                         return OrdersModel::listStatuses()[$model->ord_status];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'attribute' => 'created',
                    'filterWidgetOptions' => [
                         'presetDropdown' => true,
                         'pluginOptions' => [
                              'locale' => [
                                   'format' => 'YYYY-MM-DD',
                              ]
                         ],
                         'pluginEvents' => [
                              "apply.daterangepicker" => "function() { apply_filter('date') }",
                         ]
                    ],
                    'value' => function ($model) {
                         return date('d-m-Y H:i', $model->ord_created_at);
                    },
                    'label' => 'Utworzony'
               ],
               [
                    'class' => '\kartik\grid\ActionColumn',
                    'header' => 'Akcje',
                    'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
                    'viewOptions' => [
                         'title' => 'Pokaż szczegóły'
                    ],
                    'updateOptions' => [
                         'title' => 'Edytuj'
                    ],
                    'deleteOptions' => [
                         'title' => 'Usuń'
                    ],
               ],
          ],
     ])
     ?>
</div>

<script type="text/javascript">
     function apply_filter() {

          $('.grid-view').yiiGridView('applyFilter');

     }
</script>

