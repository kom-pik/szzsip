<?php

use common\models\UsersModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $user UsersModel */
/* @var $this View */


$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$form = ActiveForm::begin([
            'options' => ['id' => 'user-form'],
        ]);
?>

<?= $form->field($user, 'usr_firstname')->textInput() ?>
<?= $form->field($user, 'usr_lastname')->textInput() ?>
<?= $form->field($user, 'usr_username')->textInput(['disabled' => $user->usr_username ? 'disabled' : '']) ?>
<?= $form->field($user, 'usr_email')->textInput() ?>
<?= $form->field($user, 'usr_phone')->textInput() ?>
<hr>
<?= $form->field($user, 'currentPassword')->passwordInput() ?>
<?= $form->field($user, 'newPassword')->passwordInput() ?>
<?= $form->field($user, 'newPasswordConfirm')->passwordInput() ?>



<div class="modal-footer">
<?=
Html::submitButton(Yii::t('app', 'Zapisz'), [
    'class' => 'btn btn-primary col-sm-12'
])
?>
</div>


<?php $form->end(); ?>


