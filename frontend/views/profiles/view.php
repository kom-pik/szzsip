<?php

use yii\helpers\Html;
use yii\web\View;
use common\models\UsersModel;
use yii\bootstrap\Tabs;

/* @var $user UsersModel */
/* @var $this View */


$this->params['breadcrumbs'][] = $this->title;
$items[] = [
            'label' => 'Informacje ogólne',
            'active' => 'true',
            'options' => [
                'id' => 'info',
            ],
            'content' => $this->render('partials/info-tab', ['user' => $user])
          ];
if (!$this->params['user']->isClient()){
     $items = array_merge($items ,[[
            'label' => 'Moje projekty',
            'options' => [
                'id' => 'projects',
            ],
            'content' => $this->render('partials/projects-tab', [
                'user' => $user,
                'projectDataProvider' => $projectDataProvider,
                'projectSearchModel' => $projectSearchModel,
                'clientAllProjectsNames' => $userAllProjectsNames
            ])
        ],
        [
            'label' => 'Moje zlecenia',
            'options' => [
                'id' => 'orders',
            ],
            'content' => $this->render('partials/orders-tab', [
                'user' => $user,
                'orderDataProvider' => $orderDataProvider,
                'orderSearchModel' => $orderSearchModel,
                'userAllOrdersNames' => $userAllOrdersNames
            ])
        ],
        [
            'label' => 'Moi klienci',
            'options' => [
                'id' => 'clients',
                'visible' => !$user->isClient()
            ],
//            'content' => $this->render('partials/orders-tab', [
//                'user' => $user,
//                'orderDataProvider' => $orderDataProvider,
//                'orderSearchModel' => $orderSearchModel,
//                'userAllOrdersNames' => $userAllOrdersNames
//            ])
        ]]);
}
?>
<?=

Html::a(Yii::t('app', 'Edytuj swoje dane'), ['update'], [
    'class' => 'btn btn-success',
    'id' => 'btn-edit'
])
?>
<?=

Tabs::widget([
    'items' => $items
])
?>

