<?php

use common\models\ProjectsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $project ProjectsModel */
/* @var $this View */


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog modal-lg">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left"><i class="glyphicon glyphicon-plus"></i> Dodaj projekt</h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <?php
          $form = ActiveForm::begin([
                       'options' => ['id' => 'project-form'],
          ]);
          if (!empty($project->pro_client_fkey) && !empty($project->pro_group_fkey)){
               echo Html::hiddenInput('ProjectsModel[pro_client_fkey]', $project->pro_client_fkey);
               echo Html::hiddenInput('ProjectsModel[pro_group_fkey]', $project->pro_group_fkey);
          }

          ?>
          <div class="modal-body project-form">
               <div class="row">
                    <div class="col-md-8">

                         <?= $form->field($project, 'pro_name')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                         <?= $form->field($project, 'pro_status')->dropDownList($project::listOpenStatuses(), ['prompt' => 'Wybierz status (domyślnie nowy)']) ?>
                    </div>
               </div>

               <div class="row">
                    <div class="col-md-4">
                         <?=
                                 $form->field($project, 'pro_owner_fkey')
                                 ->dropDownList($userList, [
                                      'prompt' => 'Wybierz właściciela projektu (domyślnie: ' . Yii::$app->user->identity->usr_username . ')'])
                         ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                                 $form->field($project, 'pro_client_fkey')
                                 ->dropDownList($clientsList, [
                                      'options' => [
                                           $project->pro_client_fkey => ['Selected' => true]
                                      ],
                                      'prompt' => 'Wybierz klienta',
                                      $project->isNewRecord && empty($project->pro_client_fkey) ? '' : 'disabled' => true
                                 ])->label($project->attributeLabels()['pro_client_fkey'] .
                                         Html::a(
                                                 '<i class="glyphicon glyphicon-plus m-l-5 show-client-form"></i>',
                                                 '/clients/add',
                                                 [
                                                      'class' => 'relative hint-top-middle',
                                                      'data-hint' => "Dodaj klienta"
                                                 ]
                                                 )
                                         )
                         ?>
                    </div>
                    <div class="col-md-4">
                         <?=
                         $this->params['user']->isAdmin() ? $form->field($project, 'pro_group_fkey')
                                         ->dropDownList($groupsList, [
                                              'options' => [
                                                   $project->pro_group_fkey || (!empty($project->client) && $project->client->cli_group_fkey) => ['Selected' => true]
                                              ],
                                              'prompt' => 'Wybierz grupę',
                                              $project->isNewRecord ? '' : 'disabled' => TRUE,
                                              !$project->pro_group_fkey ? '' : 'disabled' => true
                                         ]) : ''
                         ?>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-4">
                         <?= $form->field($project, 'pro_type')->dropDownList($project::listTypes(), ['prompt' => 'Wybierz rodzaj projektu']) ?>
                    </div>
                    <div class="col-md-4">
                         <?= $form->field($project, 'pro_budget_type')->dropDownList($project::listBudgetTypes(), ['prompt' => 'Wybierz rodzaj projektu']) ?>
                    </div>
                    <div class="col-md-4">
                         <?= $form->field($project, 'pro_budget_value')->input('number', ['min' => '0', 'step' => '0.01', 'value' => !empty($project->pro_budget_value) ? $project->pro_budget_value : '0.00']) ?>
                    </div>
               </div>    
               <div class="row">
                    <div class="col-md-12">
                         <?= $form->field($project, 'pro_description')->textArea(['class' => 'tinymce']) ?>
                    </div>
               </div>

          </div>
          <div class="modal-footer project-form">

               <?=
               Html::submitButton(Yii::t('app', 'Zapisz'), [
                    'class' => 'btn btn-primary col-sm-12 col-xs-12'
               ])
               ?>
               <?php ActiveForm::end(); ?> 

          </div>
          <div class="my-hidden hidden-client-form">
               <?php echo $this->render('/clients/partials/client-form', array(
                    'partialData' => array(
                         'client' => new \common\models\ClientsModel(),
                         'groupsList' => $groupsList,
                         'userList' => $userList,
                         'priceLists' => array(),
                    ),
                    'disabled' => true
               ))?>
          </div>
     </div>
</div>
<script>
     tinymce.init(tinymceDefaultOptions);
</script>