<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\ProjectsModel;
use common\models\UsersModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;

/* @var $this View */
/* @var $projects ProjectsModel[] */
/* @var $dataProvider ActiveDataProvider */
/* @var $user UsersModel */

$this->params['breadcrumbs'][] = $this->title;
$user = $this->params['user'];
?>

<div class="site-index">

     <?=
     GridView::widget([
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-briefcase"></i> Projekty',
          ],
          'showPageSummary' => true,
          'toolbar' => [
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj projekt', Url::to(['add']), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
               ],
               [
                    'content' => !$user->isClient() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone projekty'), Url::to(['delete']), ['class' => 'btn btn-danger grid-mass-delete disabled']) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> ' . Yii ::t('app', 'reset_grid_view'), ['index'], [
                         'class' => 'btn btn-default reset-grid',
                         'title' => Yii::t('app', 'Resetuj widok')
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
               [
                    'class' => '\kartik\grid\CheckboxColumn'
               ],
               [
                    'class' => '\kartik\grid\SerialColumn'
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $projectsNames,
                         'options' => [
                              'placeholder' => 'filtruj po nazwie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true,
                              'minimumInputLength' => 3,
                         ]
                    ],
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->getShortName(), Url::to(['view', 'id' => $model->pro_id]), ['data-pjax' => 0, 'title' => $model->pro_name]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $userList,
                         'options' => [
                              'placeholder' => 'filtruj po właścicielu ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'owner',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->owner->usr_username, Url::to(['/users/view', 'id' => $model->pro_owner_fkey]), ['data-pjax' => 0]);
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => $clientsList,
                         'options' => [
                              'placeholder' => 'filtruj po kliencie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'client',
                    'format' => 'raw',
                    'value' => function($model) {
                         return $model->client ?
                                 Html::a($model->client->cli_acronym, Url::to(['/clients/view', 'id' => $model->pro_client_fkey]), ['data-pjax' => 0]) :
                                 Html::a('dodaj klienta', Url::to(['add-property', 'id' => $model->pro_id, 'property' => 'pro_client_fkey', 'action' => 'index']), ['data-pjax' => 0, 'title' => 'dodaj klienta', 'class' => 'add-property']);
                         ;
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => ProjectsModel::listStatuses(),
                         'options' => [
                              'placeholder' => 'filtruj po statusie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'status',
                    'value' => function($model) {
                         return ProjectsModel::listStatuses()[$model->pro_status];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'attribute' => 'created',
                    'filterWidgetOptions' => [
                         'presetDropdown' => true,
                         'pluginOptions' => [
                              'locale' => [
                                   'format' => 'YYYY-MM-DD',
                              ]
                         ],
                         'pluginEvents' => [
                              "apply.daterangepicker" => "function() { apply_filter('date') }",
                         ]
                    ],
                    'value' => function ($model) {
                         return date('d-m-Y H:i', $model->pro_created_at);
                    },
               ],
               [
                    'class' => '\kartik\grid\ActionColumn',
                    'header' => 'Akcje',
                    'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
                    'viewOptions' => [
                         'title' => 'Pokaż szczegóły'
                    ],
                    'updateOptions' => [
                         'title' => 'Edytuj'
                    ],
                    'deleteOptions' => [
                         'title' => 'Usuń'
                    ],
               ],
          ],
     ])
     ?>
</div>

<script type="text/javascript">
     function apply_filter() {

          $('.grid-view').yiiGridView('applyFilter');

     }
</script>