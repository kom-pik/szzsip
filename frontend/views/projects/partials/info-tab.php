<?php

use common\models\ProjectsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\OrdersModel;
use common\models\UsersModel;
use yii\data\ActiveDataProvider;

/* @var $project ProjectsModel */
/* @var $this View */
/* @var $orders OrdersModel[] */
/* @var $dataProvider ActiveDataProvider */
/* @var $user UsersModel */

$user = $this->params['user'];
?>
<?php if (!empty($modal)) : ?>
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h3  class="pull-left">Projekt <?= $project->pro_name ?></h3>
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
               <?php endif; ?>
               
                    <div class="col-sm-12"><div class="entity-info">
                    <?php if (empty($modal) && !$user->isClient()) : ?>
                         <div class="row m-t-5">
                              <?=
                              Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Dodaj zlecenie do projektu'), ['/orders/add', 'project_id' => $project->pro_id], ['class' => 'btn btn-success add-object', 'id' => 'buttonAddOrder'])
                              ?>
                              <?=
                              Html::a(Yii::t('app', '<i class="glyphicon glyphicon-pencil"></i> Edytuj projekt'), ['/projects/update', 'id' => $project->pro_id], ['class' => 'btn btn-success btn-update'])
                              ?>
                              <?=
                              Html::a('<i class="glyphicon glyphicon-usd"></i> Dodaj koszt', ['add-cost', 'entityId' => $project->pro_id, 'entityType' => \common\models\EntitiesCostsModel::ENTITY_TYPE_PROJECTS], ['class' => 'btn btn-success add-object'])
                              ?>
                              <hr> 
                         </div>
                    <?php endif; ?>
                    <div class="row">
                         <div class="col-sm-12">
                              <label class=""><?= $project->getAttributeLabel('pro_name') ?></label>
                              <div><?= $project->pro_name ?></div>
                              <hr>
                         </div>

                         <div class="col-sm-12">
                              <label><?= $project->getAttributeLabel('pro_description') ?></label>
                              <div><?= $project->pro_description ?></div>
                              <hr>
                         </div>
                    </div>


                    <div class="row">
                         <?php if (!$user->isClient()): ?>
                              <div class="col-sm-2">
                                   <label><?= $project->getAttributeLabel('pro_client_fkey') ?></label>
                                   <div><?=
                                        $project->client ?
                                                Html::a($project->client->cli_acronym, Url::to(['/clients/view', 'id' => $project->client->cli_id])) :
                                                Html::a(Yii::t('app', 'dodaj klienta'), Url::to(['add-property', 'id' => $project->pro_id, 'property' => 'pro_client_fkey']), ['class' => 'add-property'])
                                        ?>
                                   </div><hr>
                              </div>

                              <div class="col-sm-2">
                                   <label><?= $project->getAttributeLabel('pro_owner_fkey') ?></label>
                                   <div><?= Html::a($project->owner->usr_username, Url::to(['/users/view', 'id' => $project->owner->usr_id])) ?></div><hr>
                              </div>
                         <?php endif; ?>
                         <?php if (!$user->isClient()): ?>
                              <div class="col-sm-2">
                                   <label><?= $project->getAttributeLabel('pro_group_fkey') ?></label>
                                   <div><?= !empty($project->group) ? Html::a($project->group->gro_name, Url::to(['/groups/view', 'id' => $project->group->gro_id]), ['target' => '_blank']) : 'brak'?></div><hr>
                              </div>
                         <?php endif; ?>
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('pro_created_at') ?></label>
                              <div><?= date('d-m-Y H:i', $project->pro_created_at) ?></div>
                              <hr>
                         </div>

                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('pro_status') ?></label>
                              <div>
                                   <?= ProjectsModel::listStatuses()[$project->pro_status] ?>                    
                                   <?=
                                   $user->isClient() ? '' :
                                           Html::a(Yii::t('app', '<i class="glyphicon glyphicon-refresh"></i>'), Url::to(['add-property', 'id' => $project->pro_id, 'property' => 'pro_status']), [
                                               'class' => 'add-property',
                                               'title' => 'Zmień status',
                                               'class' => 'add-property m-l-5'
                                           ])
                                   ?>

                              </div>
                              <hr>
                         </div>
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('pro_type') ?></label>
                              <div><?= !empty($project->pro_type) ? $project::listTypes()[$project->pro_type] : Html::a('dodaj typ projektu', Url::to(['add-property', 'id' => $project->pro_id, 'property' => 'pro_type']), ['class' => 'add-property'])?></div>
                              <hr>
                         </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('pro_budget_type') ?></label>
                              <div><?= !empty($project->pro_budget_type) ? $project::listBudgetTypes()[$project->pro_budget_type] : Html::a('dodaj typ budżetu', Url::to(['add-property', 'id' => $project->pro_id, 'property' => 'pro_budget']), ['class' => 'add-property'])?></div>
                              <hr>
                         </div>
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('pro_budget_value') ?></label>
                              <div><?= !empty($project->pro_budget_value) ? $project->pro_budget_value : Html::a('dodaj wartość budżetu', Url::to(['add-property', 'id' => $project->pro_id, 'property' => 'pro_budget']), ['class' => 'add-property'])?></div>
                              <hr>
                         </div>
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('time') ?></label>
                              <div><?= common\helpers\TimeHelper::HourMinSec($project->getProjectTime())?></div>
                              <hr>
                         </div>
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('budget_use') ?></label>
                              <div><?= number_format($project->getBudgetUse()*100, 2, ',', ' ') . '%'?></div>
                              <hr>
                         </div>
                         <div class="col-sm-2">
                              <label><?= $project->getAttributeLabel('costs') ?></label>
                              <div><?= Html::a(number_format($project->getCostsValue(), 2, ',', ' ') . ' PLN', Url::to('#costs-tab'), ['data-toggle' => 'tab'])?></div>
                              <hr>
                         </div>
                    </div>
                    </div>
                    <div class="row">
                         <?php
                         if (empty($modal)) {
                              $params = [
                                  'user' => $user,
                                  'project' => $project,
                                  'searchModel' => $searchModel,
                                  'dataProvider' => $dataProvider,
                                  'userList' => $userList,
                                  'clientsList' => $clientsList,
                                  'orderNames' => $orderNames,
                                  'orders' => $orders];
                              echo $this->render('view/project-orders-grid', $params);
                         }
                         ?>

                    </div>
               </div>
<?php if (!empty($modal)) : ?>
                    <div class="modal-footer"></div>
               </div>
          </div>
     </div>
<?php endif; ?>