<?php

use common\models\ProjectsModel;
use common\models\ClientsModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $project ProjectsModel */
/* @var $this View */
/* @var $property string */

$form = ActiveForm::begin([
            'options' => ['id' => 'add-property-form'],
            'action' => Url::to(['add-property', 'id' => $project->pro_id, 'property' => $property, 'action' => $action])
        ]);

switch ($property) {
     case 'pro_status':
          $header = 'Zmień status projektu';
          $field = $form->field($project, 'pro_status')->dropDownList($project->listStatuses(), ['prompt' => 'Wybierz status']);
          break;
     case 'pro_client_fkey':
          $header = 'Dodaj klienta do projektu';
          $field = $form->field($project, 'pro_client_fkey')->dropDownList(ClientsModel::findAllClients(null, $this->params['group_id']), ['prompt' => 'Wybierz klienta']);
          break;
     case 'pro_type':
          $header = 'Wybierz typ projektu';
          $field = $form->field($project, 'pro_type')->dropDownList($project::listTypes(), ['prompt' => 'Wybierz typ projektu']);
          break;
     case 'pro_budget':
          $header = 'Dodaj budżet projektu';
          $fields[] = $form->field($project, 'pro_budget_type')->dropDownList($project::listBudgetTypes(), ['prompt' => 'Wybierz rodzaj budżetu']);
          $fields[] = $form->field($project, 'pro_budget_value')->textInput(['placeholder' => 'Wpisz wartość budżetu']);
          break;
}
?> 
<div class="modal-dialog">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left"><?= $header ?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
               <?php if (!empty($field)){
                    echo $field;
               } else {
                    foreach ($fields as $field){
                         echo $field;
                    }
               }  ?>

          </div>
          <div class="modal-footer">
               <?=
               Html::submitButton(Yii::t('app', 'Zapisz'), [
                   'class' => 'btn btn-primary col-sm-12',
//                'id' => 'modal-submit-button',
               ])
               ?>
          </div>


          <?php $form->end(); ?>
     </div>
</div>