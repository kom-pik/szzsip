<?php

use frontend\models\Project;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $project Project */
/* @var $this View */
$form = ActiveForm::begin([
             'options' => ['id' => 'project-form'],
        ]);
?>
<div class="row">
     <div class="col-sm-12">
          <div><?= $form->field($project, 'pro_name')->textInput() ?></div>
          <hr>
     </div>

     <div class="col-sm-12">
          <div><?= $form->field($project, 'pro_description')->textArea(['class' => 'tinymce']) ?></div>
          <hr>
     </div>
</div>
<div class="row">
     <div class="col-sm-3 col-xs-12">
          <div><?= $form->field($project, 'pro_client_fkey')->dropDownList($clientsList, ['prompt' => 'Wybierz klienta']) ?></div>
          <hr>
     </div>

     <div class="col-sm-3 col-xs-12">
          <div><?= $form->field($project, 'pro_owner_fkey')->dropDownList($userList, ['prompt' => 'Wybierz właściciela projektu (domyślnie: ' . Yii::$app->user->identity->usr_username . ')']) ?></div>
          <hr>
     </div>

     <div class="col-sm-3 col-xs-12">
          <label><?= $project->getAttributeLabel('pro_created_at') ?></label>
          <div><?= date('d-m-Y H:i', $project->pro_created_at) ?></div>
          <hr>
     </div>

     <div class="col-sm-3 col-xs-12">
          <div><?= $form->field($project, 'pro_status')->dropDownList($project->isNewRecord ? $project::listOpenStatuses() : $project::listStatuses(), ['prompt' => 'Wybierz status (domyślnie nowy)']) ?></div>
          <hr>
     </div>
</div>
<div class="row">
     <div class="col-sm-3 col-xs-12">
          <div><?= $form->field($project, 'pro_type')->dropDownList($project::listTypes(), ['prompt' => 'Wybierz typ projektu']) ?></div>
          <hr>
     </div>
     <div class="col-sm-3 col-xs-12">
          <div><?= $form->field($project, 'pro_budget_type')->dropDownList($project::listBudgetTypes(), ['prompt' => 'Wybierz typ budżetu']) ?></div>
          <hr>
     </div>
     <div class="col-sm-3 col-xs-12">
          <div>
               <?=
               $form->field($project, 'pro_budget_value')->input('number', [
                    'min' => '0',
                    'step' => '0.01',
                    'placeholder' => 'Wpisz wartość budżetu'
               ])
               ?>
          </div>
          <hr>
     </div>
     <?php if ($this->params['user']->isAdmin() && empty($project->pro_group_fkey)): ?>
     <div class="col-sm-3 col-xs-12">
          <div>
               <?= $form->field($project, 'pro_group_fkey')->dropDownList($groupsList, [
                    'prompt' => 'Wybierz grupę',
                    $project->pro_group_fkey => ['selected' => true]
                       ])
               ?>
          </div>
          <hr>
     </div>
     <?php endif;?>
</div>


<div class="modal-footer">
     <?=
     Html::submitButton(Yii::t('app', 'Zapisz'), [
          'class' => 'btn btn-success col-sm-4'
     ])
     ?>
</div>


<?php $form->end(); ?>

<script>
     tinymce.init(tinymceDefaultOptions);
</script>