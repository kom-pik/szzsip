<?php

use common\models\ProjectsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\OrdersModel;
use common\models\UsersModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\helpers\TimeHelper;

/* @var $project ProjectsModel */
/* @var $this View */
/* @var $orders OrdersModel[] */
/* @var $dataProvider ActiveDataProvider */
/* @var $user UsersModel */

$columns = [
     [
          'class' => '\kartik\grid\CheckboxColumn'
     ],
     [
          'class' => '\kartik\grid\SerialColumn'
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => $orderNames,
               'options' => [
                    'placeholder' => 'filtruj po nazwie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true,
               ]
          ],
          'attribute' => 'name',
          'format' => 'raw',
          'value' => function($model) {
               return Html::a(Yii::t('app', $model->ord_name), Url::to(['/orders/view', 'id' => $model->ord_id]), ['data-pjax' => 0, 'title' => $model->ord_name]);
          }
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => $userList,
               'options' => [
                    'placeholder' => 'filtruj po właścicielu ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true
               ]
          ],
          'attribute' => 'owner',
          'format' => 'raw',
          'value' => function($model) {
               return Html::a($model->owner->usr_username, Url::to(['/users/view', 'id' => $model->owner->usr_id]), ['data-pjax' => 0]);
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => $clientsList,
               'options' => [
                    'placeholder' => 'filtruj po kliencie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true
               ]
          ],
          'attribute' => 'client',
          'format' => 'raw',
          'value' => function($model) {
               if ($model->client) {
                    return Html::a($model->client->cli_acronym, Url::to(['/clients/view', 'id' => $model->client->cli_id]), ['data-pjax' => 0]);
               }
               return 'brak';
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_SELECT2,
          'filterWidgetOptions' => [
               'data' => OrdersModel::listStatuses(),
               'options' => [
                    'placeholder' => 'filtruj po statusie ...',
                    'initValueText' => ''
               ],
               'pluginOptions' => [
                    'allowClear' => true
               ]
          ],
          'attribute' => 'status',
          'value' => function($model) {
               return OrdersModel::listStatuses()[$model->ord_status];
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'filterType' => GridView::FILTER_DATE_RANGE,
          'attribute' => 'created',
          'filterWidgetOptions' => [
               'presetDropdown' => true,
               'pluginOptions' => [
                    'locale' => [
                         'format' => 'YYYY-MM-DD',
                    ]
               ],
               'pluginEvents' => [
                    "apply.daterangepicker" => "function() { apply_filter('date') }",
               ]
          ],
          'value' => function ($model) {
               return date('d-m-Y H:i', $model->ord_created_at);
          },
     ],
     [
          'class' => '\kartik\grid\DataColumn',
          'attribute' => 'time',
          'value' => function ($model) {
               return TimeHelper::HourMinSec($model->getOrderTime());
          },
          'filter' => false
     ],
];

if (!$user->isClient()) {
     $columns[] = [
          'class' => '\kartik\grid\ActionColumn',
          'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
          'header' => 'Akcje',
          'controller' => 'orders',
          'viewOptions' => [
               'title' => 'Pokaż szczegóły'
          ],
          'updateOptions' => [
               'title' => 'Edytuj'
          ],
          'deleteOptions' => [
               'title' => 'Usuń',
               'message' => 'Potwierdź usunięcie zlecenia'
          ],
     ];
}
echo GridView::widget([
     'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<i class="glyphicon glyphicon-list"></i> Zlecenia projektu',
     ],
     'toolbar' => [
          [
               'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj zlecenie', Url::to(['/orders/add', 'project_id' => $project->pro_id]), ['class' => 'btn btn-success add-object']),
          ],
          [
               'content' => $user->isAdminOrSupervisor() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczone zlecenia'), Url::to(['add']), ['class' => 'btn btn-danger']) : ''
          ],
          [
               'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['view', 'id' => $project->pro_id], [
                    'class' => 'btn btn-default',
                    'title' => Yii::t('app', 'Resetuj widok')
               ])
          ],
          '{export}',
          '{toggleData}'
     ],
     'pjax' => true,
     'pjaxSettings' => [
          'neverTimeout' => true,
          'enablePushState' => false,
     ],
     'dataProvider' => $dataProvider,
     'filterModel' => $searchModel,
     'columns' => $columns,
     'showPageSummary' => true,
]);


