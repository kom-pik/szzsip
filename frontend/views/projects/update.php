<?php
use yii\web\View;

/* @var $this View*/

$this->params['breadcrumbs'][] = $this->title;

echo $this->render('partials/project-form', [
               'project' => $project,
               'userList' => $userList,
               'clientsList' => $clientsList,
               'groupsList' => $groupsList
    ]);


