<?php

use yii\helpers\Html;
use yii\web\View;
use common\models\ProjectsModel;
use yii\bootstrap\Tabs;

/* @var $project ProjectsModel */
/* @var $this View */

$this->params['breadcrumbs'][] = $this->title;
$user = $this->params['user'];
?>

<?php if (!empty($modal)) : ?>
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h3 class="pull-left">Projekt <?= $project->pro_name ?></h3>
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               </div>
               <div class="modal-body">
               <?php endif; ?>
               <?php
               $items = [
                    [
                    'label' => 'Informacje ogólne',
                    'options' => [
                         'id' => 'info-tab'
                    ],
                    'active' => 'true',
                    'content' => $this->render('partials/info-tab', [
                         'project' => $project,
                         'searchModel' => $searchModel,
                         'dataProvider' => $dataProvider,
                         'userList' => $userList,
                         'clientsList' => $clientsList,
                         'orderNames' => $orderNames,
                         'orders' => $orders])
                    ],
                    [
                         'label' => 'Koszty',
                         'options' => [
                              'id' => 'costs-tab'
                         ],
                         'content' => $this->render('/partials/costs-tab', [
                              'user' => $user,
                              'entity' => $project,
                              'costsTabParams' => $costsTabParams
                         ])
                    ],
               ];

               echo Tabs::widget([
                    'items' => $items
               ]);
               ?>
               <?php if (!empty($modal)) : ?>
                    <div class="modal-footer"></div>
               </div>
          </div>
     </div>
<?php endif; ?>
