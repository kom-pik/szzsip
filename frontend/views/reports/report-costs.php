<?php
use common\models\ReportModel;
use common\models\ClientsModel;
use common\helpers\TimeHelper;
use common\models\EntitiesCostsModel;

/* @var $reportModel ReportModel*/
/* @var $client ClientsModel*/

$i = 1;
if (!empty($client->costs)) :
?>
<div>
     <div class="report-label-div m-t-5"><label>Koszty wg typu</label></div>
     <table class="table table-striped table-hover">
          <th>Lp.</th>
          <th>Rodzaj kosztu</th>
          <th>Wartość</th>

          <tbody>
               <?php foreach ($client->costs as $cost): ?>
               <tr>
                    <td><?php echo $i?>.</td>
                    <td><?php echo EntitiesCostsModel::listCostsTypes()[$cost['enc_type']]?></td>
                    <td><?php echo $cost['cost_group_value']?> PLN</td>
               </tr>
               <?php 
               $i++;
               ?>
               <?php endforeach;?>               
          </tbody>
          <tfoot>
               <tr>
                    <td></td>
                    <td><label>Koszty netto razem</label></td>
                    <td><label><?php echo number_format($client->costsValue, 2, ',', ' ')?> PLN</label></td>
               </tr>
          </tfoot>
     </table>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif;
