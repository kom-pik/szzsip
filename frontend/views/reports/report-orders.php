<?php

use common\helpers\TimeHelper;
use common\models\OrdersModel;
use yii\helpers\Html;

/* @var $ordersData OrdersModel[]*/
$i = 1;
if (!empty($ordersData)) :

?>
<div>
     <div class="report-label-div m-t-5"><label>Zlecenia</label></div>
     <table class="table table-striped table-hover">
          <th>Lp.</th>
          <th>Nazwa</th>
          <th>Właściciel</th>
          <?php if (empty($client)):?>
          <th>Klient</th>
          <?php endif;?>
          <th>Projekt</th>
          <th>Budżet</th>
          <th>% wykorzystania</th>
          <th>Koszty</th>
          <th>Liczba godzin</th>

          <tbody>
               <?php foreach ($ordersData as $order): ?>
               <tr>
                    <td><?php echo $i?>.</td>
                    <td><?php echo Html::a($order->ord_name, ['orders/view', 'id' => $order->ord_id], ['class' => 'report-entity-name', 'target' => '_blank']) ?></td>
                    <td><?php echo !empty($order->owner) ? Html::a($order->owner->getName(), ['/users/view', 'id' => $order->owner->usr_id], ['class' => '', 'target' => '_blank']) : 'brak'?></td>
                    <?php if (empty($client)):?>
                    <td><?php echo !empty($order->client) ? "<a href='/clients/view/{$order->client->cli_id}' target='_blank'>{$order->client->getName()}</a>" : 'brak'?></td>
                    <?php endif;?>
                    <td><?php echo !empty($order->project) ? $order->project->pro_name : '-'?></td>
                    <td><?php echo !empty($order->ord_budget_type) && !empty($order->ord_budget_value) ? $order::listBudgetTypes()[$order->ord_budget_type] ." ({$order->pro_budget_value})": '-'?></td>
                    <td><?php echo !empty($order->ord_type) && !empty($order->ord_budget_value) ? number_format($order->budgetUse*100, 2, ',', ' ').'%' : '-'?></td>
                    <td><?php echo number_format($order->costsValue, 2, ',', ' ')?> PLN</td>
                    <td><?php echo TimeHelper::HourMinSec($order->orderTime)?></td>
               </tr>
               <?php 
               $i++;
               ?>
               <?php endforeach;?>               
          </tbody>
          <tfoot>
               <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <?php if (empty($client)):?>
                    <td></td>
                    <?php endif;?>
                    <td></td>
                    <td><label>Razem</label></td>
                    <td><label><?php echo number_format($totalCost, 2, ',', ' ')?> PLN</label></td>
                    <td><label><?php echo TimeHelper::HourMinSec($totalTime)?></label></td>
               </tr>
          </tfoot>
     </table>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif; 
