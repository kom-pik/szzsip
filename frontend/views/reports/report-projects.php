<?php

use common\helpers\TimeHelper;
use common\models\ProjectsModel;
use yii\helpers\Html;

/* @var $projectsData ProjectsModel[]*/
$i = 1;
if (!empty($projectsData)) :
?>
<div>
     <div class="report-label-div m-t-5"><label>Projekty</label></div>
     <table class="table table-striped table-hover">
          <th>Lp.</th>
          <th>Nazwa</th>
          <th>Właściciel</th>
          <?php if (empty($client)):?>
          <th>Klient</th>
          <?php endif;?>
          <th>Budżet</th>
          <th>% wykorzystania</th>
          <th>Koszty</th>
          <th>Liczba godzin</th>

          <tbody>
               <?php foreach ($projectsData as $project): ?>
               <tr>
                    <td><?php echo $i?>.</td>
                    <td><?php echo Html::a($project->pro_name, ['projects/view', 'id' => $project->pro_id], ['class' => 'report-entity-name', 'target' => '_blank']) ?></td>
                    <td><?php echo Html::a($project->owner->getName(), ['/users/view', 'id' => $project->owner->usr_id], ['class' => '', 'target' => '_blank'])?></td>
                    <?php if (empty($client)):?>
                    <td><?php echo !empty($project->client) ? "<a href='/clients/view/{$project->client->cli_id}' target='_blank'>{$project->client->getName()}</a>" : 'brak'?></td>
                    <?php endif;?>
                    <td><?php echo !empty($project->pro_budget_type) && !empty($project->pro_budget_value) ? $project::listBudgetTypes()[$project->pro_budget_type] ." ({$project->pro_budget_value})": '-'?></td>
                    <td><?php echo !empty($project->pro_type) && !empty($project->pro_budget_value) ? number_format($project->budgetUse*100, 2, ',', ' ').'%' : '-'?></td>
                    <td><?php echo number_format($project->costsValue, 2, ',', ' ')?> PLN</td>
                    <td><?php echo TimeHelper::HourMinSec($project->projectTime)?></td>
               </tr>
               <?php 
               $i++;
               ?>
               <?php endforeach;?>               
          </tbody>
          <tfoot>
               <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <?php if (empty($client)):?>
                    <td></td>
                    <?php endif;?>
                    <td></td>
                    <td><label>Razem</label></td>
                    <td><label><?php echo number_format($totalCost, 2, ',', ' ')?> PLN</label></td>
                    <td><label><?php echo TimeHelper::HourMinSec($totalTime)?></label></td>
               </tr>
          </tfoot>
     </table>
</div>
<?php 
else:
     echo '<div">Brak danych</div>';
endif; 
