<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'O aplikacji';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div>Aplikacja SZZSIP to CRM w chmurze, dostępny zawsze tam, gdzie jest Internet.</div>
    <div>
         SZZSIP pozwala na:
         <ul>
              <li>Tworzenie projektów, zleceń i zadań</li>
              <li>Przypisywanie projektów, zleceń i zadań do klientów i wykonawców</li>
              <li>Tworzenie bazy klientów</li>
              <li>Raportowanie kosztów i czasu pracy nad konkretnym zadaniem/zleceniem/projektem</li>
              <li>Dodawanie wielu użytkowników z różnymi poziomami uprawnień (administrator, kierownik, pracownik, klient)</li>
              <li>Tworzenie cenników i przypisywanie ich do klientów</li>
         </ul>
    </div>
</div>
