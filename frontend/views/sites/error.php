<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Powyższy błąd nastąpił podczas przetwarzania Twojego zapytania przez serwer.
    </p>
    <p>
         Jeśli problem będzie się powtarzał, prosimy o <?php echo Html::a('kontakt', '/sites/contact')?>.
    </p>

</div>
