<?php

use yii\widgets\ActiveForm;
use common\models\MigrationsModel;
use yii\helpers\Html;

$this->title = 'Migracje';
$this->params['breadcrumbs'][] = $this->title;

$migration = new MigrationsModel;
$form = ActiveForm::begin();

?>
<div class="container">
     <div class="row">
          <div class="col-sm-12">
               <?php echo $form->field($migration, 'name')->input('text', ['placeholder' => 'nazwa', 'required' => 'required'])->label(false); ?>
          </div>
     </div>
     <div class="row">
          <div class="col-sm-12">
               <?php echo $form->field($migration, 'body')->textArea(['rows' => 10, 'placeholder' => 'SQL', 'required' => 'required'])->label(false); ?>
          </div>
     </div>
     <div class="row">
          <div class="col-sm-12">
               <?php echo Html::submitButton('Zapisz', ['class' => 'btn btn-success col-sm-4']); ?>
          </div>
          
     </div>
     <?php ActiveForm::end();?>
     <div class="row">
          <?php if (!empty($migrations)) :?>
          <?php foreach ($migrations as $mig) :?>
          <pre><?php echo $mig;?></pre>
          <?php endforeach;?>
          <?php endif;?>
     </div>
</div>
