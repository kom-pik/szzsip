<?php

use frontend\models\AddWorktimeForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\datetime\DateTimePicker;

/* @var $addWorkTimeModel AddWorktimeForm */
/* @var $this View */

$form = new ActiveForm();
?>
<div class="modal-dialog">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left">Dodaj czas pracy do zadania</h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <?php $form::begin()?>
          <div class="modal-body">
               <div class="row">
                    <div class="col-sm-6">
                    <?php
                         echo DateTimePicker::widget([
                              'model' => $addWorkTimeModel,
                              'options' => [
                                   'placeholder' => 'Data rozpoczęcia',
                                   'readonly' => true,
                                   'id' => 'startWork',
                              ],
                              'attribute' => 'startWork',
                              'size' => 'sm',
                              'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                              'pluginOptions' => [
                                  'format' => 'dd-mm-yyyy hh:ii',
                                  'autoclose' => true,
                                   'language' => 'pl',
                              ]
                         ]);
                    ?>
                    </div>
                    <div class="col-sm-6">
                    <?php
                         echo DateTimePicker::widget([
                              'model' => $addWorkTimeModel,
                              'options' => [
                                   'placeholder' => 'Data zakończenia',
                                   'readonly' => true,
                                   'id' => 'endWork'
                              ],
                              'attribute' => 'endWork',
                              'size' => 'sm',
                              
                              'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                              'pluginOptions' => [
                                  'format' => 'dd-mm-yyyy hh:ii',
                                  'autoclose' => true,
                                  'language' => 'pl',
                              ],
                              'pluginEvents' => [
                                   'open' => 'function() { console.log("open"); }',
                              ]
                         ]);
                    ?>
                    </div>
               </div>
          </div>
          <div class="modal-footer">
               <?php 
                    echo Html::submitButton('Zapisz', ['class' => 'btn btn-success col-sm-12']);
               ?>
          </div>
          <?php $form::end()?>
     </div>
</div>
