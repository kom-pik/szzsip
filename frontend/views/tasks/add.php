<?php

use common\models\TasksModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $task TasksModel */
/* @var $this View */
?>
<div class="modal-dialog">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left">Dodaj zadanie</h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <?php echo $this->render('partials/task-form', array(
               'task' => $task
          ))?>
     </div>
</div>