<?php

use common\models\OrdersTasksModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $orderTask OrdersTasksModel*/
/* @var $this View */
/* @var $property string*/

?> 
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
             <h3 class="pull-left"><?= $header?></h3>
             <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <?= $body ?>
        </div>
        <div class="modal-footer">
            <?= Html::button('Ok', ['class' => 'btn btn-primary  col-sm-12', 'data-dismiss' => 'modal'])?>
        </div>
    </div>
</div>