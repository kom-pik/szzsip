<?php

use common\models\TasksModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $task TasksModel */
/* @var $this View */

$taskForm = ActiveForm::begin([
            'options' => ['id' => 'task-form'],
]);

?>
<div class="modal-body">
     <?= $taskForm->field($task, 'tas_name')->textInput() ?>
     <?= $taskForm->field($task, 'tas_status')->dropDownList(TasksModel::listStatuses()) ?>
     <?= $this->params['user']->isAdmin() ? $taskForm->field($task, 'tas_group_fkey')->dropDownList($this->params['groups']) : ''?>
     <?= $taskForm->field($task, 'tas_description')->textArea(['class' => 'tinymce']) ?>
</div> 

<div class="modal-footer">
     <?=
     Html::submitButton(Yii::t('app', 'Zapisz'), [
         'class' => 'btn btn-primary col-sm-12'
     ])
     ?>
</div>
<?php ActiveForm::end(); ?>

<script>
     tinymce.init(tinymceDefaultOptions);
</script>