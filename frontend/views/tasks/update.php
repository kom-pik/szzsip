<?php
use yii\web\View;

/* @var $this View*/

$this->title = 'Edytuj zadanie';
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('partials/task-form', array(
               'task' => $task
          ));


