<?php

use common\models\TasksModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $task TasksModel */
/* @var $this View */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-12">
     <div class="row">
          <?php echo Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Dodaj zadanie do zlecenia'), ['/orders/add-task-to-order', 'order_id' => 0,'task_id' => $task->tas_id], ['class' => 'btn btn-success modal-preview']); ?>
          <?php echo Html::a(Yii::t('app', '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edytuj zadanie'), ['update', 'id' => $task->tas_id], ['class' => 'btn btn-success']); ?>
          <hr> 
     </div>

     <div class="row">
          <div class="col-sm-12">
               <label class=""><?= $task->getAttributeLabel('tas_name') ?></label>
               <div><?= $task->tas_name ?></div>
               <hr>
          </div>
          <div class="col-sm-12">
               <label class=""><?= $task->getAttributeLabel('tas_description') ?></label>
               <div><?= $task->tas_description ?></div>
               <hr>
          </div>    
          <div class="row my-col-sm-2">
               <div class="col-sm-2">
                    <label><?= $task->getAttributeLabel('tas_created_by') ?></label>
                    <div><?= Html::a($task->creator->usr_username, Url::to(['/users/view', 'id' => $task->tas_created_by])) ?></div><hr>
               </div>

               <div class="col-sm-2">
                    <label><?= $task->getAttributeLabel('tas_created_at') ?></label>
                    <div><?= date('d-m-Y H:i', $task->tas_created_at) ?></div><hr>
               </div>

               <div class="col-sm-2">
                    <label><?= $task->getAttributeLabel('tas_status') ?></label>
                    <div><?= TasksModel::listStatuses()[$task->tas_status] ?></div><hr>
               </div>
          </div>
     </div>
