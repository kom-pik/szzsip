<?php
use common\models\UsersModel;
use yii\web\View;


/* @var $this View*/
/* @var $user UsersModel*/

$this->title = 'Dodaj użytkownika';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog modal-lg">
     <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left"><i class="glyphicon glyphicon-plus"></i> Dodaj użytkownika</h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
           <div class="modal-body">
               <?= $this->render('partials/user-form', [
                    'user' => $user,
                    'clientsList' => $clientsList,
                    'groupsList' => $groupsList
                       ])?>

           </div>