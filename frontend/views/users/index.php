<?php

//

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\OrdersModel;
use common\models\ProjectsModel;
use common\models\UsersModel;
use common\models\ClientsModel;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;
use kartik\grid\ActionColumn;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Użytkownicy';
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->isGuest ? null: Yii::$app->user->identity;

?>

<div class="site-index">

     <?=
     GridView::widget([
          'panel' => [
               'type' => GridView::TYPE_PRIMARY,
               'heading' => '<i class="glyphicon glyphicon-user"></i> Użytkownicy',
          ], 'toolbar' => [
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-plus"></i> Dodaj użytkownika', Url::to(['add']), ['class' => 'btn btn-success add-object', 'data-pjax' => 0])
               ],
               [
                    'content' => $user->isAdminOrSupervisor() ? Html::a(Yii::t('app', '<i class="glyphicon glyphicon-minus"></i> Usuń zaznaczonych użytkowników'), Url::to(['delete']), ['class' => 'btn btn-danger grid-mass-delete disabled', 'data-pjax' => 0]) : ''
               ],
               [
                    'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Resetuj widok', ['index'], [
                         'class' => 'btn btn-default reset-grid',
                         'title' => Yii::t('app', 'Resetuj widok')
                    ])
               ],
               '{export}',
               '{toggleData}'
          ],
          'pjax' => true,
          'pjaxSettings' => [
               'neverTimeout' => true,
               'enablePushState' => false,
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
               [
                    'class' => '\kartik\grid\CheckboxColumn'
               ],
               [
                    'class' => '\kartik\grid\SerialColumn'
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
//                    'filterWidgetOptions' => [
//                        'data' => $orderNames,
//                        'options' => [
//                            'placeholder' => 'filtruj po nazwie ...',
//                            'initValueText' => ''
//                            ],
//                        'pluginOptions' => [
//                            'allowClear' => true,
////                            'minimumInputLength' => 3,
//                        ]
//                    ],
                    'attribute' => 'username',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->usr_username, Url::to(['view', 'id' => $model->usr_id]), ['data-pjax' => 0, 'title' => $model->usr_username]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => UsersModel::findAllUsers(['!=', 'usr_type', UsersModel::TYPE_CLIENT]),
                         'options' => [
                              'placeholder' => 'filtruj po nazwie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true,
//                            'minimumInputLength' => 3,
                         ]
                    ],
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                         return Html::a($model->getName(), Url::to(['view', 'id' => $model->usr_id]), ['data-pjax' => 0, 'title' => $model->getName()]);
                    }
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => UsersModel::listTypes(),
                         'options' => [
                              'placeholder' => 'filtruj po typie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'type',
                    'format' => 'raw',
                    'value' => function($model) {
                         return UsersModel::listTypes()[$model->usr_type];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                         'data' => UsersModel::listStatuses(),
                         'options' => [
                              'placeholder' => 'filtruj po statusie ...',
                              'initValueText' => ''
                         ],
                         'pluginOptions' => [
                              'allowClear' => true
                         ]
                    ],
                    'attribute' => 'status',
                    'value' => function($model) {
                         return UsersModel::listStatuses()[$model->usr_status];
                    },
               ],
               [
                    'class' => '\kartik\grid\DataColumn',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'attribute' => 'created',
                    'filterWidgetOptions' => [
                         'presetDropdown' => true,
                         'pluginOptions' => [
                              'locale' => [
                                   'format' => 'YYYY-MM-DD',
                              ]
                         ],
                         'pluginEvents' => [
                              "apply.daterangepicker" => "function() { apply_filter('date') }",
                         ]
                    ],
                    'value' => function ($model) {
                         return gmdate('d-m-Y H:i', $model->usr_created_at);
                    },
               ],
               [
                    'class' => '\kartik\grid\ActionColumn',
                    'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
                    'header' => 'Akcje',
                    'viewOptions' => [
                         'title' => 'Pokaż szczegóły'
                    ],
                    'updateOptions' => [
                         'title' => 'Edytuj'
                    ],
                    'deleteOptions' => [
                         'title' => 'Usuń',
                         'message' => 'Potwierdź usunięcie'
                    ],
                    'buttonOptions' => [
                         'class' => 'add-object'
                    ]
               ],
          ],
          'showPageSummary' => true,
     ])
     ?>
</div>

<script type="text/javascript">
     function apply_filter() {

          $('.grid-view').yiiGridView('applyFilter');

     }
</script>