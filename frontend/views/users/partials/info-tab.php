<?php

use common\models\UsersModel;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $user UsersModel */
/* @var $this View */
?>
<div class="col-sm-12">
     <div class="row">
          <hr> 
     </div>
     <div class="row">
          <div class="col-sm-12">
               <label class=""><?= $user->getAttributeLabel('name') ?></label>
               <div><?= $user->name ?></div>
               <hr>
          </div>

     </div>
     <div class="row">
          <div class="col-sm-6">
               <label><?= $user->getAttributeLabel('usr_phone') ?></label>
               <div>
                    <?=
                         $user->usr_phone ? 
                              $user->usr_phone :
                              Html::a(Yii::t('app', 'dodaj telefon'), ['add-property', 'id' => $user->usr_id, 'property' => 'phone'], ['class' => 'add-property'])
                    ?>
               </div>
          </div>
          <div class="col-sm-6">
               <label><?= $user->getAttributeLabel('usr_email') ?></label>
               <div>
                    <?=
                         !empty($user->usr_email) ?
                            Html::mailto(Yii::t('app', $user->usr_email), $user->usr_email, []) :
                            Html::a(Yii::t('app', 'Dodaj e-mail'), Url::to(['add-property', 'id' => $user->usr_id, 'property' => 'email']), ['class' => 'add-property'])
                    ?>
               </div>
          </div>
     </div>
     <hr>
     <div class="row my-col-sm-2">
          <div class="col-sm-2">
               <label><?= $user->getAttributeLabel('usr_username') ?></label>
               <div><?= $user->usr_username ?></div><hr>
          </div>
          <div class="col-sm-2">
               <label><?= $user->getAttributeLabel('usr_type') ?></label>
               <div>
                    <?=
                         $user->isClient() ?
                                 $user::listTypes()[$user->usr_type] . ' (' . Html::a($user->company->cli_acronym, ['/clients/view', 'id' => $user->company->cli_id]) . ')' :
                                 $user::listTypes()[$user->usr_type]
                    ?>
               </div>
               <hr>
          </div>
          <div class="col-sm-2">
               <label><?= $user->getAttributeLabel('usr_created_at') ?></label>
               <div><?= date('d-m-Y H:i', $user->usr_created_at) ?></div><hr>
          </div>

          <div class="col-sm-2">
               <label><?= $user->getAttributeLabel('usr_status') ?></label>
               <div>
                    <?php
                         echo UsersModel::listStatuses()[$user->usr_status];
                         if ((!$this->params['user']->isClient() && $user->isClient()) || ($this->params['user']->isAdminOrSupervisor() && $this->params['user']->usr_id != $user->usr_id)) {
                              echo Html::a(Yii::t('app', '<i class="glyphicon glyphicon-refresh"></i>'), Url::to(['add-property', 'id' => $user->usr_id, 'property' => 'status']), [
                                  'class' => 'add-property m-l-5',
                                  'title' => 'Zmień status',
                              ]);
                         }
                    ?>
               </div>
               <hr>
          </div>
     </div>
</div>

