<?php
use common\models\UsersModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $user UsersModel */
/* @var $this View */
?>
<div class="container">
    <?php $form = ActiveForm::begin([
        'options' => ['id' => 'user-form'],
    ]); ?>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($user, 'usr_firstname')->textInput() ?></div>
          <div class="col-sm-6"><?= $form->field($user, 'usr_lastname')->textInput() ?></div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($user, 'usr_username')->textInput([!empty($user->usr_username) ? 'disabled' : '']) ?></div>
          <div class="col-sm-6"><?= $form->field($user, 'usr_email')->textInput() ?></div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($user, 'usr_phone')->textInput() ?></div>
          <div class="col-sm-6">
               <?=
               $form->field($user, 'usr_type')
               ->dropDownList($this->params['user']->isAdmin() ? UsersModel::listTypes() : UsersModel::listTypesWhenSupervisor(), [
                   'value' => $user->usr_type,
                   'prompt' => 'Wybierz typ użytkownika'
               ])
               ?>  
          </div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($user, 'usr_group_fkey')->dropDownList($groupsList, ['prompt' =>'Wybierz grupę']) ?></div>
          <div class="col-sm-6"><?= $form->field($user, 'usr_client_fkey')->dropDownList($clientsList, ['prompt' => 'Wybierz klienta']) ?></div>
     </div>
     <div class="row">
          <div class="col-sm-12">     
               <?php if (!$user->isNewRecord && $user->usr_id == $this->params['user']->usr_id) {
                         echo '<hr>';
                         echo $form->field($user, 'currentPassword')->passwordInput();
                    }
               ?>
          </div>
          <div class="col-sm-6"></div>
     </div>
     <div class="row">
          <div class="col-sm-6"><?= $form->field($user, 'newPassword')->passwordInput() ?></div>
          <div class="col-sm-6"><?= $form->field($user, 'newPasswordConfirm')->passwordInput() ?></div>
     </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Zapisz'), [
            'class' => 'btn btn-primary col-sm-12'
        ]) ?>
    </div>

    <?php $form->end(); ?>
</div>
