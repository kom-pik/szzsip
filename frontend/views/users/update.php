<?php

use common\models\UsersModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $user UsersModel */
/* @var $this View */

$this->title = 'Edytuj użytkownika';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if ($isAjax):?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
          <div class="modal-header">
               <h3 class="pull-left"><?= $this->title ?></h3>
               <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
<?php         endif;?>
<?= $this->render('partials/user-form', [
                       'user' => $user,
                       'usersList' => $usersList,
                       'clientsList' => $clientsList,
                       'groupsList' => $groupsList
        ])?>

<?php if ($isAjax):?>
          </div>
    </div>
</div>
<?php         endif;?>
