<?php

use yii\helpers\Html;
use yii\web\View;
use common\models\UsersModel;
use yii\bootstrap\Tabs;

/* @var $user UsersModel */
/* @var $this View */

$this->params['breadcrumbs'][] = $this->title;

if ($this->params['user']->isAdminOrSupervisor()) {
     echo Html::a(Yii::t('app', 'Edytuj dane użytkownika'), ['update', 'id' => $user->usr_id], [
         'class' => 'btn btn-success',
         'id' => 'btn-edit'
     ]);
}
?>

<?php if (!empty($modal)) : ?>
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h3>Użytkownik <?= $user->name?></h3>
               </div>
               <div class="modal-body">
<?php endif; ?>
<?php
$items[] = [
    'label' => 'Informacje ogólne',
    'options' => [
        'id' => 'info-tab'
    ],
    'active' => 'true',
    'content' => $this->render('partials/info-tab', ['user' => $user])
];

if (!$this->params['user']->isClient()) {
     $items = array_merge($items, [
         [
             'label' => 'Projekty użytkownika',
             'options' => [
                 'id' => 'projects-tab'
             ],
             'content' => $this->render('partials/projects-tab', [
                 'user' => $user,
                 'projectDataProvider' => $projectDataProvider,
                 'projectSearchModel' => $projectSearchModel,
                 'clientAllProjectsNames' => $userAllProjectsNames
             ])
         ],
         [
             'label' => 'Zlecenia użytkownika',
             'options' => [
                 'id' => 'orders-tab'
             ],
             'content' => $this->render('partials/orders-tab', [
                 'user' => $user,
                 'orderDataProvider' => $orderDataProvider,
                 'orderSearchModel' => $orderSearchModel,
                 'userAllOrdersNames' => $userAllOrdersNames
             ])
         ]
     ]);
}

echo Tabs::widget([
    'items' => $items
]);
?>
<?php if (!empty($modal)) : ?>
                    <div class="modal-footer"></div>
               </div>
          </div>
     </div>
<?php endif; ?>
