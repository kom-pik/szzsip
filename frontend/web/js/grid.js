var Grid = {
     $document: $(document),
     init: function(){
          this.setEventListeners();
     },
     setEventListeners: function(){
          this.$document.on('click', '.grid-mass-delete', Grid.showModalConfirmDelete);
          this.$document.on('click', '#confirm-delete', Grid.massDeleteRows);
          this.$document.on('change', '[name="selection[]"], [name="order-tasks[]"], .select-on-check-all', Grid.activateMassButtons);
          this.$document.on('input change', '.grid-view input.form-control', Grid.applyFilter);
          this.$document.on('click', '.grid-mass-add-task-to-order', Grid.massAddTaskToOrder);
     },
     
     showModalConfirmDelete: function(e){
          e.preventDefault();
          $('#modal-confirm-delete').modal('show');
          return false;
     },
     massDeleteRows: function(e){
          e.preventDefault();
          var ids = $('.grid-view').yiiGridView('getSelectedRows');
          var url = $('.grid-mass-delete').attr('href');
          $.ajax({
               type: "POST",
               url: url,
               data: {
                    ids: ids
               },
               success: function(){
               }
          });
          return false;
     },
     activateMassButtons: function(){
          if ($('[name="selection[]"]:checked, [name="order-tasks[]"]:checked').length > 0){
               $('.grid-mass-delete, .grid-mass-add-task-to-order').removeClass('disabled');
          } else {
               $('.grid-mass-delete, .grid-mass-add-task-to-order').addClass('disabled');
          }
     },
     applyFilter: function (){
          setTimeout(function() { apply_filter(); }, 700);
          //MY_TODO focus on input after ajax
          return false;
     },
     massAddTaskToOrder: function(e){
          e.preventDefault();
          var taskIds = $('.grid-view').yiiGridView('getSelectedRows');
          var url = '/orders/add-task-to-order';
          $.ajax({
               type: "POST",
               url: url,
               data: {
                    taskIds: taskIds
               },
               success: function(response){
                    var $modal = $('#modal');
                    $modal.find('.modal-dialog').html(response);
                    $modal.modal('show');
               }
          })
          return false;
     }
};

$(function(){
     Grid.init();
});

function resetGrid(){
     $('.grid-reset:visible').trigger('click');
}