var tinymceDefaultOptions = {
          selector: 'textarea.tinymce',
          height: 200,
          theme: 'modern',
          plugins: 'print preview fullpage paste searchreplace autolink directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools media contextmenu colorpicker textpattern help',
          toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat code',
          image_advtab: true,
          templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
          ],
          language: 'pl',
          content_css: [
//            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
          ]
     };

function apply_filter(selector = '.grid-view') {
     $(selector).yiiGridView('applyFilter');
}

function startGlobalLoader() {
     $('body').addClass('kv-grid-loading');
}

function stopGlobalLoader() {
     $('body').removeClass('kv-grid-loading');
}

function startLoader(selector) {
     $(selector).addClass('kv-grid-loading');
}
;

function stopLoader(selector) {
     $(selector).removeClass('kv-grid-loading');
}

function closeAlerts() {
     $(".alert").alert('close');
}

function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var App = {
     $document: $(document),
     init: function () {
          this.setEventListeners();
     },
     setEventListeners: function () {
          this.$document.on("change", "#reportmodel-type", App.submitReportForm);
          this.$document.on("click", "#report-submit", App.submitReportForm);
          this.$document.on("click", ".btn-update", App.loadUpdateFormAjax);
          this.$document.on("click", ".submit-form", App.submitForm);
          this.$document.on("click", "a[data-toggle=tab]", App.setActiveTab);
          this.$document.on("click", ".show-client-form", App.showClientForm);
          this.$document.on('submit', '.hidden-form', App.submitHiddenForm);
          this.$document.on('click', 'a.disabled:not(.btn)', App.disabledClick);
     },
     submitReportForm: function (e) {
          e.preventDefault();
          var data = $('#report-form').serializeArray();
          var url = '/reports/prepare';
          if ($(this).is("#reportmodel-type") && $(this).val() === '') {
               return false;
          }
          startLoader('.site-index');
          $.ajax({
               type: 'POST',
               data: data,
               url: url,
               success: function (response) {
                    $('#report-data').html(response);
                    stopLoader('.site-index');
               },
               error: function (response) {
                    $('#report-data').html(response);
                    stopLoader('.site-index');
               }
          });
          return false;
     },
     loadUpdateFormAjax: function (e) {
          e.preventDefault();
          var url = $(this).attr('href');
          $.ajax({
               type: 'POST',
               url: url,
               success: function (response) {
                    $('.entity-info').html(response);
               }
          });
          return false;
     },
     submitForm: function (e) {
          e.preventDefault();
          $(this).closest('form').submit();
     },
     setActiveTab() {
          $('.nav-tabs li').removeClass('active');
          $('.nav-tabs a[href="' + $(this).attr('href') + '"]').tab('show');
     },
     showClientForm: function (e){
          e.preventDefault();
          $(this).closest('.modal-content').find('.project-form').toggle(300);
          $('.hidden-client-form').toggle(300).find('textarea, input, select').prop('disabled', false);
          if ($(this).is('.hide-form')){
               $('.hidden-client-form').find('textarea, input, select').prop('disabled', true);
          }
          return false;
     },
     submitHiddenForm: function (e) {
          e.preventDefault();
          var data = $(this).serialize();
          startLoader('.hidden-form');
          $.ajax({
               url: $(this).attr('action'),
               type: 'post',
               data: data,
               success: function(response){
                    stopLoader('.hidden-form');
                    if (response.success == true){
                         var clientsSelect = '';
                         for (var i in response.clients){
                              clientsSelect += '<option value="'+i+'" >'+response.clients[i]+'</option>';
                         }
                         $('#projectsmodel-pro_client_fkey').html(clientsSelect);
                         $('#projectsmodel-pro_client_fkey').val(response.cli_id);
                         $('.show-client-form:not(.hide-form)').trigger('click');
                    } else {
                         for (var i in response.errors){
                              $('[name*='+i+']').parent().addClass('has-error').find('.help-block').html(response.errors[i]);
                         }
                    }
               },
               error: function (response) {
                    console.log(response)
                    stopLoader('.hidden-form');
               }
          });
          return false;
     },
     disabledClick: function (e) {
          e.preventDefault();
          return false;
     }
};

$(function () {
     App.init();
     $(document).ready(function () {
          setTimeout(function () {
               $(".alert").alert('close');
          }, 2500);

          setInterval(function () {
               var date = new Date();
               var d = date.getDate();
               if (d < 10) {
                    d = '0' + d;
               }
               var m = date.getMonth() + 1;
               if (m < 10) {
                    m = '0' + m;
               }
               var y = date.getFullYear();
               var H = date.getHours();
               if (H < 10) {
                    H = '0' + H;
               }
               var i = date.getMinutes();
               if (i < 10) {
                    i = '0' + i;
               }
               var s = date.getSeconds();
               if (s < 10) {
                    s = '0' + s;
               }
               $('.pjax-clock').html('<div>' + H + ':' + i + ':' + s + '</div>' + '<div>' + d + '-' + m + '-' + y + '</div>');
          }, 1000);
          
          if (urlParam('tab')){
               $('a[href="#'+urlParam('tab')+'"]').trigger('click');
          }
     });
     
     $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip(); 
     });
     
     tinymce.init(tinymceDefaultOptions);
});