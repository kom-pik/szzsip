var Modals = {
     $document: $(document),

     init: function () {
          this.setEventListeners();
     },

     setEventListeners: function () {
          this.$document.on('click', '.add-property, #buttonAddOrder, .add-object, .modal-preview', Modals.showModal);
          this.$document.on('click', '#start, #stop, #pause, #continue', Modals.taskWorkActions);
          this.$document.on('submit', '#project-form, #add-property-form, .ajax-form', Modals.ajaxModalForm);
          this.$document.on('hidden.bs.modal', '#modal, #modal-large', Modals.resetModals);
          this.$document.on('shown.bs.modal', '#modal', Modals.addAlpha);
     },

     taskWorkActions: function (e) {
          e.preventDefault();
          Modals.showModal(e, $(this));
     },

     showModal: function (e, obj) {
          e.preventDefault();
          if (typeof obj === 'undefined') {
               obj = $(this);
          }
          var $modal = $('#modal');
          if (obj.hasClass('modal-preview')) {
               $modal = $('#modal-large');
          }
          var $url = obj.attr('href');
          $modal.load($url);
          $modal.modal('show');
     },

     ajaxModalForm: function (e) {
          e.preventDefault();
          var data = $(this).serialize();
          var url = $(this).attr('action');
          $.ajax({
               type: 'POST',
               url: url,
               data: data,
               success: function (response) {
                    var alertClass = 'alert-success';
                    if (response.success === false) {
                         alertClass = 'alert-error';
                    }
                    var html = '<div class="alert ' + alertClass + ' fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response.message + '</div>';
                    $('.modal.fade.in').modal('hide');
                    $('body').prepend(html);
                    closeAlerts();
                    resetGrid();
               },

          });
          return false;
     },
     resetModals: function () {
          $(this).find('.modal-content').html('');
          if ($(this).is('#modal')) {
               $('#modal-large').find('.modal-content').removeClass('opacity-65');
          }
          $('.grid-reset:visible').trigger('click');
     },
     addAlpha: function () {
          $('#modal-large').find('.modal-content').addClass('opacity-65');
     }
};

$(function () {
     Modals.init();
});

$('.kv-grid-table').on('submit', '#order-form', function (e) {
     e.preventDefault();

     var data = $(this).serializeArray();
     var url = $(this).attr('action');
     $.ajax({
          type: 'POST',
          url: url,
          data: data,
          success: function (response) {
               $('#orderDescRow').replaceWith(response);
          }

     });
     return false;
});